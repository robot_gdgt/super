"""
Jog v0.0
"""

import can
import logger
import logging
import math
from os import path
# import redis
import subprocess
import sys
from time import sleep, time

from status_led import StatusLED
from speak_direct_can import speak_direct
from gdgt_platform import *
from can_ids import *
from tts_groups import *
from var_dump import var_dump as vd

##### SETUP #####

# try:
#   red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
#   red.get('dummy')
# except redis.exceptions.ConnectionError:
#   raise Exception('Is the redis server running?')

try:
    subprocess.run("sudo ip link set can0 down", shell=True)
    subprocess.run(f"sudo ip link set can0 up type can bitrate {CAN_BUS_SPEED}", shell=True)
    bus = can.interface.Bus(bustype='socketcan_native', channel='can0')
    # can_filters = [
    #     {"can_id": CAN_CMD_HALT, "can_mask": 0x7FF, "extended": False},
    #     {"can_id": CAN_STATUS_VIS_OBST, "can_mask": 0x7FF, "extended": False},
    #     {"can_id": 0x000, "can_mask": 0x700, "extended": False}, # Want all alert packets
    #     # TTS
    #     {"can_id": CAN_CMD_SPEAK, "can_mask": 0x7FF, "extended": False},
    #     {"can_id": CAN_SPEAK_DIRECT, "can_mask": 0x700, "extended": False},
    # ]
    # bus.set_filters(can_filters)
except:
    raise Exception('Failed to configure CAN')

"""
uint8_t immediate = CANbuf[0] >> 7; // immediate = 1, buffered = 0
uint8_t speed = CANbuf[0] & 0x1F; // 0 - 17
if(speed > 17) speed = 17;
uint8_t lr = CANbuf[1] >> 7; // left = 1, right = 0
uint8_t x = (CANbuf[1] & 0x7F); // 0 - 127cm
uint8_t y = CANbuf[2]; // 0 - 255cm
"""

# when = 'i'
speed = 0.5
dir = 'l'
x = 10
# y = 50
rtn = 0

while (True):
    userInput = str(input('Enter data: ')).lower()
    cmd = userInput[0:1]
    val = userInput[1:].strip()
    if cmd == 'q':
        break

    if cmd == '/' or cmd == '?':
        print()
        # print(f'w when (i b):       {when}')
        print(f's speed (0.1 - 1.0): {speed}')
        print(f'd dir (l r):         {dir}')
        print(f'x jog dist (cm):     {x}')
        # print(f'y rev dist (cm):    {y}')
        print(f'r return:            {rtn}')
        print()
        continue

    userInput = (userInput + ' 0 0').split()

    # if cmd == 'w':
    #   if val == 'i' or val == 'b':
    #       when = val
    #   else:
    #       print('Invalid')
    #   continue

    if cmd == 's':
        s = float(int(float(val) * 10) / 10)
        if s < 0.1 or s > 1.0:
            print('Invalid')
        else:
            speed = s
        continue

    if cmd == 'd':
        if val == 'l' or val == 'r':
            dir = val
        else:
            print('Invalid')
        continue

    if cmd == 'x':
        t = int(val)
        if t < 0:
            print('Invalid')
        else:
            x = t
        continue

    # if cmd == 'y':
    #   t = int(val)
    #   if t < 0:
    #       print('Invalid')
    #   else:
    #       y = t
    #   continue

    if cmd == 'r':
        t = int(val)
        if t == 0 or t == 1:
            rtn = t
        else:
            print('Invalid')
        continue

    if cmd == 'g':
        bytes = []

        id = CAN_CMD_RC_MOVE_JOG

        bytes.append(
            # (RC_MOVE_IMMED if when == 'i' else 0) |
            round(speed * 10)
        )

        bytes.append((0x80 if dir == 'l' else 0) | (0x40 if rtn == 1 else 0) | x)

        # bytes.append(y)

        print(bytes)
        msg = can.Message(
            arbitration_id=id,
            is_extended_id=False,
            dlc=len(bytes),
            data=bytes)
        bus.send(msg, timeout=0.1)

subprocess.run("sudo ip link set can0 down", shell=True)
