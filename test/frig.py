"""
Frig v0.0
"""

import can_proxy_thread as can
import logger
import logging
import math
from os import path
# import redis
# import subprocess
# import sys
from time import sleep, time

from status_led import StatusLED
# from speak_direct_can import speak_direct
from gdgt_platform import *
from can_ids import *
from tts_groups import *
from var_dump import var_dump as vd

hbLED = StatusLED(LED_PIN, True)

FILE = path.splitext(path.basename(__file__))[0]
# logger.init(FILE, logging.INFO)
# logger.init(None, logging.INFO)

# States
STATE_LOOKING_FOR_FRIG = 1
STATE_APPROACHING_FRIG = 2
STATE_OPEN_DOOR = 3

# App state object class
class AppState:
    def __init__(self):
        self.state = STATE_LOOKING_FOR_FRIG

state = AppState()

class RollingMedian:
    def __init__(self, size, init_value = 0):
        self.size = size
        self.buffer = [init_value] * size
        self.ptr = 0
        self.count = 0

    def add(self, v):
        self.buffer[self.ptr] = v
        self.ptr = (self.ptr + 1) % self.size
        if self.count < self.size:
            self.count += 1

    def get(self):
        if self.count < self.size:
            return None
        buf = self.buffer.copy()
        buf.sort()
        return buf[int(self.size / 2)]

##### SETUP #####

# try:
#   red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
#   red.get('dummy')
# except redis.exceptions.ConnectionError:
#   raise Exception('Is the redis server running?')

can.add_filter('motor', CAN_STATUS_RC_STAT, 0x7FF)

can.add_filter('frig', CAN_CMD_HALT, 0x7FF)
can.add_filter('frig', CAN_STATUS_FRIG,  0x7FF)
can.add_filter('frig', 0x000, 0x700) # Want all alert packets

print('HERE')

# speak_direct('supervisor is running')

rm_center_x = RollingMedian(5)
rm_center_z = RollingMedian(5)
rm_left_x = RollingMedian(5)
rm_left_z = RollingMedian(5)

hbLED.heartbeat()

##### LOOP #####

try:

    last_rc_ping = 0

    while True:

        print(f'last_rc_ping: {last_rc_ping}')
        now = time()

        # wait while motors are running
        is_moving = True
        CANrx = can.get('motor')
        if CANrx != None:
            print(CANrx)
            if CANrx.data[0] == 0: # Stopped
                is_moving = False
            else:
                can.send('motor', CAN_STATUS_RC, True)
                last_rc_ping = now
        
        if is_moving and (now - last_rc_ping) > 1:
            can.send('motor', CAN_STATUS_RC, True)
            last_rc_ping = now

        if is_moving == False:
            CANrx = can.get('frig')
            if CANrx != None:
                print(CANrx)

            if state.state == STATE_LOOKING_FOR_FRIG:
                if CANrx is not None:
                    if CANrx.id == CAN_STATUS_FRIG:
                        rm_center_x.add(int.from_bytes([CANrx.data[0], CANrx.data[1]], byteorder='big', signed=True))
                        center_x = rm_center_x.get()
                        rm_center_z.add(int.from_bytes([CANrx.data[2], CANrx.data[3]], byteorder='big', signed=True))
                        center_z = rm_center_z.get()
                        rm_left_x.add(int.from_bytes([CANrx.data[4], CANrx.data[5]], byteorder='big', signed=True))
                        left_x = left_x.get()
                        rm_left_z.add(int.from_bytes([CANrx.data[6], CANrx.data[7]], byteorder='big', signed=True))
                        left_z = rm_left_z.get()
                        print(f"center: ${center_x}, ${center_z} left: ${left_x}, ${left_z}")
                        if center_x != None and center_z != None:
                            # is it far from the center
                            a = math.degrees(math.atan(center_x / center_z))
                            print(f"angle: ${a}")
                            if(a > 15): # turn right
                                print('turn right')
                            elif(a < -15): # turn left
                                print('turn left')
                            # print(f'dist: {dist} dir: {dir} dir2: {dir2}')
                            # msg = can.Message(
                            #     id=CAN_CMD_RC_MOVE_ANGLE,
                            #     is_extended_id=False,
                            #     dlc=2,
                            #     data=[dist + 128, dir2 + 128])
                            # bus.send(msg, timeout=0.1)

        elif state.state == STATE_APPROACHING_FRIG:
            pass

        elif state.state == STATE_OPEN_DOOR:
            pass

        # dt = time() - now
        # print(f"{int(1.0/dt)} FPS")

        # Blink the LED
        hbLED.tickle()

        # if state.state == CAN_CMD_PGM_QUIT or state.state == CAN_CMD_HALT:
        #     break

except KeyboardInterrupt:
    logging.info('KeyboardInterrupt')

# msg = can.Message(
#     id=CAN_CMD_RC_MOVE,
#     is_extended_id=False,
#     dlc=2,
#     data=[0, 0])
# bus.send(msg, timeout=0.1)

if state.state == CAN_CMD_HALT:
    sleep(5)
    subprocess.run("sudo halt", shell=True)
