"""
MoveTurnMove - Move straign, turn, then move straight again

data.js: {
    "name": "Home to Hall",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "help": "Move straign, turn, then move straight again.",
    "params": [
        {
            "key": "iDist1",
            "label": "Distance 1 (cm)",
            "kind": "integer",
            "default": 75,
        },
        {
            "key": "iRad1",
            "label": "Radius 1 (cm)",
            "kind": "integer",
            "default": 140,
        },
        {
            "key": "iAng1",
            "label": "Turn angle 1 (°)",
            "kind": "integer",
            "default": 80,
        },
        {
            "key": "iDist2",
            "label": "Distance 2 (cm)",
            "kind": "integer",
            "default": 268,
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
}
"""

# import logging
import time

# import can_proxy_thread as can
# from can_ids import CAN_CMD_RC_MOVE_TO, CAN_STATUS_RC, RC_MOVE_IMMED, RC_MOVE_END_STOP

from ..Blackboard import Blackboard as bb
from ..Node import Node
from ..movement import moveDist, moveAngle, alignHeading, isMoving

INTERNAL_IDLE = 0
MOVE1 = 1

class MoveTurnMove(Node):

    internalState = INTERNAL_IDLE
    prevIntState = None
    timeoutStart = None
    speed = None

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'iDist1'):
            raise RuntimeError(f'Missing param - {self.label}.iDist1')
        if not hasattr(self, 'iRad1'):
            raise RuntimeError(f'Missing param - {self.label}.iRad1')
        if not hasattr(self, 'iAng1'):
            raise RuntimeError(f'Missing param - {self.label}.iAng1')
        if not hasattr(self, 'iDist2'):
            raise RuntimeError(f'Missing param - {self.label}.iDist2')
        if not hasattr(self, 'iTimeout'):
            raise RuntimeError(f'Missing param - {self.label}.iTimeout')

    def init(self):
        speed = bb.get(self.label, 'fMoveSpeed', 0.2)

        self.timeoutStart = time.monotonic()
        self.speed = bb.get('OpenFrig', 'fMoveSpeed', default=0.2)
        self.internalState = MOVE1
        self.prevIntState = -1

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        newState = (self.prevIntState != self.internalState)
        self.prevIntState = self.internalState

        if self.internalState == MOVE1:
            if newState:
                print('Home2Hall MOVE1')
                if self.dist1:
                    moveDist(self.speed, self.iDist1, end='c')
                if self.dist1:
                    moveAngle(self.speed, self.iAng1, radius=self.iRad1, when='b', end='c')
                if self.dist1:
                    moveDist(self.speed, self.iDist2, when='b', end='s')
            else:
                if not isMoving():
                    return (Node.SUCCESS, None)

        # Has timeout occurred?
        if self.iTimeout > 0 and (time.monotonic() - self.timeoutStart) > self.iTimeout: # Timeout after iTimeout seconds
            return (Node.FAILURE, None)

        return (Node.RUNNING, None)
