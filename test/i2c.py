import smbus

bus = smbus.SMBus(1)    # 0 = /dev/i2c-0 (port I2C0), 1 = /dev/i2c-1 (port I2C1)

"""
0x0A What slots are occupied?
0xA0 Enable slots
	0b1010ESSS:
		E = Write to EEPROM
		S = Enable slot
0x05 What is version and segment ID?
    twiState = TWI_WHAT_SEGMENT;
0x50 Save the segment ID
	0b01010NNN
		NNN = Segment ID
0xC0 LED off
0xC1 LED on
0xCC Hard reset
"""

bus.write_byte(0x31, 0xAF)
bus.write_byte(0x31, 0xA7)
