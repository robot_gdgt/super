"""
Supervisor v0.0
"""

import can
import logger
import logging
import math
from os import path
import redis
import subprocess
import sys
from time import sleep, time

from status_led import StatusLED
from speak_direct_can import speak_direct
from gdgt_platform import *
from can_ids import *
from tts_groups import *
# from var_dump import var_dump as vd

hbLED = StatusLED(LED_PIN, True)

FILE = path.splitext(path.basename(__file__))[0]

logger.init(None, logging.INFO)

# App state object class
class AppState:
    def __init__(self):
        self.state = CAN_CMD_PGM_RUNNING

state = AppState()

class RollingMedian:
    def __init__(self, size, init_value = 0):
        self.size = size
        self.buffer = [init_value] * size
        self.ptr = 0

    def add(self, v):
        self.buffer[self.ptr] = v
        self.ptr = (self.ptr + 1) % self.size

    def get(self):
        buf = self.buffer.copy()
        buf.sort()
        return buf[int(self.size / 2)]

##### SETUP #####

try:
    red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
    red.get('dummy')
except redis.exceptions.ConnectionError:
    raise Exception('Is the redis server running?')

try:
    subprocess.run("sudo ip link set can0 down", shell=True)
    subprocess.run(f"sudo ip link set can0 up type can bitrate {CAN_BUS_SPEED}", shell=True)
    bus = can.interface.Bus(bustype='socketcan_native', channel='can0')
    can_filters = [
        {"can_id": CAN_CMD_HALT, "can_mask": 0x7FF, "extended": False},
        {"can_id": CAN_STATUS_VIS_OBST, "can_mask": 0x7FF, "extended": False},
        {"can_id": 0x000, "can_mask": 0x700, "extended": False}, # Want all alert packets
        # TTS
        {"can_id": CAN_CMD_SPEAK, "can_mask": 0x7FF, "extended": False},
        {"can_id": CAN_SPEAK_DIRECT, "can_mask": 0x700, "extended": False},
    ]
    bus.set_filters(can_filters)
except:
    raise Exception('Failed to configure CAN')

speak_direct('supervisor is running')

run_avg_dist = RollingMedian(7)
run_avg_dir = RollingMedian(7)
prev_dist = 0
prev_dir = 0

hbLED.heartbeat()

##### LOOP #####

try:

    while True:

        now = time()

        CANrx = bus.recv(0.0)
        if CANrx is not None:
#             print(CANrx)
            if CANrx.arbitration_id == CAN_STATUS_VIS_OBST:
                if state.state == CAN_CMD_PGM_RUNNING:
                    if CANrx.dlc == 3:
                        run_avg_dist.add(CANrx.data[0])
                        dist = run_avg_dist.get()
                        run_avg_dir.add(CANrx.data[1])
                        dir = run_avg_dir.get()
                        if dist != prev_dist or dir != prev_dir:
                            dir2 = -(dir - 128) * 4
                            if dir2 < -127:
                                dir2 = -127
                            elif dir2 > 127:
                                dir2 = 127
                            print(f'dist: {dist} dir: {dir} dir2: {dir2}')
                            msg = can.Message(
                                arbitration_id=CAN_CMD_RC_MOVE,
                                is_extended_id=False,
                                dlc=2,
                                data=[dist + 128, dir2 + 128])
                            bus.send(msg, timeout=0.1)
                            prev_dist = dist
                            prev_dir = dir

            elif CANrx.arbitration_id == CAN_CMD_SUPER:
                state.state = CANrx.data[0]
                if state.state != CAN_CMD_PGM_RUNNING:
                    msg = can.Message(
                        arbitration_id=CAN_CMD_RC_MOVE,
                        is_extended_id=False,
                        dlc=2,
                        data=[0, 0])
                    bus.send(msg, timeout=0.1)

            elif CANrx.arbitration_id & 0x700 == 0x000: # any alert
                state.state = CAN_CMD_PGM_SLEEP
                msg = can.Message(
                    arbitration_id=CAN_CMD_RC_MOVE,
                    is_extended_id=False,
                    dlc=2,
                    data=[0, 0])
                bus.send(msg, timeout=0.1)

            elif CANrx.arbitration_id == CAN_CMD_HALT:
                logging.info('shutdown')
                state.state = CAN_CMD_HALT

            elif CANrx.arbitration_id == CAN_CMD_SPEAK or CANrx.arbitration_id & 0x700 == CAN_SPEAK_DIRECT:
                red.rpush('tts', json.dumps({
                    'can_id': CANrx.arbitration_id,
                    'is_remote_frame': CANrx.is_remote_frame,
                    'dlc': CANrx.dlc,
                    'data': [x for x in CANrx.data]
                }))

#         dt = time() - now
#         print(f"{int(1.0/dt)} FPS")

        # Blink the LED
        hbLED.tickle()

        if state.state == CAN_CMD_PGM_QUIT or state.state == CAN_CMD_HALT:
            break

except KeyboardInterrupt:
    logging.info('KeyboardInterrupt')

msg = can.Message(
    arbitration_id=CAN_CMD_RC_MOVE,
    is_extended_id=False,
    dlc=2,
    data=[0, 0])
bus.send(msg, timeout=0.1)

subprocess.run("sudo ip link set can0 down", shell=True)

if state.state == CAN_CMD_HALT:
    sleep(5)
    subprocess.run("sudo halt", shell=True)
