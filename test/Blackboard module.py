"""
Methods for accessing the blackboard
"""

import json
import logging

dict = {}
log = []

def get(caller, key):
    global dict
    if key in dict:
        value = dict[key]
    else:
        value = None
    logging.info(f'{caller} get bb key {key} = {value}')
    return value

def set(caller, key, value):
    global dict, log
    if key[0] == 'i':
        value = int(value)
    elif key[0] == 'f':
        value = float(value)
    logging.info(f'{caller} set bb key {key} = {value}')
    dict[key] = value
    log.append({'caller':caller, 'key':key, 'value':value})

def delete(caller, key):
    global dict
    logging.info(f'{caller} del bb key {key}')
    if key in dict:
        del dict[key]

#     def addToQueue(caller, queue, value):
#         logging.info(f'{caller} add {value} to queue {queue}')
#         return red.rpush(queue, value)
#
#     def CANtx(caller, id, irf = False, dlc = None, data = None):
#         msg = {id: id}
#         if irf:
#             msg['irf'] = irf
#         if dlc != None:
#             msg['dlc'] = dlc
#         if data != None:
#             msg['data'] = data
#         return caller.addToQueue('CANtx', json.dumps(msg))
#
#     def CANfilter(caller, action, id, mask):
#         filter = {
#             action: action,
#             id: id,
#             mask: mask,
#         }
#         return caller.addToQueue('CANfilter', json.dumps(filter))

def getLog(caller):
    global log
    l = log.copy()
    log = []
    return l

def dump(caller):
    global dict
    return dict
