"""
Supervisor - Main
"""

import logging
import math
# import redis
import subprocess
from time import sleep, time

from can_ids import *
import can_proxy_thread as can
from gdgt_platform import *
from speak_direct_can_thread import speak_direct
from status_led import StatusLED
from tts_groups import *
from var_dump import var_dump as vd

hbLED = StatusLED(LED_PIN, True)
hbLED.heartbeat()

# FILE = path.splitext(path.basename(__file__))[0]
# logger.init(FILE, logging.INFO)
# logger.init(None, logging.INFO)

##### STATES #####

STATE_UNINIT = 0
STATE_INIT = 1
STATE_READY = 2
STATE_START = 3
STATE_AT_HALL = 4
STATE_LOOK_FOR_FRIG = 5
STATE_AT_FRIG = 9
STATE_OPEN_DOOR = 6
STATE_REPOSITION = 7
STATE_GRAB_CAN = 8

STATE_AT_HALL_LOOKING = 0
STATE_AT_HALL_ROTATING = 1
STATE_AT_HALL_MOVING = 2

STATE_LOOK_FOR_FRIG_LOOKING = 0
STATE_LOOK_FOR_FRIG_ROTATING = 1
STATE_LOOK_FOR_FRIG_MOVING = 2
STATE_LOOK_FOR_FRIG_ALIGNING = 3

STATE_AT_FRIG_LOOKING = 0
STATE_AT_FRIG_ROTATING = 1
STATE_AT_FRIG_MOVING = 2

STATE_OPEN_DOOR_LOOKING = 0
STATE_OPEN_DOOR_POSITIONING = 1
STATE_OPEN_DOOR_EXT_ARM = 2
STATE_OPEN_DOOR_ROTATING = 3
STATE_OPEN_DOOR_PULL_BACK = 4
STATE_OPEN_DOOR_ROTATE = 5

STATE_NAMES = [
    'Uninitialized',
    'Initialize',
    'Ready',
    'Start',
    'At hall',
    'Looking for frig',
    'Opening frig door',
    'Repositioning',
    'Grab the can',
    'At the frig',
]

class RollingMedian:
    def __init__(self, size, init_value = 0):
        self.size = size
        self.buffer = [init_value] * size
        self.ptr = 0
        self.count = 0

    def add(self, v):
        self.buffer[self.ptr] = v
        self.ptr = (self.ptr + 1) % self.size
        if self.count < self.size:
            self.count += 1

    def get(self):
        if self.count < self.size:
            return None
        buf = self.buffer.copy()
        buf.sort()
        return buf[int(self.size / 2)]

    def reset(self, init_value = 0):
        self.buffer = [init_value] * self.size
        self.ptr = 0
        self.count = 0

##### CONSTANTS #####

FRIG_SETBACK_X = 30 # 50
FRIG_SETBACK_Z = 450

##### GLOBALS #####

state = STATE_UNINIT
state_prev = -1
sub_state = -1
sub_state_prev = -1

move_speed = 0.4
prev_CANrx = None

sim_mode = False

rm_center_x = RollingMedian(5)
rm_center_z = RollingMedian(5)
rm_left_x = RollingMedian(5)
rm_left_z = RollingMedian(5)

##### SETUP #####

can.del_filter('main', 'all', 'all')
can.add_filter('main', CAN_CMD_HALT, 0x7FF)
can.add_filter('main', CAN_CMD_SUPER, 0x7F0)
can.add_filter('main', 0x000, 0x700) # Want all alert packets

can.add_filter('motor', CAN_STATUS_RC_STAT, 0x7FF)

##### HELPER FUNCTIONS #####

def move_dist(speed, dist_ang, dir='f', turn='s', radius = 0, when='i', end='s'):
    dist_ang = round(dist_ang)
    radius = round(radius)

    print(f'\nmove_dist(speed={speed}, dist_ang={dist_ang}, dir={dir}, turn={turn}, radius={radius}, when={when}, end={end})')

    bytes = []

    bytes.append(
        (RC_MOVE_IMMED if when == 'i' else 0) |
        (RC_MOVE_END_STOP if end == 's' else 0) |
        (RC_MOVE_FORWARD if dir == 'f' else 0) |
        round(speed * 10)
    )

    if turn != 's':
        r = (RC_MOVE_LEFT if turn == 'l' else 0) | radius
    else:
        r = RC_MOVE_STRAIGHT
    bytes.append(r >> 8)
    bytes.append(r & 0xFF)

    bytes.append(dist_ang >> 8)
    bytes.append(dist_ang & 0xFF)

    can.send('move', CAN_CMD_RC_MOVE_DIST, data=bytes)

def move_to(speed, x, z, when='i', end='s'):
    x = round(x)
    z = round(z)

    print(f'\nmove_to(speed={speed}, x={x}, z={z}, when={when}, end={end})')

    bytes = []

    bytes.extend(x.to_bytes(2, 'big', signed=True))
    bytes.extend(z.to_bytes(2, 'big', signed=True))

    bytes.append(
        (RC_MOVE_IMMED if when == 'i' else 0) |
        (RC_MOVE_END_STOP if end == 's' else 0) |
        round(speed * 10)
    )

    can.send('move', CAN_CMD_RC_MOVE_TO, data=bytes)

def is_moving():
    global prev_CANrx

    if "last_rc_ping" not in is_moving.__dict__: is_moving.last_rc_ping = 0

    moving = True
    CANrx = can.get('motor')
    if CANrx != None:
        if(prev_CANrx == CANrx):
            print(f'. ', end='')
        else:
            prev_CANrx = CANrx
            print(f'\nCAN Rx: {CANrx}', end='')
        if CANrx['id'] == CAN_STATUS_RC_STAT and len(CANrx['data']) > 0:
            if CANrx['data'][0] == 0: # Stopped
                moving = False
            else:
                can.send('motor', CAN_STATUS_RC_STAT, True)
                is_moving.last_rc_ping = time()
    
    if moving and (time() - is_moving.last_rc_ping) > 1:
        can.send('motor', CAN_STATUS_RC_STAT, True)
        is_moving.last_rc_ping = time()

    return moving

##### LOOP #####

spinner = 0
spinners = ['-', '\\', '|', '/']
print('READY\n-', end='')

while True:

    # now = time()

    spinner = (spinner + 1) % 4
    print(f'\b{spinners[spinner]}', end='')

    CANrx = can.get('main')
    if CANrx is not None:
        if(prev_CANrx == CANrx):
            print(f'. ', end='')
        else:
            prev_CANrx = CANrx
            print(f'\nCAN Rx: {CANrx}', end='')
        if CANrx['id'] == CAN_CMD_SUPER and len(CANrx['data']) > 0:
            if CANrx['data'][0] == SUPER_STATE and len(CANrx['data']) > 1:
                state = CANrx['data'][1]
                state_prev = -1
            elif CANrx['data'][0] == SUPER_SPEED and len(CANrx['data']) > 1:
                move_speed = CANrx['data'][1] / 10
                print(f'\nstate.speed = {move_speed}')
        if CANrx['id'] == CAN_CMD_HALT:
            logging.info('shutdown')
            state = CAN_CMD_HALT
            break
    
    state_changed = (state_prev != state)
    if state_changed:
        can.send('main', CAN_STATUS_SUPER, data=[state])
        if state < len(STATE_NAMES): 
            print(f'\n\n===== New state: {STATE_NAMES[state]} ({state}) =====')
        else:
            print(f'\n\n===== New state: {state} =====')
        state_prev = state
        sub_state = 0
        sub_state_prev = 0
        sub_state_changed = True
    else:
        sub_state_changed = (sub_state != sub_state_prev)
        if sub_state_changed:
            sub_state_prev = sub_state
            print(f'\n----- New sub_state: {sub_state} -----')

    ##### STATE_UNINIT #####

    if state == STATE_UNINIT:
        if state_changed:
            can.send('main', CAN_CMD_RC_MOVE, data=[0, 0, 0, 0])
            can.send('look', CAN_CMD_VISION, data=[VISION_PGM, VISION_PGM_IDLE])
            can.send('look', CAN_CMD_IMU, data=[0])
            can.send('look', CAN_CMD_HEADLIGHT, data=[0])
            can.del_filter('look', 'all',  'all')

    ##### STATE_INIT #####

    elif state == STATE_INIT:
        if state_changed:
            # can.send('main', CAN_ARM_SEQUENCE, [CAN_ARM_INIT, CAN_ARM_UNPARK, CAN_ARM_HOME, CAN_ARM_PARK, CAN_ARM_LIMP])
            sleep(2)

        state = STATE_READY

    ##### STATE_READY #####

    elif state == STATE_READY:
        if state_changed:
            speak_direct('Gidget is ready.')

    ##### STATE_START #####

    elif state == STATE_START:
        if state_changed:
            speak_direct('Hi ho? Hi ho. Off to the mini-frij I go.')
            # Send move commands
            move_dist(move_speed, 59, when='b', end='c')
            move_dist(move_speed, 91, turn='r', radius=200, when='b', end='c')
            move_dist(move_speed, 181, when='b', end='s')

        elif is_moving() == False:
            state = STATE_AT_HALL
            # state = STATE_READY

    ##### STATE_AT_HALL #####

    elif state == STATE_AT_HALL:
        if state_changed:
            can.add_filter('look', CAN_STATUS_IMU,  0x7FF)
            can.send('look', CAN_CMD_IMU, data=[50])
            sub_state = STATE_AT_HALL_MOVING

        if sub_state == STATE_AT_HALL_LOOKING:
            # Align to 90°
            CANrx = can.get('look')
            if CANrx is not None and len(CANrx['data']) >= 6:
                x = int.from_bytes([CANrx['data'][0], CANrx['data'][1]], byteorder='big', signed=True)
                if not sim_mode and x < 89.5:
                    if not is_moving():
                        move_dist(move_speed * 0.2, 90 - x, turn='r', radius=15, when='i', end='c')
                elif not sim_mode and x > 90.5:
                    if not is_moving():
                        move_dist(move_speed * 0.2, x - 90, turn='l', radius=15, when='i', end='c')
                else:
                    pass

        elif sub_state == STATE_AT_HALL_MOVING:
                    # Move to next waypoint - in view of frig
                    speak_direct('Oh mini-frij, I\'m coming to get you.')
                    move_dist(move_speed, 337, when='i', end='c')
                    move_dist(move_speed, 90, turn='r', radius=98, when='b', end='c')
                    move_dist(move_speed, 130, when='b', end='c')
                    move_dist(move_speed * 0.5, 18, when='b', end='s')
                    can.del_filter('look', 'all',  'all')
                    can.send('look', CAN_CMD_IMU, data=[0])
                    sub_state = STATE_AT_HALL_MOVING

        elif sub_state == STATE_AT_HALL_MOVING:
            if not is_moving():
                state = STATE_UNINIT

    ##### STATE_LOOK_FOR_FRIG #####

    elif state == STATE_LOOK_FOR_FRIG:
        if state_changed:
            rm_center_x.reset()
            rm_center_z.reset()
            rm_left_x.reset()
            rm_left_z.reset()
            can.add_filter('look', CAN_STATUS_FRIG,  0x7FF)
            can.send('look', CAN_CMD_VISION, data=[VISION_PGM, VISION_PGM_FRIG_FIND])
            sub_state = STATE_LOOK_FOR_FRIG_LOOKING if not sim_mode else STATE_LOOK_FOR_FRIG_MOVING

        if sub_state == STATE_LOOK_FOR_FRIG_LOOKING:
            CANrx = can.get('look')
            if CANrx is not None:
                if CANrx['id'] == CAN_STATUS_FRIG and len(CANrx['data']) == 8:
                    rm_center_x.add(int.from_bytes([CANrx['data'][0], CANrx['data'][1]], byteorder='big', signed=True))
                    center_x = rm_center_x.get()
                    rm_center_z.add(int.from_bytes([CANrx['data'][2], CANrx['data'][3]], byteorder='big', signed=True))
                    center_z = rm_center_z.get()
                    rm_left_x.add(int.from_bytes([CANrx['data'][4], CANrx['data'][5]], byteorder='big', signed=True))
                    left_x = rm_left_x.get()
                    rm_left_z.add(int.from_bytes([CANrx['data'][6], CANrx['data'][7]], byteorder='big', signed=True))
                    left_z = rm_left_z.get()
                    print(f"center: {center_x}, {center_z} left: {left_x}, {left_z}")
                    if center_x != None and center_z != None:
                        # is it far from the center
                        a = round(math.degrees(math.atan(center_x / center_z)))
                        print(f"angle: {a}")
                        if(a > 15): # turn right
                            move_dist(move_speed * 0.5, a/2, turn='r', radius = 0)
                            sub_state = STATE_LOOK_FOR_FRIG_ROTATING
                        elif(a < -15): # turn left
                            move_dist(move_speed * 0.5, -a/2, turn='l', radius = 0)
                            sub_state = STATE_LOOK_FOR_FRIG_ROTATING
                        else: # Okay, let's move to a point front-left of frig.
                            f = math.sqrt(math.pow(left_x - center_x, 2) + math.pow(left_z - center_z, 2))
                            fX = (center_x - left_x) # always +
                            fZ = (left_z - center_z)
                            tX = round((left_x - (FRIG_SETBACK_Z / f * fZ) - (FRIG_SETBACK_X / f * fX)) / 10)
                            tZ = round((left_z - (FRIG_SETBACK_Z / f * fX) + (FRIG_SETBACK_X / f * fZ)) / 10)
                            print(f"f mm: {f} fX mm: {fX} fZ mm: {fZ} tX cm: {tX} tZ cm: {tZ}")
                            # move_to(move_speed * 0.5, tX, tZ)
                            # move_dist(move_speed * 0.5, 6, turn='r', radius = 0)
                            sub_state = STATE_LOOK_FOR_FRIG_MOVING
                            # and we're done looking
                            can.send('look', CAN_CMD_VISION, data=[VISION_PGM, VISION_PGM_IDLE])
                            can.del_filter('look', 'all',  'all')

        elif sub_state == STATE_LOOK_FOR_FRIG_ROTATING:
            if not is_moving():
                rm_center_x.reset()
                rm_center_z.reset()
                rm_left_x.reset()
                rm_left_z.reset()
                sub_state = STATE_LOOK_FOR_FRIG_LOOKING

        elif sub_state == STATE_LOOK_FOR_FRIG_MOVING:
            if not is_moving():
                state = STATE_AT_FRIG

    ##### STATE_AT_FRIG #####

    elif state == STATE_AT_FRIG:
        if state_changed:
            can.add_filter('look', CAN_STATUS_IMU,  0x7FF)
            can.send('look', CAN_CMD_IMU, data=[50])
            sub_state = STATE_AT_FRIG_LOOKING

        if sub_state == STATE_AT_FRIG_LOOKING:
            # Align to 180°
            CANrx = can.get('look')
            if CANrx is not None and len(CANrx['data']) >= 6:
                x = int.from_bytes([CANrx['data'][0], CANrx['data'][1]], byteorder='big', signed=True)
                if not sim_mode and x < 179.5:
                    if not is_moving():
                        move_dist(move_speed * 0.2, 180 - x, turn='r', radius=15, when='i', end='c')
                elif not sim_mode and x > 180.5:
                    if not is_moving():
                        move_dist(move_speed * 0.2, x - 180, turn='l', radius=15, when='i', end='c')
                else:
                    can.del_filter('look', 'all',  'all')
                    can.send('look', CAN_CMD_IMU, data=[0])
                    state = STATE_OPEN_DOOR

    ##### STATE_OPEN_DOOR #####

    elif state == STATE_OPEN_DOOR:
        if state_changed:
            state = STATE_OPEN_DOOR

        if sub_state == STATE_OPEN_DOOR_LOOKING:
            CANrx = can.get('look')
            if CANrx is not None:
                if CANrx['id'] == CAN_STATUS_FRIG and len(CANrx['data']) == 4:
                    rm_left_x.add(int.from_bytes([CANrx['data'][0], CANrx['data'][1]], byteorder='big', signed=True))
                    left_x = rm_left_x.get()
                    print(f"left: {left_x}")
                    if left_x != None:
                        if(left_x > 520): # turn right
                            a = round((left_x - 520) / 10)
                            print(f'turn right {a}')
                            # move_dist(move_speed * 0.5, a, turn='r', radius = 0)
                            sub_state = STATE_OPEN_DOOR_POSITIONING
                        elif(left_x < 480): # turn left
                            a = round((480 - left_x) / 10)
                            print(f'turn left {a}')
                            # move_dist(move_speed * 0.5, a, turn='l', radius = 0)
                            sub_state = STATE_OPEN_DOOR_POSITIONING
                        else: # Okay, let's extend the arm and then turn to make contact
                            can.send('look', CAN_ARM_DOOR)
                            can.send('look', CAN_CMD_VISION, data=[VISION_PGM, VISION_PGM_IDLE])
                            can.del_filter('look', 'all',  'all')
                            sub_state = STATE_OPEN_DOOR_EXT_ARM

        elif sub_state == STATE_OPEN_DOOR_POSITIONING:
            if not is_moving():
                rm_left_x.reset()
                sub_state = STATE_OPEN_DOOR_LOOKING
                sleep(2)

        elif sub_state == STATE_OPEN_DOOR_EXT_ARM:
            if sub_state_changed:
                start = time()
            if time() - start > 4:
                sub_state = STATE_OPEN_DOOR_ROTATING

        elif sub_state == STATE_OPEN_DOOR_ROTATING:
            if sub_state_changed:
                # Tell arm to move until pressure is felt
                move_dist(move_speed * 0.5, 10, turn='r', radius = 0)
            elif not is_moving():
                sub_state = STATE_OPEN_DOOR_PULL_BACK

        elif sub_state == STATE_OPEN_DOOR_PULL_BACK:
            if sub_state_changed:
                move_dist(move_speed * 0.5, 10, dir='r', turn='s')
            elif not is_moving():
                sub_state = STATE_OPEN_DOOR_ROTATE

        elif sub_state == STATE_OPEN_DOOR_ROTATE:
            if sub_state_changed:
                move_dist(move_speed * 0.5, 45, dir='r', turn='l', radius = 30)
            elif not is_moving():
                state = STATE_REPOSITION

    ##### STATE_REPOSITION #####

    elif state == STATE_REPOSITION:
        # Do stuff
        sleep(1)

        state = STATE_GRAB_CAN

    ##### STATE_GRAB_CAN #####

    elif state == STATE_GRAB_CAN:
        if state_changed:
            can.add_filter('look', CAN_STATUS_FRIG,  0x7FF)
            can.send('look', CAN_CMD_VISION, data=[VISION_PGM, VISION_PGM_CAN])
            can.send('look', CAN_ARM_GRAB_CAN)
            # sub_state = STATE_GRAB_CAN_WAITING

        # state = STATE_UNINIT

    # dt = time() - now
    # print(f"{int(1.0/dt)} FPS")

    # Blink the LED
    hbLED.tickle()

    if state == CAN_CMD_QUIT or state == CAN_CMD_HALT:
        break

move_dist(0, 0)

if state == CAN_CMD_HALT:
    sleep(5)
    subprocess.run("sudo halt", shell=True)

print('\nGood-bye')
