"""
Methods for accessing the blackboard
"""

import json
import logging

class Blackboard:

    dict = {}
    log = []

    def __init__(self, caller):
        self.caller = caller

    def get(self, key):
        if key in Blackboard.dict:
            value = Blackboard.dict[key]
        else:
            value = None
        logging.info(f'{self.caller} get bb key {key} = {value}')
        return value

    def set(self, key, value):
        if key[0] == 'i':
            value = int(value)
        elif key[0] == 'f':
            value = float(value)
        logging.info(f'{self.caller} set bb key {key} = {value}')
        Blackboard.dict[key] = value
        Blackboard.log.append({'caller':self.caller, 'key':key, 'value':value})

    def delete(self, key):
        logging.info(f'{self.caller} del bb key {key}')
        if key in Blackboard.dict:
            del Blackboard.dict[key]

#     def addToQueue(self, queue, value):
#         logging.info(f'{self.caller} add {value} to queue {queue}')
#         return red.rpush(queue, value)
#
#     def CANtx(self, id, irf = False, dlc = None, data = None):
#         msg = {id: id}
#         if irf:
#             msg['irf'] = irf
#         if dlc != None:
#             msg['dlc'] = dlc
#         if data != None:
#             msg['data'] = data
#         return self.addToQueue('CANtx', json.dumps(msg))
#
#     def CANfilter(self, action, id, mask):
#         filter = {
#             action: action,
#             id: id,
#             mask: mask,
#         }
#         return self.addToQueue('CANfilter', json.dumps(filter))
    def getLog(self):
        l = Blackboard.log.copy()
        Blackboard.log = []
        return l

    def dump(self):
        return Blackboard.dict
