"""
Methods for accessing the blackboard
"""

import json
import logging
import redis_client

red = redis_client.init()

class Blackboard:
    def __init__(self, caller):
        self.caller = caller

    def get(self, key):
        value = red.get(key)
        if value != None:
            if key[0] == 'i':
                value = int(value)
            elif key[0] == 'f':
                value = float(value)
        logging.info(f'{self.caller.label} get bb key {key} = {value}')
        return value

    def set(self, key, value, ex=None, px=None):
        logging.info(f'{self.caller.label} set bb key {key} = {value}')
        return red.set(key, value, ex, px)

    def delete(self, key):
        logging.info(f'{self.caller.label} del bb key {key}')
        return red.delete(key)

    def addToQueue(self, queue, value):
        logging.info(f'{self.caller.label} add {value} to queue {queue}')
        return red.rpush(queue, value)

    def CANtx(self, id, irf = False, dlc = None, data = None):
        msg = {id: id}
        if irf:
            msg['irf'] = irf
        if dlc != None:
            msg['dlc'] = dlc
        if data != None:
            msg['data'] = data
        return self.addToQueue('CANtx', json.dumps(msg))

    def CANfilter(self, action, id, mask):
        filter = {
            action: action,
            id: id,
            mask: mask,
        }
        return self.addToQueue('CANfilter', json.dumps(filter))