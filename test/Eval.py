"""
Eval

data.js: {
    "name": "Eval",
    "icon": "fab fa-python",
    "cat": "condition",
    "help": "Evaluate a python expression and return the resulting state.",
    "params": [
        {
            "key": "eval",
            "label": "Evaluate",
            "kind": "textarea"
        }
    ]
}
"""

import logging

from ..Node import Node

class Eval(Node):
    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'eval'):
            raise RuntimeError(f'Missing param - {self.label}.eval')

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        try:
            rtn = eval(self.eval)
        except Exception as e:
            print(e)
            print(f'{self.eval} -> {rtn}', end='')
            rtn = Node.FAILURE
        return (rtn, None)
