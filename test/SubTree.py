"""
SubTree

data.js: {
    "name": "SubTree",
    "label": "{treeName}",
    "icon": "fas fa-tree",
    "cat": "composite",
    "params": [
        {
            "key": "treeName",
            "label": "Sub-tree",
            "kind": "select",
            "options": []
        }
    ]
}
"""

from ..Node import Node
from ..Composite import Composite

class SubTree(Composite):

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'treeName'):
            raise RuntimeError(f'Missing param - {self.label}.treeName')
