<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require('../../gdgt_shared/can_ids.php');

$speed = $_GET['s'];
$dir = $_GET['d'];

$redis = new Redis();
//Connecting to Redis
try {
	$redis->pconnect('/var/run/redis/redis-server.sock');
} catch(Exception $e) {
	die('{"error":"unable to connect to redis server"}');
}
// $laser = $redis->get('laser');
// $redis->set($k, $v);
$data = [];
if($dist) $data[] = 0;

$redis->rpush('CANtx', json_encode([
	'can_id' => CAN_CMD_RC_MOVE,
	'dlc' => count($data),
	'data' => $data
]));