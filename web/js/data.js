const SUCCESS = 1
const FAILURE = 2
const RUNNING = 3
const ABORT = -1

function replyText(reply) {
	switch(parseInt(reply)) {
	case SUCCESS:
		return 'Success';
	case FAILURE:
		return 'Failure';
	case RUNNING:
		return 'Running';
	case ABORT:
		return 'Abort';
	}
	return '?';
}

const nodeCats = {
	"composite": {
		"name": "Composites",
		"applyTo": [
			"root",
			"composite"
		],
	},
	"shieldDeco": {
		"name": "Shield Decorators",
		"applyTo": [
			"composite",
			"action"
		],
	},
	"replyDeco": {
		"name": "Reply Decorators",
		"applyTo": [
			"composite",
			"action"
		],
	},
	"execDeco": {
		"name": "Exec Decorators",
		"applyTo": [
			"composite",
			"action"
		],
	},
	"condition": {
		"name": "Conditions",
		"applyTo": [
			"root",
			"composite"
		],
	},
	"action": {
		"name": "Actions",
		"applyTo": [
			"root",
			"composite"
		],
	},
	"importFile": {
		"name": "Import",
		"applyTo": [
			"root",
			"composite"
		],
	},
};

const nodeTypes = {
	"root": {
		"name": "Start",
		"icon": "fas fa-map-marker",
		"cat": "root",
        "params": [
            {
                "key": "bbInit",
                "label": "Initalize BB<br>key=value (1 per line)",
                "kind": "textarea"
            }
        ]
	},
"Parallel": {
    "name": "Parallel",
    "icon": "fas fa-bars",
    "cat": "composite"
},
"Selector": {
    "name": "Selector",
    "icon": "far fa-question-circle",
    "cat": "composite",
    "help": "Execute each child in order until one returns Success.",
},
"SelectorMem": {
    "name": "Selector*",
    "icon": "fas fa-question-circle",
    "cat": "composite",
    "help": "Execute each child in order until one returns Success. Returns to Running child.",
},
"Sequence": {
    "name": "Sequence",
    "icon": "far fa-arrow-alt-circle-right",
    "cat": "composite",
    "help": "Execute each child in order until one returns Failure.",
},
"SequenceMem": {
    "name": "Sequence*",
    "icon": "fas fa-arrow-alt-circle-right",
    "cat": "composite",
    "help": "Execute each child in order until one returns Failure. Returns to Running child.",
},
"Blocker": {
    "name": "Blocker",
    "label": "Always reply {iReply}",
    "icon": "fas fa-shield-alt",
    "cat": "shieldDeco",
    "help": "Do not execute node and always return specified state.",
    "params": [
        {
            "key": "iReply",
            "label": "Reply",
            "kind": "select",
            "options": [
                {"value": SUCCESS, "text": "Success"},
                {"value": FAILURE, "text": "Failure"},
                {"value": RUNNING, "text": "Running"},
                {"value": ABORT, "text": "Abort"}
            ]
        },
    ]
},
"OnlyOnce": {
    "name": "Only Once",
    "icon": "fas fa-shield-alt",
    "cat": "shieldDeco",
    "help": "Return previously returned state after first Success or Failure",
},
"TestBBshield": {
    "name": "Test BB",
    "label": "If [{bbKey}] is {ibool} reply {iReply}",
    "icon": "fas fa-shield-alt",
    "cat": "shieldDeco",
    "help": "Return specified state if blackboard value is true/false without triggering node.",
    "params": [
        {
            "key": "bbKey",
            "label": "Return reply if<br>blackboard key",
            "kind": "key"
        },
        {
            "key": "compare",
            "label": "Is",
            "kind": "select",
            "options": [
                {"value": "=", "text": "="},
                {"value": "!=", "text": "!="},
                {"value": "<", "text": "<"},
                {"value": "<=", "text": "<="},
                {"value": ">", "text": ">"},
                {"value": ">=", "text": ">="},
                {"value": "null", "text": "is null"},
            ]
        },
        {
            "key": "value",
            "label": "Value"
        },
        {
            "key": "iReply",
            "label": "Reply",
            "kind": "select",
            "options": [
                {"value": SUCCESS, "text": "Success"},
                {"value": FAILURE, "text": "Failure"},
                {"value": RUNNING, "text": "Running"}
            ]
        },
    ]
},
"AlwaysFailure": {
    "name": "Always Failure",
    "icon": "fas fa-times-circle",
    "cat": "replyDeco",
    "help": "Reply is always Failure regardless of reply generated by node."
},
"AlwaysRunning": {
    "name": "Always Running",
    "icon": "fas fa-running",
    "cat": "replyDeco",
    "help": "Reply is always Running regardless of reply generated by node."
},
"AlwaysSuccess": {
    "name": "Always Success",
    "icon": "fas fa-check-circle",
    "cat": "replyDeco",
    "help": "Reply is always Success regardless of reply generated by node."
},
"Invert": {
    "name": "Invert",
    "icon": "fas fa-random",
    "cat": "replyDeco",
    "help": "Node's Success or Failure replies become Failure or Success, respectively."
},
"RunningOrFailure": {
    "name": "Running or Failure",
    "icon": "far fa-times-circle",
    "cat": "replyDeco",
    "help": "Reply is Running or Failure regardless of reply generated by node."
},
"RunningOrSuccess": {
    "name": "Running or Success",
    "icon": "far fa-check-circle",
    "cat": "replyDeco",
    "help": "Reply is Running or Success regardless of reply generated by node."
},
"Repeat": {
    "name": "Repeat N Times",
    "nodeLabel": "Repeat {i} Times",
    "icon": "fas fa-sync-alt",
    "cat": "execDeco",
    "params": [
        {
            "key": "i",
            "label": "Number of times",
            "default": "1",
            "kind": "integer"
        }
    ]
},
"RepeatUntilFailure": {
    "name": "Repeat Until Failure",
    "icon": "fas fa-sync-alt",
    "cat": "execDeco",
    "params": [
        {
            "key": "i",
            "label": "Maximum number of Successes",
            "kind": "integer"
        }
    ]
},
"RepeatUntilSuccess": {
    "name": "Repeat Until Success",
    "icon": "fas fa-sync-alt",
    "cat": "execDeco",
    "params": [
        {
            "key": "i",
            "label": "Maximum number of Failures",
            "kind": "integer"
        }
    ]
},
"TestBB": {
    "name": "Test BB Value",
    "label": "Is [{bbKey}] {compare} {value}",
    "icon": "fas fa-question",
    "cat": "condition",
    "params": [
        {
            "key": "bbKey",
            "label": "Return <strong>Success</strong> if<br>blackboard key",
            "kind": "key"
        },
        {
            "key": "compare",
            "label": "Is",
            "kind": "select",
            "options": [
                {"value": "=", "text": "="},
                {"value": "!=", "text": "!="},
                {"value": "<", "text": "<"},
                {"value": "<=", "text": "<="},
                {"value": ">", "text": ">"},
                {"value": ">=", "text": ">="},
                {"value": "null", "text": "is null"},
            ]
        },
        {
            "key": "value",
            "label": "Value"
        }
    ]
},
"Abort": {
    "name": "Abort",
    "icon": "fas fa-stop",
    "cat": "action",
    "help": "Say something, then abort.",
    "params": [
        {
            "key": "text",
            "label": "Speak Text",
            "kind": "textarea"
        },
    ]
},
"AlignHeading": {
    "name": "Align to Heading",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "help": "Compare heading to target and turn to match (±1°).",
    "params": [
        {
            "key": "iTargetHeading",
            "label": "Target Heading (°)",
            "kind": "integer",
            "default": 0,
        },
        {
            "key": "iSpeed",
            "label": "Speed (QPPS)",
            "kind": "integer",
            "default": 20,
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 10,
        },
    ]
},
"ApproachCan": {
    "name": "Approach Can",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "help": "Locate the can and move to a specific distance from it.",
    "params": [
        {
            "key": "iMinZ",
            "label": "Min Z (cm)",
            "kind": "integer",
            "default": 38,
        },
        {
            "key": "iMaxZ",
            "label": "Max Z (cm)",
            "kind": "integer",
            "default": 48,
        },
        {
            "key": "iMaxAng",
            "label": "Max ° from center",
            "kind": "integer",
            "default": 10,
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
},
"ApproachFrig": {
    "name": "Approach Frig",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "help": "Move to initial frig position. Target X and Z are distance from the front, left corner",
    "params": [
        {
            "key": "iMoveToX",
            "label": "Move to X (cm)",
            "kind": "integer",
            "default": 0,
        },
        {
            "key": "iMoveToZ",
            "label": "Move to Z (cm)",
            "kind": "integer",
            "default": -36,
        },
        {
            "key": "iTargetHeading",
            "label": "Target Heading (°)",
            "kind": "integer",
            "default": 0,
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
},
"Backup": {
    "name": "Backup",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "params": [
        {
            "key": "ims",
            "label": "Backup for ms",
            "kind": "integer",
        },
    ]
},
"CanOnTable": {
    "name": "Can on Table",
    "icon": "fas fa-hand",
    "cat": "action",
    "help": "Locate table and place can on top.",
    "params": [
        {
            "key": "iMinZ",
            "label": "Min Z (cm)",
            "kind": "integer",
            "default": 48,
        },
        {
            "key": "iMaxZ",
            "label": "Max Z (cm)",
            "kind": "integer",
            "default": 58,
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
},
"CenterOnHall": {
    "name": "Center on Hall",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "help": "Determine location in relationship to hall and adjust as needed.",
    "params": [
        {
            "key": "iNullZone",
            "label": "Null Zone Width (cm)",
            "kind": "integer",
            "default": 80,
        },
        {
            "key": "iHeading",
            "label": "Heading (°)",
            "kind": "integer",
            "default": 0,
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
},
"ElapseTime": {
    "name": "Elapsed Time",
    "icon": "fas fa-hourglass-half",
    "cat": "action",
    "help": "Time an event.",
    "params": [
        {
            "key": "iEvent",
            "label": "Event",
            "kind": "select",
            "options": [
                {"value": 1, "text": "Start"},
                {"value": 2, "text": "End"}
            ]
        }
    ]
},
"GetHeading": {
    "name": "Get Heading",
    "icon": "fas fa-compass",
    "cat": "action",
    "help": "Get the current heading and save it in the blackboard.",
    "params": [
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
},
"GetStatus": {
    "name": "Get Status",
    "icon": "fas fa-circle-question",
    "cat": "action",
    "help": "Send a CAN status command and wait for reply.",
    "params": [
        {
            "key": "hStatusCANid",
            "label": "Status CAN ID 0x",
        },
        {
            "key": "bbKey",
            "label": "BB Key to set",
            "kind": "key"
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
},
"GrabCan": {
    "name": "Grab Can",
    "icon": "fas fa-hand",
    "cat": "action",
    "help": "Locate and grab the can.",
    "params": [
        {
            "key": "iMinZ",
            "label": "Min Z (cm)",
            "kind": "integer",
            "default": 38,
        },
        {
            "key": "iMaxZ",
            "label": "Max Z (cm)",
            "kind": "integer",
            "default": 48,
        },
        {
            "key": "iMaxAng",
            "label": "Max ° from center",
            "kind": "integer",
            "default": 10,
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
},
"IncrementBB": {
    "name": "Inc BB Value",
    "label": "[{bbKey}] += {iInc}",
    "icon": "fas fa-cog",
    "cat": "action",
    "params": [
        {
            "key": "bbKey",
            "label": "Key to set",
            "kind": "key"
        },
        {
            "key": "iInc",
            "label": "± Value",
            "default": 1
        }
    ]
},
"LocateCan": {
    "name": "Locate Can",
    "icon": "fas fa-eye",
    "cat": "action",
    "help": "Locate the can and save it in the blackboard.",
    "params": [
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
},
"LocateFrig": {
    "name": "Locate Frig",
    "icon": "fas fa-eye",
    "cat": "action",
    "help": "Locate the frig and save it in the blackboard.",
    "params": [
        {
            "key": "hStatusCANid",
            "label": "Range",
            "kind": "select",
            "options": [
                {"value": "0x1d4", "text": "Close"},
                {"value": "0x1d3", "text": "Far"},
            ]
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
},
"Move": {
    "name": "Move",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "params": [
        {
            "key": "iType",
            "label": "Type",
            "kind": "select",
            "options": [
                {"value": "0x209", "text": "Distance"},
                {"value": "0x20a", "text": "Angle"}
            ]
        },
        {
            "key": "iDistAng",
            "label": "Distance (cm) or Angle (°)",
            "kind": "integer"
        },
        {
            "key": "dir",
            "label": "Direction",
            "kind": "select",
            "options": [
                {"value": "f", "text": "Forward"},
                {"value": "r", "text": "Reverse"}
            ],
            "default": "f"
        },
        {
            "key": "turn",
            "label": "Turn",
            "kind": "select",
            "options": [
                {"value": "s", "text": "Straight"},
                {"value": "l", "text": "Left"},
                {"value": "r", "text": "Right"}
            ],
            "default": "s"
        },
        {
            "key": "iRadius",
            "label": "Radius (cm)",
            "kind": "integer"
        },
        {
            "key": "when",
            "label": "When",
            "kind": "select",
            "options": [
                {"value": "i", "text": "Immediate"},
                {"value": "b", "text": "Buffered"}
            ],
            "default": "i"
        },
        {
            "key": "end",
            "label": "End",
            "kind": "select",
            "options": [
                {"value": "s", "text": "Stop"},
                {"value": "c", "text": "Coast"}
            ],
            "default": "s"
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 10
        },
    ]
},
"Moves": {
    "name": "Moves",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "help": "JSON data = array of objects with these values:<br>\
    dist = distance (cm) or<br>\
    ang = angle (°) and<br>\
    rad = radius (cm)<br>\
    sp = speed† (m/s)<br>\
    sf = speed factor† (* sys speed)<br>\
    turn = turn† (r/l)<br>\
    dir = direction† (f/r)<br>\
    end = end† (c/s - last always s)<br>\
    † = optional - 1st value is default\
    ",
    "params": [
        {
            "key": "moves",
            "label": "Moves JSON data",
            "kind": "textarea",
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 10
        },
    ]
},
"Nop": {
    "name": "Do Nothing",
    "icon": "far fa-circle",
    "cat": "action",
    "params": [
        {
            "key": "iReply",
            "label": "Reply",
            "kind": "select",
            "options": [
                {"value": SUCCESS, "text": "Success"},
                {"value": FAILURE, "text": "Failure"},
                {"value": RUNNING, "text": "Running"}
            ]
        }
    ]
},
"OpenFrig": {
    "name": "Open Frig",
    "icon": "fas fa-hand",
    "cat": "action",
    "help": "Align with the frig and open the door. Contact point is on the side, measured from the front, left corner.",
    "params": [
        {
            "key": "iDepth",
            "label": "Contact point (cm)",
            "kind": "integer",
            "default": 5,
        },
        {
            "key": "iBackup",
            "label": "Backup (cm)",
            "kind": "integer",
            "default": 15,
        },
        {
            "key": "iSpin",
            "label": "Spin (°)",
            "kind": "integer",
            "default": 30,
        },
    ]
},
"Pause": {
    "name": "Pause",
    "label": "Pause {fSecs} seconds",
    "icon": "fas fa-hourglass-half",
    "cat": "action",
    "params": [
        {
            "key": "fSecs",
            "label": "Seconds",
            "kind": "float",
        },
        {
            "key": "iReply",
            "label": "Reply",
            "kind": "select",
            "options": [
                {"value": SUCCESS, "text": "Success"},
                {"value": FAILURE, "text": "Failure"}
            ]
        }
    ]
},
"ScanForCan": {
    "name": "Scan for Can",
    "icon": "fas fa-eye",
    "cat": "action",
    "help": "Look for the can, rotating if needed.",
    "params": [
        {
            "key": "iMaxAng",
            "label": "Max ° from center",
            "kind": "integer",
            "default": 10,
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
},
"SendCAN": {
    "name": "Send CAN",
    "icon": "fas fa-paper-plane",
    "cat": "action",
    "help": "Send a CAN command.",
    "params": [
        {
            "key": "hCANid",
            "label": "Send CAN ID 0x",
        },
        {
            "key": "CANdata",
            "label": "Send Data (space-delimited)",
        },
    ]
},
"SendCANAndWait": {
    "name": "Send CAN & Wait",
    "icon": "fas fa-paper-plane",
    "cat": "action",
    "help": "Send a CAN command and wait for status.",
    "params": [
        {
            "key": "hActCANid",
            "label": "Send CAN ID 0x",
        },
        {
            "key": "actCANdata",
            "label": "Send Data (space-delimited)",
        },
        {
            "key": "hStatusCANid",
            "label": "Status CAN ID 0x",
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
},
"SetBB": {
    "name": "Set BB Value",
    "label": "Set [{bbKey}] to {bbValue}",
    "icon": "fas fa-cog",
    "cat": "action",
    "params": [
        {
            "key": "bbKey",
            "label": "Key to set",
            "kind": "key"
        },
        {
            "key": "bbValue",
            "label": "Value"
        }
    ]
},
"Speak": {
    "name": "Speak",
    "icon": "fas fa-volume-low",
    "cat": "action",
    "help": "Speak from a group, a specific phrase or sound, or speak ad-hoc text.",
    "params": [
        {
            "key": "idgroup",
            "label": "Speech Group ID",
            "kind": "integer",
        },
        {
            "key": "idstring",
            "label": "String or Sound ID",
            "kind": "integer",
        },
        {
            "key": "text",
            "label": "Speak Text",
            "kind": "textarea"
        },
    ]
},
"Wander": {
    "name": "Wander",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "params": [
        {
            "key": "iturnrate",
            "label": "Turn Rate",
            "kind": "integer",
            "default": 1,
        },
    ]
},

};
