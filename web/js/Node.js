class Node {
    id;
    parent;
    type;
    label;
    nodeLabel;
    note;
    replyDeco;
    shieldDeco;
    execDeco;
    params;
    order;
    isDisabled;
    children = [];
    col;
    level = 0;
    height;
    bottom;
    $dom;
    $conn;
    hideChildren;

    constructor(data, parent) {
        _(data.type);
        this.id = data.id;
        this.parent = parent;
        this.type = data.type;
        this.label = data.label || (nodeTypes[this.type] ? nodeTypes[this.type].label : this.type);
        this.nodeLabel = data.nodeLabel || nodeTypes[this.type].label || nodeTypes[this.type].name;
        this.note = data.note;
        this.replyDeco = data.replyDeco || {};
        this.shieldDeco = data.shieldDeco || {};
        this.execDeco = data.execDeco || {};
        this.params = data.params || {};
        this.order = data.order;
        this.isDisabled = data.isDisabled;
        this.hideChildren = data.hideChildren;

        if ($.isEmptyObject(this.params) && nodeTypes[this.type].params) {
            for (var param of nodeTypes[this.type].params) {
                this.params[param.key] = param.default;
            }
        }

        this.draw();

        for (var cat of ['replyDeco', 'shieldDeco', 'execDeco']) {
            $.each(this[cat], (dtype, deco) => {
                this.addDeco(dtype, deco);
            });
        }

        var n = parseInt(data.id.substr(1));
        if (n > lastNodeId) lastNodeId = n;

        if (data.children) {
            for (var child of data.children) {
                this.addChild(new Node(child, this));
            }
        }
    }

    import(filename) {
        var that = this;
        websocket.send(
            'open',
            {
                name: filename
            },
            function (reply) {
                console.log('reply: ', reply);
                if (reply.error) {
                    alert(`Failed to load ${filename}: ${reply.error}.`);
                    return;
                }
                var tree = that.reassignIds(reply.data.children[0]);
                tree.order = 999;
                _(tree);
                that.addChild(new Node(tree, that));
                // root.reassignIds();
                repositionNodes();
                // centerRoot();
            }
        );
    }

    reassignIds(dataNode) {
        if(dataNode.id != 'root') {
            lastNodeId += 1;
            dataNode.id = 'n' + lastNodeId;
        }
        if (dataNode.children) {
            for (var child of dataNode.children) {
                this.reassignIds(child);
            }
        }
        return dataNode;
    }

    draw() {
        // Draw node and connector on the canvas
        var label;
        if (this.nodeLabel) label = this.nodeLabel;
        else if (this.label) label = this.label;
        else if (nodeTypes[this.type].label) label = nodeTypes[this.type].label;
        else label = nodeTypes[this.type].name;
        var data = {
            id: this.id,
            label: label,
            icon: nodeTypes[this.type].icon,
            class: nodeTypes[this.type].cat + (this.isDisabled ? ' isDisabled' : ''),
            isDisabled: this.isDisabled
        };
        // Add tree node to canvas
        $('#canvas').append(Mustache.render($(this.type == 'root' ? '#root_template' : '#node_template').html().trim(), data));
        this.$dom = $(`#${this.id}`);
        this.$dom.data('node', this);

        if (this.id == 'root') return;

        // Add connector between node and parent
        data = {
            id: `${this.id}conn`,
            d: ''
        };
        $('#canvas').append(Mustache.render($('#connector_template').html().trim(), data));
        this.$conn = $(`#${this.id}conn`);
    }

    addDeco(dtype, deco, reselect) {
        dirty(true);

        var cat = nodeTypes[dtype].cat;
        if (deco) {
            this[cat][dtype] = deco;
        } else {
            this[cat][dtype] = {};
            if (nodeTypes[dtype].params) {
                for (var param of nodeTypes[dtype].params) {
                    this[cat][dtype][param.key] = param.default;
                }
            }
        }
        var label = nodeTypes[dtype].name;
        if (nodeTypes[dtype].nodeLabel) {
            label = nodeTypes[dtype].nodeLabel;
            $.each(this[cat][dtype], (k, v) => {
                if (k == 'ireply') v = replyText(v);
                else if (k == 'ibool') v = v == 1 ? 'T' : 'F';
                if (v !== '') label = label.replace(`{${k}}`, v);
            })
        }
        var data = {
            type: dtype,
            label: label,
            icon: nodeTypes[dtype].icon,
        };
        this.$dom.find(`.${cat}`).append(Mustache.render($('#deco_template').html().trim(), data));

        if (reselect) {
            // root.setColumns();
            this.height = this.$dom.outerHeight();
            if (!rowHeights[this.level] || this.height > rowHeights[this.level]) rowHeights[this.level] = this.height;
            root.reposition();
            this.select();
        }
    }

    addChild(node) {
        dirty(true);

        this.children.push(node);
        this.children.sort((a, b) => {
            return a.order - b.order;
        });
        $.each(this.children, (index, child) => {
            child.order = index;
        });
        this.$dom.addClass('showChildren');
    }

    // Function to recursively assign nodes to even-numbered columns in a left-justified fashion.
    // This approach is conservative. It does not tuck lower levels under the levels above.
    setColumns(col, level) {
        this.col = col || 0;
        this.level = level || 0;

        if (this.level == 0) rowColCounts = [];
        if (typeof rowColCounts[this.level] == 'undefined') rowColCounts[this.level] = -2;
        if (this.col - 2 < rowColCounts[this.level]) this.col = rowColCounts[this.level] + 2;
        rowColCounts[this.level] = this.col;

        this.height = this.$dom.outerHeight();
        if (!rowHeights[this.level] || this.height > rowHeights[this.level]) rowHeights[this.level] = this.height;

        if (!this.children.length || this.hideChildren) return;

        var next = this.col - (this.children.length - 1);
        for (var child of this.children) {
            child.setColumns(next, this.level + 1);
            next = 0;
        }
        this.col = (this.children[this.children.length - 1].col - this.children[0].col) / 2 + this.children[0].col;
        rowColCounts[this.level] = this.col;
        return next;
    }

    countDecendants() {
        var count = 0;
        for (var child of this.children) {
            count += 1 + child.countDecendants();
        }
        return count;
    }

    reposition() {
        // Position node and connector on the canvas
        var top = CANVAS_PADDING + canvasOffset.top;
        for (var i = 0; i < this.level; i++) top += rowHeights[i] + ROW_SPACING;
        var left = this.col * COL_WIDTH + CANVAS_PADDING + canvasOffset.left;
        this.$dom.css({ top: top, left: left });
        this.bottom = top + this.height;

        if (this.type == 'root' || left + NODE_WIDTH + CANVAS_PADDING > canvasDims.width) canvasDims.width = left + NODE_WIDTH + CANVAS_PADDING;
        if (this.type == 'root' || top + rowHeights[this.level] + CANVAS_PADDING > canvasDims.height) canvasDims.height = top + rowHeights[this.level] + CANVAS_PADDING;

        if (this.type != 'root') {
            var width, height, d;
            height = top - this.parent.bottom;
            top = this.parent.bottom;
            if (this.col <= this.parent.col) { // node is left or directly below parent
                left += NODE_WIDTH / 2 - 1;
                width = (this.parent.col - this.col) * COL_WIDTH + 2;
                d = `M 1 ${height} C 1 ${(height / 3)} ${(width - 1)} ${(height - height / 3)} ${(width - 1)} 0`;
            } else { // node is right of parent
                left = this.parent.col * COL_WIDTH + NODE_WIDTH / 2 - 1 + CANVAS_PADDING + canvasOffset.left;
                width = (this.col - this.parent.col) * COL_WIDTH + 2;
                d = `M 1 0 C 1 ${(height - height / 3)} ${(width - 1)} ${(height / 3)} ${(width - 1)} ${height}`;
            }
            this.$conn.css({ top: top, left: left }).attr('width', width).attr('height', height);
            this.$conn.find('path').attr('d', d);
        }

        if (!this.children.length || this.hideChildren) return;

        for (var child of this.children) {
            child.reposition();
        }

        if (this.type == 'root') {
            _(canvasDims);
            $('#canvas').css(canvasDims);
        }
    }

    unselect() {
        this.$dom.removeClass('selected');
        $('.applicable').removeClass('applicable');
        $('#inspector').empty();
        setTimeout(() => {
            if (!selectedNode) {
                $('#left_panel').animate({ left: -180 });
                $('#right_panel').animate({ right: -180 });
            }
        }, 100);
    }

    select() {
        selectedNode = this;
        lastSelectedNode = this;

        bringNodeIntoView(this);

        // Mark node as selected
        this.$dom.removeClass('state1 state2 state3').addClass('selected');

        // Display form
        this.displayForm();

        // Root node can have only one child
        if (this.type == 'root' && this.children.length > 0) return;

        // Show applicable on left panel
        $('.applyTo_' + nodeTypes[this.type].cat).addClass('applicable');
        if (!$.isEmptyObject(this.replyDeco)) {
            $('.type.replyDeco').removeClass('applicable');
        }
    }

    selectNext(dir) {
        if (dir == ARROW_UP) {
            if (this.parent) {
                this.unselect();
                this.parent.prevChild = this;
                this.parent.select();
            }
        } else if (dir == ARROW_LEFT) {
            if (this.parent) {
                var sibs = this.parent.getSiblings(this);
                if (sibs && sibs.left) {
                    this.unselect();
                    sibs.left.select();
                }
            }
        } else if (dir == ARROW_RIGHT) {
            if (this.parent) {
                var sibs = this.parent.getSiblings(this);
                if (sibs && sibs.right) {
                    this.unselect();
                    sibs.right.select();
                }
            }
        } else if (dir == ARROW_DOWN) {
            if (this.children.length && !this.hideChildren) {
                this.unselect();
                (this.prevChild ? this.prevChild : this.children[0]).select();
            }
        }
    }

    getSiblings(child) {
        var j = -1;
        for (var i = 0; i < this.children.length; i++) {
            if (this.children[i] == child) {
                j = i;
                break;
            }
        }
        if (j == -1) return null; // Should never happen
        return {
            left: j > 0 ? this.children[j - 1] : null,
            right: j < (this.children.length - 1) ? this.children[j + 1] : null
        };
    }

    move(dir) {
        if (!this.parent) return;
        var moved = this.parent.moveChild(this, dir);
        if (moved) {
            dirty(true);
            repositionNodes();
        }
    }

    moveChild(child, dir) {
        var j = -1;
        for (var i = 0; i < this.children.length; i++) {
            if (this.children[i] == child) {
                j = i;
                break;
            }
        }
        if (j == -1) return false; // Should never happen
        if (dir == ARROW_LEFT) {
            if (j == 0) return false;
            child.order -= 1.5;
        } else if (dir == ARROW_RIGHT) {
            if (j == (this.children.length - 1)) return false;
            child.order += 1.5;
        }
        this.children.sort((a, b) => {
            return a.order - b.order;
        });
        $.each(this.children, (index, child) => {
            child.order = index;
        });
        return true;
    }

    toggleChildren(force) { // force = true = hide children
        if (typeof force == 'undefined') this.hideChildren = !this.hideChildren;
        else this.hideChildren = force;

        this.displayChildren(!this.hideChildren);
        if (this.hideChildren) this.$dom.removeClass('showChildren').addClass('hideChildren');
        else this.$dom.removeClass('hideChildren').addClass('showChildren');
        root.setColumns();
        root.reposition();
        dirty(true);
    }

    toggleIsDisabled() {
        this.isDisabled = !this.isDisabled;
        if (this.isDisabled) {
            this.$dom.addClass('isDisabled');
            if (nodeTypes[this.type].cat == 'composite') this.toggleChildren(true);
        } else this.$dom.removeClass('isDisabled');
        dirty(true);
    }

    displayChildren(state) {
        if (state && this.hideChildren) return;
        $.each(this.children, (i, child) => {
            if (state) {
                child.$dom.show();
                child.$conn.show();
            } else {
                child.$dom.hide();
                child.$conn.hide();
                if (child == selectedNode) {
                    child.unselect();
                    selectedNode = null;
                }
            }
            child.displayChildren(state);
        });
    }

    displayForm() {
        // Empty the form
        $('#inspector').empty();

        // Display the node form elements
        var fields = '';
        if (nodeTypes[this.type].params) {
            $.each(nodeTypes[this.type].params, (i, param) => {
                if (param.kind == 'select') {
                    var value = this.params[param.key] ? this.params[param.key] : param.default;
                    for (var opt of param.options) opt.selected = opt.value == value ? 'selected' : '';
                    var data = {
                        label: param.label,
                        name: param.key,
                        key: param.key,
                        options: param.options
                    };
                    fields += Mustache.render($('#form_select_template').html().trim(), data);
                } else if (param.kind == 'cb') {
                    var value = this.params[param.key] ? this.params[param.key] : param.default;
                    var data = {
                        label: param.label,
                        name: param.key,
                        key: param.key,
                        value: param.value,
                        notvalue: param.notvalue,
                        checked: value == param.value ? 'checked' : ''
                    };
                    fields += Mustache.render($('#form_cb_template').html().trim(), data);
                } else {
                    var data = {
                        label: param.label,
                        name: param.key,
                        key: param.key,
                        value: (this.params[param.key] || this.params[param.key] === 0) ? this.params[param.key] : param.default
                    };
                    var tname = param.kind == 'textarea' ? '#form_textarea_template' : '#form_field_template';
                    fields += Mustache.render($(tname).html().trim(), data);
                }
            });
        }

        var data = {
            name: nodeTypes[this.type].name,
            icon: nodeTypes[this.type].icon,
            help: nodeTypes[this.type].help,
            nodeTitle: nodeTypes[this.type].label,
            id: this.id,
            label: this.label,
            note: this.note,
            cat: this.type == 'root' ? 'root' : nodeTypes[this.type].cat,
            isComp: nodeTypes[this.type].cat == 'composite',
            notRoot: this.type != 'root',
            fields: fields
        };
        $('#inspector').append(Mustache.render($('#node_form_template').html().trim(), data));

        // Display the decorators
        for (var cat of ['replyDeco', 'shieldDeco', 'execDeco']) {
            if (this[cat]) {
                $.each(this[cat], (dtype, deco) => {
                    $(`.${cat}_section`).show();

                    var fields = '';
                    if (nodeTypes[dtype].params) {
                        $.each(nodeTypes[dtype].params, (i, param) => {
                            if (param.kind == 'select') {
                                var value = deco[param.key] ? deco[param.key] : param.default;
                                for (var opt of param.options) opt.selected = opt.value == value ? 'selected' : '';
                                var data = {
                                    label: param.label,
                                    name: `${dtype}_${param.key}`,
                                    key: param.key,
                                    options: param.options
                                };
                                fields += Mustache.render($('#form_select_template').html().trim(), data);
                            } else {
                                var data = {
                                    label: param.label,
                                    name: `${dtype}_${param.key}`,
                                    key: param.key,
                                    value: deco[param.key] ? deco[param.key] : param.default
                                };
                                fields += Mustache.render($('#form_field_template').html().trim(), data);
                            }
                        });
                    }
                    var data = {
                        type: dtype,
                        name: nodeTypes[dtype].name,
                        icon: nodeTypes[dtype].icon,
                        help: nodeTypes[dtype].help,
                        //                         label: deco.label,
                        note: deco.note,
                        fields: fields
                    };
                    $(`#${cat}`).append(Mustache.render($('#deco_form_template').html().trim(), data));
                });
            }
        }
        //         $('#left_panel, #right_panel').show();
        $('#files_panel').animate({ left: -180 });
        filesOpen = false;
        $('#left_panel').animate({ left: 0 });
        $('#right_panel').animate({ right: 0 });
        //         $('#portal').css('left', 180).css('right', 180);
    }

    updateField(field) {
        dirty(true);

        var name = $(field).attr('name').split('_');
        var value = $(field).val();
        //         _(name);
        if (name.length == 1) {
            var v = value.trim();
            if ($(field).attr('type') == 'checkbox') {
                if (!field.checked) v = $(field).data('notvalue');
                // 				_(`checkbox ${name} ${v}`);
            }
            if (name[0] == 'typeSelect') {
                if (this.label == nodeTypes[this.type].label) this.label = nodeTypes[v].label;
                this.type = v;
                clearSelectedNode(this);
            } else if (name[0] == 'label') this.label = v;
            else if (name[0] == 'note') this.note = v;
            else {
                if (name[0].charAt(0) == 'i') v = parseInt(v);
                else if (name[0].charAt(0) == 'f') v = parseFloat(v);
                this.params[name[0]] = v;
            }
            var nodeLabel;
            if (this.label) nodeLabel = this.label;
            else if (nodeTypes[this.type].label) nodeLabel = nodeTypes[this.type].label;
            else nodeLabel = nodeTypes[this.type].name;
            if (nodeTypes[this.type].params) {
                $.each(nodeTypes[this.type].params, (i, param) => {
                    var v = this.params[param.key];
                    if (param.key == 'ireply') v = replyText(v);
                    else if (param.key == 'ibool') v = v == 1 ? 'T' : 'F';
                    if (v !== '') nodeLabel = nodeLabel.replace(`{${param.key}}`, v);
                });
            }
            this.nodeLabel = nodeLabel;
            this.$dom.find('div.node_label span.label').text(nodeLabel);
            _(this.params);
        } else {
            var cat = nodeTypes[name[0]].cat;
            var v = value.trim();
            if (name[1].charAt(0) == 'i') v = parseInt(v);
            else if (name[1].charAt(0) == 'f') v = parseFloat(v);
            this[cat][name[0]][name[1]] = v;
            if (nodeTypes[name[0]].nodeLabel) {
                var label = nodeTypes[name[0]].nodeLabel;
                if (nodeTypes[name[0]].params) {
                    $.each(nodeTypes[name[0]].params, (i, param) => {
                        var v = this[cat][name[0]][param.key];
                        if (param.key == 'ireply') v = replyText(v);
                        else if (param.key == 'ibool') v = v == 1 ? 'T' : 'F';
                        if (v !== '') label = label.replace(`{${param.key}}`, v);
                    });
                }
                // 	            this.label = label;
                this.$dom.find(`div.decorator[data-type="${name[0]}"] span.label`).text(label);
            }
        }
    }

    relocate(btn, evt, parent) {
        if (parent === 'cancel') {
            selectMode = 'node';
            $('#slip').remove();
            $('.node').addClass('clickable');
        } else if (parent) {
            dirty(true);
            this.parent.deleteChild(this);
            this.order = 999;
            parent.addChild(this);
            this.parent = parent;
            repositionNodes();
            selectMode = 'node';
            $('#slip').remove();
            $('.node').addClass('clickable');
        } else {
            selectMode = 'relocate';
            $('.node.clickable').removeClass('clickable');
            $('.node.composite').addClass('clickable');
            this.parent.$dom.removeClass('clickable');
            $('#portal').append($('#relocate_template').html().trim());
        }
        evt.stopPropagation();
    }

    delete(btn, evt) {
        // Remove node and connector from the canvas
        if (!evt.altKey) {
            var decendants = this.countDecendants();
            var msg = "Delete this node";
            if (decendants > 0) msg += ` and ${decendants} decendant${decendants > 0 ? 's' : ''}`;
            msg += '?\n\nThere is no undo for this operation.';
            if (!confirm(msg)) return;
        }
        dirty(true);
        clearSelectedNode();
        lastSelectedNode = null;
        this.deleteChildrenAndNode();
        this.parent.deleteChild(this);
        repositionNodes();
    }

    deleteChildrenAndNode() {
        for (var child of this.children) {
            child.deleteChildrenAndNode();
        }
        // Delete references to children
        this.children = [];
        // Delete node
        this.$dom.remove();
        //Delete connector
        this.$conn.remove();
    }

    deleteChild(child) {
        this.prevChild = null;
        for (var i = 0; i < this.children.length; i++) {
            if (this.children[i] == child) {
                this.children.splice(i, 1);
                this.children.sort((a, b) => {
                    return a.order - b.order;
                });
                $.each(this.children, (index, child) => {
                    child.order = index;
                });
                break;
            }
        }
    }

    deleteDeco(btn, evt) {
        _(evt);
        // Remove decorator from node
        var type = $(btn).closest('.deco_form').data('type');
        if (!evt.altKey) {
            var msg = `Delete the ${nodeTypes[type].label} decorator?\n\nThere is no undo for this operation.`;
            if (!confirm(msg)) return;
        }
        dirty(true);
        // Delete from form
        $(btn).closest('.deco_form').remove();
        // Delete from node
        this.$dom.find(`div.decorator[data-type="${type}"]`).remove();
        // Delete from data
        delete this[nodeTypes[type].cat][type];
        repositionNodes();
    }

    node_note(btn, evt) {
        $(btn).closest('div.node_form').find('div.note').toggle();
    }

    deco_note(btn, evt) {
        $(btn).closest('div.deco_form').find('div.note').toggle();
    }

    save() {
        var data = {};
        var propsToCopy = [
            'id',
            'cat',
            'type',
            'label',
            'nodeLabel',
            'order',
            'isDisabled',
            'hideChildren',
            'note',
            'replyDeco',
            'shieldDeco',
            'execDeco',
            'params'
        ];
        this.cat = nodeTypes[this.type].cat;
        for (var prop of propsToCopy) {
            if (typeof this[prop] == 'undefined') continue;
            if (typeof this[prop] == 'object' && $.isEmptyObject(this[prop])) continue;
            data[prop] = this[prop];
        }
        if (this.children.length) {
            data.children = [];
            for (var child of this.children) {
                data.children.push(child.save());
            }
        }
        return data;
    }

    tick(state, sourceId) {
        _(this.id, this.label, state, sourceId);
        this.$dom.removeClass('state1 state2 state3').addClass(`state${state}`);
        var childs;
        if (sourceId === 'all') {
            childs = this.children;
        } else if (sourceId) {
            for (var i = 0; i < this.children.length; i++) {
                if (this.children[i].id == sourceId) {
                    childs = this.children.slice(i + 1);
                    break;
                }
            }
        }
        _('childs', childs);
        if (childs) {
            for (var i = 0; i < childs.length; i++) {
                childs[i].tick(0, 'all');
            }
        }
    }

    swap(elm, evt) {
        var options = [];
        var thisNode = this;
        $.each(nodeTypes, (type, nodeType) => {
            if (nodeType.cat == 'composite') {
                options.push({ value: type, text: nodeType.name, selected: thisNode.type == type ? 'selected' : '' });
            }
        });
        var $form_field = $(elm).closest('.form_field');
        $form_field.children().hide();
        $form_field.append(Mustache.render($('#swap_select_template').html().trim(), { options: options }));
    }
}
