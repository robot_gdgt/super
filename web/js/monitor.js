"use strict"

const websocket = new WebSocketClient();

websocket.onmessage = function (msg, klass) {
	_(msg);
	_(msg.data);
	if (msg.type == 'bt' || msg.type == 'tick') return;
	var s = msg.type;
	for (const k in msg.data) {
		_(k, msg.data[k]);
		s += ` | ${k} = ${JSON.stringify(msg.data[k])}`;
	};
	const $d = $(Mustache.render($('#portal_row_template').html().trim(), {
		class: klass,
		text: s
	}));
	$('#portal').append($d);
	$d.get(0).scrollIntoView();
}

class FileHandler {
	filename;
	isDirty = false;

	constructor() {
	}

	do(cmd) {
		switch (cmd) {
			case 'connect':
				websocket.send(
					'connect',
					{},
					function (reply) {
						_('reply: ', reply);
					}
				);
				break;
			case 'get':
				if (this.isDirty) {
					if (!confirm('There are unsaved changes.\n\nDiscard the changes?')) return false;
				}
				if (cmd == 'new') {
					this.filename = 'Untitled';
					$('#filename').text(`File: ${this.filename}`);
					selectedNode = null;
					lastSelectedNode = null;
					$('#canvas').empty();
					lastNodeId = 0;
					root = new Node({
						id: 'root',
						type: "root",
						label: "Start",
					}, null);
					repositionNodes();
					dirty(false);
				} else {
					if (!filename) {
						filename = prompt("Enter a filename:", Cookies.get('lastOpenFile'));
						_(filename);
						if (!filename || !filename.trim()) return false;
						console.warn('we got here');
						filename = filename.trim();
					}
					var that = this;
					websocket.send(
						'open',
						{
							name: filename
						},
						function (reply) {
							_('reply: ', reply);
							if (reply.error) {
								alert(`Failed to load ${filename}: ${reply.error}.`);
								return;
							}
							that.filename = filename;
							Cookies.set('lastOpenFile', filename, { expires: 365 })
							$('#filename').text(`File: ${filename}`);
							selectedNode = null;
							lastSelectedNode = null;
							$('#canvas').empty();
							lastNodeId = 0;
							root = new Node(reply.data);

							// Set columns from root node on down.
							root.setColumns();

							var canvasWidth = $('#canvas').innerWidth();
							canvasOffset.left = (canvasWidth / 2) - ((root.col * COL_WIDTH) + (NODE_WIDTH / 2));

							root.reposition();
							repositionNodes();

							that.dirty(false);
						}
					);
					break;
				}
				break;
			default:
				var that = this;
				websocket.send(
					cmd,
					null,
					function (reply) {
						_('reply: ', reply);
						if (reply.error) {
							alert(`${cmd} failed`);
							return;
						}
						_(`${cmd} recd`);
					}
				);
				break;
		}
	}
}

const fileHandler = new FileHandler();

function bbget(btn) {
	const $form = $('#right_panel form');
	const key = $form.find('input[name=key]').val();
	websocket.send(
		'bbget',
		{
			k: key
		},
		function (reply) {
			_('reply: ', reply);
			if (reply.error) {
				alert(`Failed to load ${filename}: ${reply.error}.`);
				return;
			}
			$form.find('input[name=value]').val(reply.data.value);
			bbdisplay({
				key: key,
				value: reply.data.value
			});
		}
	);
}

function bbset(btn) {
	const $form = $('#right_panel form');
	const key = $form.find('input[name=key]').val();
	const value = $form.find('input[name=value]').val();
	websocket.send(
		'bbset',
		{
			k: key,
			v: value
		},
		function (reply) {
			_('reply: ', reply);
			if (reply.error) {
				alert(`Failed to load ${filename}: ${reply.error}.`);
				return;
			}
			$form.find('input[name=value]').val(reply.data.value);
			bbdisplay({
				key: key,
				value: reply.data.value
			});
		}
	);
}

function bbdump(btn) {
	websocket.send(
		'bbdump',
		{},
		function (reply) {
			_('reply: ', reply);
			if (reply.error) {
				alert(`Failed to load ${filename}: ${reply.error}.`);
				return;
			}
			for (const key in reply.data.values) {
				bbdisplay({
					key: key,
					value: reply.data.values[key]
				});
			}
		}
	);
}

function bbdisplay(bb) {
	const $bb_value = $(`span.bb_value[data-key="${bb.key}"]`);
	bb.value = JSON.stringify(bb.value);
	if ($bb_value.length == 0) {
		const $d = $(Mustache.render($('#bb_value_template').html().trim(), bb));
		$('#blackboard').append($d);
		$d.get(0).scrollIntoView();
	} else {
		$bb_value.text(bb.value);
	}
}

$(document).ready(function () {

	$('#toolbar').on('click', 'li[data-cmd]', function () {
		_($(this).data('cmd'));
		fileHandler.do($(this).data('cmd'));
	});

	$('#right_panel').on('click', '.bb_row', function () {
		const key = $(this).find('.bb_value').data('key');
		_(key);
		$('#right_panel input[name=key]').val(key);
		bbget();
	});

	fileHandler.do('connect');
});

function _(...v) {
	console.log(...v);
}
