"use strict"

const websocket = new WebSocketClient();

class FileHandler {
	filename;
	isDirty = false;

	constructor() {
	}

	do(cmd, filename) {
		if (cmd == 'save' && !this.filename) cmd = 'save_as';
		switch (cmd) {
			case 'save_as':
				var filename = prompt("Enter a filename:", this.filename);
				if (!filename || !filename.trim()) return false;
				this.filename = filename.trim();
				$('#filename').text(this.filename);
				Cookies.set('lastOpenFile', this.filename, { expires: 365 })
			case 'save':
				var that = this;
				websocket.send(
					'save',
					{
						name: this.filename,
						nodes: root.save()
					},
					function (reply) {
						console.log('reply: ', reply);
						if (reply.error) {
							alert(`Failed to save ${that.filename}: ${reply.error}.`);
							return;
						}
						that.dirty(false);
					}
				);
				break;
			case 'new':
			case 'open':
				if (this.isDirty) {
					if (!confirm('There are unsaved changes.\n\nDiscard the changes?')) return false;
				}
				if (cmd == 'new') {
					this.filename = 'Untitled';
					$('#filename').text(this.filename);
					selectedNode = null;
					lastSelectedNode = null;
					$('#canvas').empty();
					lastNodeId = 0;
					root = new Node({
						id: 'root',
						type: "root",
					}, null);
					repositionNodes();
					centerRoot();
					dirty(false);
				} else {
					if (!filename) {
						filename = prompt("Enter a filename:", Cookies.get('lastOpenFile'));
						console.log(filename);
						if (!filename || !filename.trim()) return false;
						console.warn('we got here');
						filename = filename.trim();
					}
					var that = this;
					websocket.send(
						'open',
						{
							name: filename
						},
						function (reply) {
							console.log('reply: ', reply);
							if (reply.error) {
								alert(`Failed to load ${filename}: ${reply.error}.`);
								return;
							}
							that.filename = filename;
							Cookies.set('lastOpenFile', filename, { expires: 365 })
							$('#filename').text(filename);
							selectedNode = null;
							lastSelectedNode = null;
							$('#canvas').empty();
							lastNodeId = 0;
							root = new Node(reply.data);

							repositionNodes();
							centerRoot();

							that.dirty(false);
						}
					);
				}
				break;
			case 'files':
				var that = this;
				if (filesOpen) {
					$('#files_panel').animate({ left: -180 });
					$('#results').animate({ left: 4 });
					filesOpen = false;
				} else {
					websocket.send(
						'files',
						null,
						function (reply) {
							console.log('reply: ', reply);
							if (reply.error) {
								alert(`Failed to get file listing: ${reply.error}.`);
								return;
							}
							$('#files_panel').empty();
							var files = JSON.parse(reply.data);
							for (var f of files) {
								$('#files_panel').append(Mustache.render($('#file_template').html().trim(), { name: f }));
							}
							$('#files_panel').animate({ left: 0 });
							$('#results').animate({ left: 184 });
							filesOpen = true;
						}
					);
				}
				break;
			case 'delete':
				var that = this;
				if (confirm(`Permanently delete '${filename}'?\n\nThere is no undo for this operation.`)) {
					websocket.send(
						'delete',
						{
							name: filename
						},
						function (reply) {
							console.log('reply: ', reply);
							if (reply.error) {
								alert(`Failed to delete ${filename}: ${reply.error}.`);
								return;
							}
							filesOpen = false;
							fileHandler.do('files');
						}
					);
				}
				break;
			case 'monitor':
				window.open('monitor.html', 'monitor');
				break;
			case 'run':
				if(!this.filename || this.isDirty) {
					alert('Save first.');
					return;
				}
				$('.node.state1').removeClass('state1');
				$('.node.state2').removeClass('state2');
				$('.node.state3').removeClass('state3');
			default:
				var that = this;
				websocket.send(
					cmd,
					null,
					function (reply) {
						console.log('reply: ', reply);
						if (reply.error) {
							alert(`${cmd} failed`);
							return;
						}
						console.log(`${cmd} recd`);
					}
				);
				break;
		}
	}

	dirty(is) {
		if (this.isDirty == is) return;
		this.isDirty = is;
		if (is) $('#filename').append(' <i id="dirtyMsg" class="fas fa-circle-exclamation" title="Unsaved changes"></i>');
		else $('#dirtyMsg').remove();
	}
}