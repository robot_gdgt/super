"use strict";

const TOP_ROW_TOP = 20;
const ROW_SPACING = 64;
const COL_WIDTH = 100;
const NODE_WIDTH = 160;
const CANVAS_PADDING = 1000;
const PORTAL_PADDING = 16;

const ARROW_LEFT = 37;
const ARROW_UP = 38;
const ARROW_RIGHT = 39;
const ARROW_DOWN = 40;

var root;
var selectedNode = null;
var lastSelectedNode = null;
var isDirty = false;
var lastNodeId = 0;

var rowHeights = [];
var rowColCounts = [];
var selectMode = 'node';
var canvasOffset = {
	top: 0,
	left: 0
};
var editingText = false;
var canvasDims = {
	width: 0,
	height: 0
};
var canvasScale = 1;
var dragging = false;
var filesOpen = false;

const fileHandler = new FileHandler();

function clearSelectedNode(node) {
	if (selectedNode) {
		selectedNode.unselect();
		selectedNode = null;
	}
	if (node) node.select();
}

function repositionNodes() {

	// Assign nodes to columns starting at the root.
	root.setColumns();

	// Update node and connector top and left.
	root.reposition();
}

function centerRoot() {
	var x = root.col * COL_WIDTH + CANVAS_PADDING + NODE_WIDTH / 2;
	var y = root.bottom - root.height / 2;
	const $portal = $('#portal');
	var l = $portal.width() / 2 - x;
	var t = $portal.height() / 2 - y;
	if (t > -CANVAS_PADDING + PORTAL_PADDING) t = -CANVAS_PADDING + PORTAL_PADDING;
	$('#canvas').css({ left: l, top: t });
}

function bringNodeIntoView(node, instant) {
	const canvasPos = $('#canvas').position();
	var x = canvasPos.left + node.col * COL_WIDTH + CANVAS_PADDING + NODE_WIDTH / 2;
	var y = canvasPos.top + node.bottom - node.height / 2;
	const $portal = $('#portal');
	var limit, dx = 0, dy = 0;
	// left
	limit = $('#left_panel').width() + NODE_WIDTH / 2 + PORTAL_PADDING;
	if (x < limit) dx = limit - x;
	else {
		limit = $portal.width() - $('#right_panel').width() - NODE_WIDTH / 2 - PORTAL_PADDING;
		if (x > limit) dx = limit - x;
	}
	limit = node.height / 2 + PORTAL_PADDING;
	if (y < limit) dy = limit - y;
	else {
		limit = $portal.height() - node.height / 2 - PORTAL_PADDING;
		if (y > limit) dy = limit - y;
	}
	if (instant) $('#canvas').css({ left: canvasPos.left + dx, top: canvasPos.top + dy });
	else $('#canvas').animate({ left: canvasPos.left + dx, top: canvasPos.top + dy });
}

function dirty(is) {
	fileHandler.dirty(is);
}

function trashFile(btn, evt) {
	evt.stopPropagation();
	var $btn = $(btn);
	var fn = $btn.closest('div.file').data('fn');
	_('trash', fn);
	fileHandler.do('delete', fn);
}

$(document).ready(function () {
	// Display the node types in the left panel.
	for (var catKey in nodeCats) {
		var cat = nodeCats[catKey];
		// Add cat to pallet as an unclickable header
		cat.cat = catKey;
		if (catKey != 'root') $('#left_panel').append(Mustache.render($('#category_template').html().trim(), cat));
		// Loop through types and add them as clickable items
		for (var key in nodeTypes) {
			var type = nodeTypes[key];
			if (type.cat != catKey) continue
			type.type = key;
			type.classes = `${catKey} ${key}`;
			if (cat.applyTo) type.classes += ' applyTo_' + cat.applyTo.join(' applyTo_');
			$('#left_panel').append(Mustache.render($('#type_template').html().trim(), type));
		}
	}
	// Add the file names under Import
	websocket.send(
		'files',
		null,
		function (reply) {
			console.log('reply: ', reply);
			if (reply.error) {
				alert(`Failed to get file listing: ${reply.error}.`);
				return;
			}
			var cat = nodeCats['importFile'];
			var files = JSON.parse(reply.data);
			for (var f of files) {
				var data = {classes: '', filename: f};
				if (cat.applyTo) data.classes += ' applyTo_' + cat.applyTo.join(' applyTo_');
				$('#left_panel').append(Mustache.render($('#import_template').html().trim(), data));
			}
		}
	);

	var filename = Cookies.get('lastOpenFile');
	if (filename && filename != 'undefined') fileHandler.do('open', filename);

	$('#files_panel').on('click', 'div.file', function () {
		var $this = $(this);
		var fn = $this.data('fn');
		_(fn);
		fileHandler.do('open', fn);
		$('#files_panel').animate({ left: -180 });
		$('#results').animate({ left: 4 });
		filesOpen = false;
	});

	$('#left_panel').on('click', 'div.category', function () {
		var $this = $(this);
		var cat = $this.data('cat');
		if ($this.find('i').hasClass('fa-caret-square-right')) { // closed
			$this.find('i').removeClass('fa-caret-square-right').addClass('fa-caret-square-down');
			$(`#left_panel div.type.${cat}`).show();
		} else {
			$this.find('i').removeClass('fa-caret-square-down').addClass('fa-caret-square-right');
			$(`#left_panel div.type.${cat}`).hide();
		}
	});

	$('#left_panel').on('click', '.type.applicable', function () {
		var $this = $(this);
		var type = $this.data('type');
		if (type == 'importFile') {
			const filename = $this.data('filename');
			selectedNode.import(filename);
		} else if (nodeTypes[type].cat == 'replyDeco' || nodeTypes[type].cat == 'shieldDeco' || nodeTypes[type].cat == 'execDeco') {
			selectedNode.addDeco(type, null, true);
		} else {
			lastNodeId++;
			var id = `n${lastNodeId}`;
			selectedNode.addChild(new Node({
				id: id,
				type: type,
				order: 999,
			}, selectedNode));
			root.setColumns();
			root.reposition();
		}
	});

	$('#canvas').on('click', '.node.clickable', function (evt) {
		var node = $(this).data('node');

		if (selectMode == 'node') {
			if (dragging) {
				dragging = false;
			} else if(evt.altKey) {
				node.toggleIsDisabled();
			} else {
				clearSelectedNode();
				node.select();
			}
		} else if (selectMode == 'relocate') {
			selectedNode.relocate(null, null, node);
		}

		evt.stopPropagation();
		return false;
	});

	$('#canvas').on('click', '.revealIcon', function (evt) {
		var $node = $(this).closest('.node');
		var node = $node.data('node');
		node.toggleChildren();

		repositionNodes();

		evt.stopPropagation();
		return false;
	});

	$('#portal').click(() => {
		if (selectMode == 'node') {
			if (dragging) {
				dragging = false;
			} else {
				clearSelectedNode();
			}
		}
	});

	$('#inspector').on('change', 'input, textarea, select', function () {
		selectedNode.updateField(this);
	})

	$('#inspector').on('focus', 'input, textarea, select', () => editingText = true);
	$('#inspector').on('blur', 'input, textarea, select', () => editingText = false);

	$('#portal').on('wheel', evt => {
		if (!evt.altKey) return;
		// canvasScale += evt.originalEvent.deltaY / 400;
		canvasScale *= evt.originalEvent.deltaY > 0 ? 0.98 : 1.02;
		_(evt.originalEvent.deltaY, canvasScale);
		$('#canvas').css({ transform: `scale(${canvasScale})` });
		evt.stopPropagation();
	});

	const draggableCanvas = new Draggable(
		document.getElementById('canvas'),
		{
			onDragStart: () => {
				dragging = true;
			},
		}
	);

	$('#toolbar').on('click', 'li[data-cmd]', function () {
		clearSelectedNode();
		_($(this).data('cmd'));
		fileHandler.do($(this).data('cmd'));
	});

	$(document).keydown((evt) => {
		// _(evt.keyCode, evt.shiftKey, evt.metaKey, evt.altKey, evt);
		if (evt.keyCode < ARROW_LEFT || evt.keyCode > ARROW_DOWN || editingText) return;
		if (!evt.altKey) {
			if (!selectedNode) {
				selectedNode = lastSelectedNode ? lastSelectedNode : root;
				selectedNode.select();
			} else {
				selectedNode.selectNext(evt.keyCode);
			}
		} else {
			if ((evt.keyCode == ARROW_LEFT || evt.keyCode == ARROW_RIGHT) && selectedNode) {
				selectedNode.move(evt.keyCode);
			} else if ((evt.keyCode == ARROW_UP || evt.keyCode == ARROW_DOWN) && selectedNode) {
				selectedNode.toggleChildren(evt.keyCode == ARROW_UP);
			}
		}
	});

});

function _(...v) {
	console.log(...v);
}
