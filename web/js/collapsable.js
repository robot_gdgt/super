var chart_config = {
	chart: {
		container: "#tree",

		animateOnInit: false,

		nodeAlign: "TOP",

		levelSeparation: 30,
		siblingSeparation: 20,
		subTeeSeparation: 20,

		connectors: {
			type: "bCurve",
			style: {
				stroke: "gray",
				"stroke-width": 2
			}
		},

		node: {
			collapsable: true,
			HTMLclass: "tree_node"
		},
		animation: {
// 			nodeAnimation: "easeOutBounce",
// 			nodeSpeed: 700,
// 			connectorsAnimation: "bounce",
// 			connectorsSpeed: 700
		},

		callback: {
			onCreateNode: () => console.log("onCreateNode"),
			onCreateNodeCollapseSwitch: () => console.log("onCreateNodeCollapseSwitch"),
			onAfterAddNode: () => console.log("onAfterAddNode"),
			onBeforeAddNode: () => console.log("onBeforeAddNode"),
			onAfterPositionNode: () => console.log("onAfterPositionNode"),
			onBeforePositionNode: () => console.log("onBeforePositionNode"),
			onToggleCollapseFinished: () => console.log("onToggleCollapseFinished"),
			onAfterClickCollapseSwitch: () => console.log("onAfterClickCollapseSwitch"),
			onBeforeClickCollapseSwitch: () => { console.log("onBeforeClickCollapseSwitch"); return 1; },
			onTreeLoaded: () => console.log("onTreeLoaded")
		}

	},
	nodeStructure: {
		text: { name: "malory" },
		children: [
			{
				text: { name: "lana" },
				collapsed: true,
				children: [
					{
						text: { name: "figgs" }
					}
				]
			},
			{
				text: { name: "sterling" },
//                     childrenDropLevel: 1,
				children: [
					{
						text: { name: "woodhouse" }
					}
				]
			},
			{
				text: { name: "sterling" },
				children: [
					{
						text: { name: "cheryl" }
					},
					{
						text: { name: "pam" }
					}
				]
			}
		]
	}
};
