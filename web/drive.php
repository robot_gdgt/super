<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

if(!empty($_POST)) {
	require('../../gdgt_shared/can_ids.php');
	
	$speed = $_POST['speed'];
	$dir = $_POST['dir'];
	$dist = $_POST['dist'];

	$redis = new Redis();
	//Connecting to Redis
	try {
		$redis->pconnect('/var/run/redis/redis-server.sock');
	} catch(Exception $e) {
		die('{"error":"unable to connect to redis server"}');
	}
	// $laser = $redis->get('laser');
	// $redis->set($k, $v);
	$data = [];
	if($dist) $data[] = 0;

	$redis->rpush('CANtx', json_encode([
		'can_id' => $dist ? CAN_CMD_RC_MOVE_DIST : CAN_CMD_RC_MOVE,
		'dlc' => count($data),
		'data' => $data
	]));
} else {
	$speed = 0;
	$dir = 0;
	$dist = 0;
}
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width">
	<title>Drive</title>
	<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
	<link rel="manifest" href="favicon/site.webmanifest">

	<style>
* {
	box-sizing: border-box;
}

body {
	margin: 0px;
	padding: 10px;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
}

#parametric, #joystick {
	border: 2px solid silver;
	width: 100%;
	padding: 10px;
	position: relative;
}

#joystick {
	margin-top: 10px;
}

#joyDiv {
	width: 280px;
	height: 280px;
}

.label {
	position: absolute;
	padding: 0 6px;
	top: -9px;
	left: 7px;
	background-color: white;
	font-weight: bold;
}
	</style>
	<script src="js/joy.js"></script>
</head>

<body>

<form action="drive.php" method="POST">
<div id="parametric">
	<div class="label">Parametric</div>
	<table>
		<tr>
			<td>Speed:</td>
			<td><input name="speed" value="<?= $speed ?>" size="6"></td>
		</tr>
		<tr>
			<td>Direction:</td>
			<td><input name="dir" value="<?= $dir ?>" size="6"></td>
		</tr>
		<tr>
			<td>Distance:</td>
			<td><input name="dist" value="<?= $dist ?>" size="6"></td>
		</tr>
	</table>
	<input type="submit" value="Submit">
</div>
<div id="joystick">
	<div class="label">Joystick</div>
	<div id="joyDiv"></div>
</div>
</form>
<script type="text/javascript">
// Create JoyStick object into the DIV 'joyDiv'
var joy1 = new JoyStick('joyDiv', {
	title: 'joyCanvas',
	internalFillColor: '#C4BC00',
	internalLineWidth: 2,
	internalStrokeColor: '#000000',
	externalLineWidth: 0,
	externalStrokeColor: 'white'
}, function(stickData) {
    console.log(stickData.xPosition, stickData.yPosition, stickData.x, stickData.y);
});
</script>
</body>

</html>