"""
CAN monitor
"""

import time

from can_ids import CAN_STATUS_FRIG_POS
import can_proxy_thread as can

##### SETUP #####

can.add_filter('main', CAN_STATUS_FRIG_POS, 0x7FF)
can.send('main', CAN_STATUS_FRIG_POS, True)
watchdogStart = time.monotonic()

##### LOOP #####

while True:

    CANrx = can.get('main')
    if CANrx is not None:
        if CANrx['id'] == CAN_STATUS_FRIG_POS and not CANrx['rr']:
            can.send('main', CAN_STATUS_FRIG_POS, True)
            watchdogStart = time.monotonic()

    if time.monotonic() - watchdogStart > 5:
        can.send('main', CAN_STATUS_FRIG_POS, True)
        watchdogStart = time.monotonic()
