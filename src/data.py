import os
import re
import can_ids

# for root, dirs, files in os.walk("bt_parts", topdown=False):
#     if os.path.basename(root) == '__pycache__':
#         continue
#     print(f'root={root} - for name in files:')
#     for name in files:
#         if name == '__pycache__':
#             continue
#         print(os.path.join(root, name))
    # print(f'root={root} - for name in dirs:')
    # for name in dirs:
    #     if name == '__pycache__':
    #         continue
    #     print(os.path.join(root, name))

cats = ["composite", "shieldDeco", "replyDeco", "execDeco", "condition", "action"]

json = ''

for cat in cats:
    path = f'bt_parts/{cat}'
    # print(path)
    files = os.listdir(path)
    # print(files)
    files.sort()
    for name in files:
        if os.path.isdir(f'{path}/{name}') or name[0:2] == '__':
            continue
        print(f'{path}/{name} - ', end='')
        with open(f'{path}/{name}', 'r') as file:
            data = file.read()
        js = re.search('data.js:(.*)"""', data, re.DOTALL)
        if js is None:
            print('skipped')
        else:
            js = js.group(1)[:-1]
            for constant in re.findall('[A-Z_]{6,}', js, re.DOTALL):
                # print(constant)
                if hasattr(can_ids, constant):
                    js = js.replace(constant, hex(getattr(can_ids, constant)))
            # print(js)
            json = f'{json}"{os.path.splitext(name)[0]}":{js},\n'
            print('added')

# print(json)

with open('bt_parts/data.template.js', 'r') as file:
    base = file.read()
expanded = base.replace('PUT_IT_HERE', json)

# print(expanded)

with open('../web/js/data.js', 'w') as file:
    file.write(expanded)

print('Wrote to web/js/data.js\n')
