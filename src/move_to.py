from can_ids import *
import can_proxy_thread as can
from gdgt_platform import *

##### SETUP #####

"""
int16_t x = CANbuf[0] << 8 | CANbuf[1]; // -32,768 .. 32,767
uint16_t z = CANbuf[2] << 8 | CANbuf[3]; // 0 .. 65535
uint8_t immediate = CANbuf[4] >> 7; // immediate = 1, buffered = 0
uint8_t end_stop = (CANbuf[4] >> 6) & 0x01; // stop = 1, coast = 0
uint8_t speed = CANbuf[4] & 0x1F; // 0 .. 17
if (speed > 17) speed = 17;
int16_t relHead = CANbuf[5] << 8 | CANbuf[6]; // -32,768 .. 32,767
"""

x = 0
z = 0
h = 0
when = 'i'
end = 's'
speed = 0.5

while (True):
	userInput = str(input('Enter data: ')).lower()
	cmd = userInput[0:1]
	val = userInput[1:].strip()
	if val == '': val = '0'
	if cmd == 'q':
		break

	if cmd == '/' or cmd == '?':
		print()
		print(f'x dist (cm):         {x}')
		print(f'z dist (cm):         {z}')
		print(f'h rel heading (°):   {h}')
		print(f'w when (i b):        {when}')
		print(f'e end (s c):         {end}')
		print(f's speed (0.1 - 1.7): {speed}')
		print()
		continue

	userInput = (userInput + ' 0 0').split()

	if cmd == 'x':
		t = int(val)
		if t < -32000 or t > 32000:
			print('Invalid')
		else:
			x = t
		continue

	if cmd == 'z':
		t = int(val)
		if t < 0 or t > 65000:
			print('Invalid')
		else:
			z = t
		continue

	if cmd == 'h':
		t = int(val)
		if t < -32000 or t > 32000:
			print('Invalid')
		else:
			h = t
		continue

	if cmd == 'w':
		if val == 'i' or val == 'b':
			when = val
		else:
			print('Invalid')
		continue

	if cmd == 'e':
		if val == 's' or val == 'c':
			end = val
		else:
			print('Invalid')
		continue

	if cmd == 's':
		s = float(int(float(val) * 10) / 10)
		if s < 0.1 or s > 1.7:
			print('Invalid')
		else:
			speed = s
		continue

	if cmd == 'g':

		id = CAN_CMD_RC_MOVE_TO

		bytes = []
		bytes.extend(x.to_bytes(2, 'big', signed=True))
		bytes.extend(z.to_bytes(2, 'big', signed=False))
		bytes.append(
			(RC_MOVE_IMMED if when == 'i' else 0) |
			(RC_MOVE_END_STOP if end == 's' else 0) |
			round(speed * 10)
		)
		bytes.extend(h.to_bytes(2, 'big', signed=True))

		print(bytes)
		can.send('move_to', id, data=bytes)
