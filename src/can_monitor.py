"""
CAN monitor
"""

from can_ids import *
import can_proxy_thread as can
from gdgt_platform import *
from speak_direct_can_thread import speak_direct
from status_led import StatusLED
from tts_groups import *
from var_dump import var_dump as vd

hbLED = StatusLED(LED_PIN, True)

##### SETUP #####

can.del_filter('main', 'all', 'all')
can.add_filter('main', 0x000, 0x000) # Want alert 000 - 007 packets
# can.add_filter('main', CAN_STATUS_BATT, 0x7FF) # Want all timeslot packets
# can.add_filter('main', CAN_CMD_HALT, 0x7FF)
# can.add_filter('main', CAN_CMD_QUIT, 0x7FF)
# can.add_filter('main', CAN_CMD_SUPER, 0x7F0)

hbLED.heartbeat()

##### LOOP #####

spinner = 0
spinners = ['-', '\\', '|', '/']
print('READY\n-', end='')

while True:

    # now = time()

    spinner = (spinner + 1) % 4
    print(f'\b{spinners[spinner]}', end='')

    CANrx = can.get('main')
    if CANrx is not None:
        print(f'\nCAN Rx: {CANrx}')

    # Blink the LED
    hbLED.tickle()
