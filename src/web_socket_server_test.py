import web_socket_server
from time import sleep

def recvCallback(client):
    print(f'recd: {client.data}')

web_socket_server.recvCallback = recvCallback

i = 0
while True:
    sleep(5)
    i += 1
    web_socket_server.broadcast(f'hello again {i}')
