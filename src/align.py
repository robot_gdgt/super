from can_ids import *
import can_proxy_thread as can
from gdgt_platform import *

##### SETUP #####

"""
uint16_t heading = CANbuf[0] << 8 | CANbuf[1]; // 0 - 359
uint8_t speed = CANbuf[2] // QPPS
"""

speed = 0
heading = 0

macros = {}

while (True):
	userInput = str(input('Enter data: ')).lower()
	cmds = userInput.split()

	if len(cmds) == 0:
		continue

	if cmds[0] == 'q':
		break

	if cmds[0][0:1] == '+':
		macro = cmds[0][1:]
		macros[macro] = [
			f's{speed}',
			f'n{heading}',
		]
		print('Saved')

	if cmds[0] in macros:
		cmds = macros[cmds[0]]
		print('Loaded')
	
	for cmd in cmds:
		val = cmd[1:]
		cmd = cmd[0:1]

		if cmd == '/' or cmd == '?':
			print()
			print(f's speed (0 - 255):{speed}')
			print(f'h heading (°):    {heading}')
			print()
			continue

		if cmd == 's':
			s = int(val)
			if s < 0 or s > 255:
				print('Invalid')
			else:
				speed = s
			continue

		if cmd == 'h':
			n = int(val)
			if n < 0 or n > 359:
				print('Invalid')
			else:
				heading = n
			continue

		if cmd == 'g':
			bytes = []

			if val == '':
				id = CAN_CMD_RC_ALIGN_HEADING

				bytes.extend(int.to_bytes(heading, 2, 'big'))
				bytes.append(speed)

			else:
				continue
				
			print(bytes)
			can.send('align', id, data=bytes)
