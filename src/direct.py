from can_ids import *
import can_proxy_thread as can
from gdgt_platform import *

while (True):
	userInput = str(input('Enter data: ')).lower()
	tok = userInput.split()
	if len(tok) == 0:
		continue
	if tok[0] == 'q':
		break

	bytes = []

	for v in tok[1:]:
		if v == '':
			continue
		s = v[0]
		v = int(v[1:])
		if s == '1':
			bytes.append(v)
		elif s == '2':
			bytes.extend(int.to_bytes(v, 2, 'big', signed=True))
		elif s == '4':
			bytes.extend(int.to_bytes(v, 4, 'big', signed=True))

	id = CAN_RC_DIRECT | int(tok[0])

	print(f'ID: {id} data: {bytes}')
	can.send('direct', id, data=bytes)
