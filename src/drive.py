from can_ids import *
import can_proxy_thread as can
from gdgt_platform import *

##### SETUP #####

"""
uint8_t immediate = CANbuf[0] >> 7; // immediate = 1, buffered = 0
uint8_t end_stop = (CANbuf[0] >> 6) & 0x01; // stop = 1, coast = 0
uint8_t fr = (CANbuf[0] >> 5) & 0x01; // forward = 1, reverse = 0
uint8_t speed = CANbuf[0] & 0x1F; // 0 - 17
if(speed > 17) speed = 17;
uint8_t lr = CANbuf[1] >> 7; // left = 1, right = 0
int16_t radius = (CANbuf[1] & 0x7F) << 8 | CANbuf[2]; // 0 - 32767 (RADIUS_STRAIGHT)
"""

action = 'd'
when = 'i'
end = 's'
dir = 'f'
speed = 0
turn = 's'
radius = 0
dist_ang = 0

macros = {}

while (True):
	userInput = str(input('Enter data: ')).lower()
	cmds = userInput.split()

	if len(cmds) == 0:
		continue

	if cmds[0] == 'q':
		break

	if cmds[0][0:1] == '+':
		macro = cmds[0][1:]
		macros[macro] = [
			f'a{action}',
			f'w{when}',
			f'e{end}',
			f'd{dir}',
			f's{speed}',
			f't{turn}',
			f'r{radius}',
			f'n{dist_ang}',
		]
		print('Saved')

	if cmds[0] in macros:
		cmds = macros[cmds[0]]
		print('Loaded')
	
	for cmd in cmds:
		val = cmd[1:].strip()
		if val == '': val = '0'
		cmd = cmd[0:1]

		if cmd == '/' or cmd == '?':
			print()
			print(f'a action (m d a):   {action}')
			print(f'w when (i b):       {when}')
			print(f'e end (s c):        {end}')
			print(f'd dir (f r):        {dir}')
			print(f's speed (0.0 - 1.7):{speed}')
			print(f't turn (l r s):     {turn}')
			print(f'r radius (cm):      {radius}')
			print(f'n dist_ang (cm, °): {dist_ang}')
			print()
			continue

		if cmd == 'a':
			if val == 'm' or val == 'd' or val == 'a':
				action = val
				if val == 'm':
					when = 'i'
			else:
				print('Invalid')
			continue

		if cmd == 'w':
			if val == 'i' or val == 'b':
				when = val
			else:
				print('Invalid')
			continue

		if cmd == 'e':
			if val == 's' or val == 'c':
				end = val
			else:
				print('Invalid')
			continue

		if cmd == 'd':
			if val == 'f' or val == 'r':
				dir = val
			else:
				print('Invalid')
			continue

		if cmd == 's':
			s = float(val)
			if s < 0 or s > 1.7:
				print('Invalid')
			else:
				speed = s
			continue

		if cmd == 't':
			if val == 'l' or val == 'r' or val == 's':
				turn = val
			else:
				print('Invalid')
			continue

		if cmd == 'r':
			r = int(val)
			if r < 0 or r > 0x7FFF:
				print('Invalid')
			else:
				radius = r
			continue

		if cmd == 'n':
			n = int(val)
			if n < 0 or n > 0xFFFF:
				print('Invalid')
			else:
				dist_ang = n
			continue

		if cmd == 'g':
			bytes = []

			if val == '0':
				if action == 'm':
					id = CAN_CMD_RC_MOVE
				elif action == 'd':
					id = CAN_CMD_RC_MOVE_DIST
				else:  # action == 'a':
					id = CAN_CMD_RC_MOVE_ANGLE

				bytes.append(
					(RC_MOVE_IMMED if when == 'i' else 0) |
					(RC_MOVE_END_STOP if end == 's' else 0) |
					(RC_MOVE_FORWARD if dir == 'f' else 0) |
					round(speed * 10)
				)

				if turn != 's':
					r = (RC_MOVE_LEFT if turn == 'l' else 0) | radius
				else:
					r = RC_MOVE_STRAIGHT
				bytes.append(r >> 8)
				bytes.append(r & 0xFF)

				if action == 'd' or action == 'a':
					bytes.append(dist_ang >> 8)
					bytes.append(dist_ang & 0xFF)

			elif val == '1' or val == '2':
				id = CAN_CMD_RC_MOVE_DIST
				speed = 10 | (RC_MOVE_FORWARD if val == '1' else 0) | RC_MOVE_END_STOP
				bytes.append(speed)
				radius = 32767
				bytes.append(radius >> 8)
				bytes.append(radius & 0xFF)
				dist = 400
				bytes.append(dist >> 8)
				bytes.append(dist & 0xFF)
			elif val == '3' or val == '4':
				id = CAN_CMD_RC_MOVE_DIST
				speed = 20 | (RC_MOVE_FORWARD if val == '3' else 0) | RC_MOVE_END_STOP
				bytes.append(speed)
				radius = 32767
				bytes.append(radius >> 8)
				bytes.append(radius & 0xFF)
				dist = 400
				bytes.append(dist >> 8)
				bytes.append(dist & 0xFF)
			elif val == '5' or val == '6':
				id = CAN_CMD_RC_MOVE_DIST
				speed = 31 | (RC_MOVE_FORWARD if val == '5' else 0) | RC_MOVE_END_STOP
				bytes.append(speed)
				radius = 32767
				bytes.append(radius >> 8)
				bytes.append(radius & 0xFF)
				dist = 400
				bytes.append(dist >> 8)
				bytes.append(dist & 0xFF)
			else:
				continue
				
			"""
			else:
				if cmd == 'p':
					id = CAN_CMD_RC_PARAMS
				else:
					id = int(userInput[0])
				del userInput[0]
				for byte in userInput:
					byte = int(byte)
					if byte >= 0 and byte <= 255:
						bytes.append(byte)
			"""
			print(bytes)
			can.send('move_to', id, data=bytes)
