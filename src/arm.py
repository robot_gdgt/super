from can_ids import *
import can_proxy_thread as can
from gdgt_platform import *

"""
#define ARM_CMD_STOP 0x00
#define ARM_CMD_STOP_DECEL 0x01
#define ARM_CMD_TOOL_READY 0x0B
#define ARM_CMD_TOOL_GET 0x0C
#define ARM_CMD_TOOL_LOCK 0x0D
#define ARM_CMD_TOOL_RETURN 0x0E
#define ARM_CMD_SEQUENCE 0x0F
#define ARM_CMD_DOOR_PRESS 0x10

#define ARM_STATE_UNKOWN 0
#define ARM_STATE_PARKED 1
#define ARM_STATE_UNPARKED 2
#define ARM_STATE_EXTENDED 3
#define ARM_STATE_TOOL_READY 4

#define ARM_ERR_NONE 0
#define ARM_ERR_NOT_FRONT 1
#define ARM_ERR_TOO_FAR 2
#define ARM_ERR_P_ANGLE 3
#define ARM_ERR_S_ANGLE 4
#define ARM_ERR_E_ANGLE 5
#define ARM_ERR_W_ANGLE 6
#define ARM_ERR_QUEUE_FULL 7
"""

direct_commands = {
	'i':('init', CAN_ARM_INIT),
	'l':('limp', CAN_ARM_LIMP),
	'p':('park', CAN_ARM_PARK),
	'u':('unpark', CAN_ARM_UNPARK),
	'e':('extend', CAN_ARM_EXTEND),
	'd':('door extend', CAN_ARM_DOOR_EXT),
	']':('home wrist', CAN_ARM_HOME_R),
	'[':('home pivot', CAN_ARM_HOME_P),
	'o':('grip open', CAN_GRIP_OPEN),
	'c':('grip close', CAN_GRIP_CLOSE),
}

x = 0
y = 0
z = 0
r = 0
g = 90
v = 1
a = 0
t = 0

macros = {}

can.add_filter('arm', CAN_STATUS_ARM, 0x7FF)

while (True):
	CANrx = can.get('arm')
	if CANrx is not None:
		if CANrx['id'] == CAN_STATUS_ARM and not CANrx['rr']:
			print(CANrx['data'])

	userInput = str(input('Enter data: ')).lower()
	cmds = userInput.split()

	if len(cmds) == 0:
		continue

	if cmds[0] == 'q':
		break

	if cmds[0][0:1] == '+':
		macro = cmds[0][1:]
		macros[macro] = [
			f'x{x}',
			f'y{y}',
			f'z{z}',
			f'r{r}',
			f'g{g}',
		]
		print('Saved')

	if cmds[0] in macros:
		cmds = macros[cmds[0]]
		print('Loaded')
	
	for cmd in cmds:
		val = cmd[1:].strip()
		if val == '': val = '0'
		cmd = cmd[0:1]

		if cmd == '/' or cmd == '?':
			print()
			for cmd in direct_commands:
				print(f'{cmd} {direct_commands[cmd][0]}')
			print( 'm moveto')
			print(f'x (world mm):        {x}')
			print(f'y (world mm):        {y}')
			print(f'z (world mm):        {z}')
			print(f'r rotate angle (°):  {r}')
			print(f'g gripper angle (°): {g}')
			print(f'v velocity factor:   {v}')
			print(f'a jog angle (°):     {a}')
			print(f't jog joint (0-4):   {t}')
			print( 'j jog')
			print( 's status')
			print( 'k solenoid 2 secs')
			print( '( grip ref led on')
			print( ') grip ref led off')
			print()
			continue
			
		if cmd in direct_commands:
			print(f'{direct_commands[cmd][0]} 0x{direct_commands[cmd][1]:03X}')
			can.send('arm', direct_commands[cmd][1])
			continue

		if cmd == 'x':
			n = int(val)
			if n < -320000 or n > 32000:
				print('Invalid')
			else:
				x = n
			continue

		if cmd == 'y':
			n = int(val)
			if n < 0 or n > 1000:
				print('Invalid')
			else:
				y = n
			continue

		if cmd == 'z':
			n = int(val)
			if n < 0 or n > 1000:
				print('Invalid')
			else:
				z = n
			continue

		if cmd == 'r':
			n = int(val)
			if n < -1000 or n > 1000:
				print('Invalid')
			else:
				r = n
			continue

		if cmd == 'g':
			n = int(val)
			if n < -360 or n > 360:
				print('Invalid')
			else:
				g = n
			continue

		if cmd == 'v':
			n = int(val)
			if n < 1 or n > 5:
				print('Invalid')
			else:
				v = n
			continue

		if cmd == 'm':
			bytes = []
			bytes.extend(int.to_bytes(x, 2, 'big', signed=True))
			bytes.extend(int.to_bytes(y, 2, 'big'))
			bytes.extend(int.to_bytes(z, 2, 'big'))
			bytes.append(0)
			print(bytes)
			can.send('align', CAN_ARM_MOVETO_A, data=bytes)
			bytes = []
			bytes.extend(int.to_bytes(r, 2, 'big', signed=True))
			bytes.extend(int.to_bytes(g, 1, 'big', signed=True))
			bytes.append(v)
			bytes.append(1)
			print(bytes)
			can.send('arm', CAN_ARM_MOVETO_B, data=bytes)

		if cmd == 's':
			can.send('arm', CAN_STATUS_ARM, rr=True)
			continue

		if cmd == '(':
			can.send('arm', CAN_GRIP_REF_LED, data=[1])
			continue

		if cmd == ')':
			can.send('arm', CAN_GRIP_REF_LED, data=[0])
			continue

		if cmd == 'a':
			n = round(float(val), 1)
			if n < -1000 or n > 1000:
				print('Invalid')
			else:
				a = n
			continue

		if cmd == 't':
			n = int(val)
			if n < 0 or n > 4:
				print('Invalid')
			else:
				t = n
			continue

		if cmd == 'j':
			bytes = []
			bytes.append(t)
			bytes.extend(int.to_bytes(int(a * 10), 2, 'big', signed=True))
			bytes.append(v)
			print(bytes)
			can.send('arm', CAN_ARM_JOG, data=bytes)
