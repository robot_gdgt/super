"""
Set BB value

data.js: {
    "name": "Set BB Value",
    "label": "Set [{bbKey}] to {bbValue}",
    "icon": "fas fa-cog",
    "cat": "action",
    "params": [
        {
            "key": "bbKey",
            "label": "Key to set",
            "kind": "key"
        },
        {
            "key": "bbValue",
            "label": "Value"
        }
    ]
}
"""

import logging

from ..Blackboard import Blackboard as bb
from ..Node import Node

class SetBB(Node):
    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'bbKey'):
            raise RuntimeError(f'Missing param - {self.label}.bbKey')
        if not hasattr(self, 'bbValue'):
            raise RuntimeError(f'Missing param - {self.label}.bbValue')
        if self.bbKey[0] == 'i':
            if self.bbValue[0:2] == '0x' or self.bbValue[0:2] == '0X':
                self.bbValue = int(self.bbValue, 0)
            else:
                self.bbValue = int(self.bbValue)
        elif self.bbKey[0] == 'h':
            self.bbValue = int(self.bbValue, 16)
        elif self.bbKey[0] == 'f':
            self.bbValue = float(self.bbValue)

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        bb.set(self.label, self.bbKey, self.bbValue)                 # pylint: disable=no-member
        return (Node.SUCCESS, None)
