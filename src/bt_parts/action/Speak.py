"""
Speak - TTS and sounds

data.js: {
    "name": "Speak",
    "icon": "fas fa-volume-low",
    "cat": "action",
    "help": "Speak from a group, a specific phrase or sound, or speak ad-hoc text.",
    "params": [
        {
            "key": "idgroup",
            "label": "Speech Group ID",
            "kind": "integer",
        },
        {
            "key": "idstring",
            "label": "String or Sound ID",
            "kind": "integer",
        },
        {
            "key": "text",
            "label": "Speak Text",
            "kind": "textarea"
        },
    ]
}
"""

import logging

import can_proxy_thread as can
from can_ids import CAN_CMD_SPEAK
from speak_direct_can_thread import speak_direct

from ..Node import Node

class Speak(Node):
    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        if hasattr(self, 'idgroup'):
            print(f'speak group {self.idgroup}')
            can.send('Speak', CAN_CMD_SPEAK, False, [1, self.idgroup])
        elif hasattr(self, 'idstring'):
            print(f'speak string {self.idstring}')
            can.send('Speak', CAN_CMD_SPEAK, False, [1, self.idstring])
        elif hasattr(self, 'text'):
            print(f'speak {self.text}')
            speak_direct(self.text)

        return (Node.SUCCESS, None)
