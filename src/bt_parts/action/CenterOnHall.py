"""
CenterOnHall - Determine location in relationship to hall and adjust as needed

data.js: {
    "name": "Center on Hall",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "help": "Determine location in relationship to hall and adjust as needed.",
    "params": [
        {
            "key": "iNullZone",
            "label": "Null Zone Width (cm)",
            "kind": "integer",
            "default": 80,
        },
        {
            "key": "iHeading",
            "label": "Heading (°)",
            "kind": "integer",
            "default": 0,
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
}
"""

# import logging
import math
import time

import can_proxy_thread as can
from can_ids import *

from ..Blackboard import Blackboard as bb
from ..Node import Node
from ..movement import alignHeading, moveDist, moveAngle, isMoving

INTERNAL_IDLE = 0
ROTATE_LEFT = 1
GET_LEFT_DIST = 2
ROTATE_RIGHT = 3
GET_RIGHT_DIST = 4
REPOSITION = 5

class CenterOnHall(Node):

    internalState = INTERNAL_IDLE
    prevIntState = None
    timeoutStart = None
    speed = None
    statusReqTimeoutStart = None

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'iNullZone'):
            raise RuntimeError(f'Missing param - {self.label}.iNullZone')
        self.iNullZone = self.iNullZone / 2
        if not hasattr(self, 'iHeading'):
            raise RuntimeError(f'Missing param - {self.label}.iHeading')
        if not hasattr(self, 'iTimeout'):
            raise RuntimeError(f'Missing param - {self.label}.iTimeout')

    def init(self):
        can.add_filter('CenterOnHall', CAN_STATUS_WALL_POS, 0x7FF)
        self.timeoutStart = time.monotonic()
        self.speed = bb.get('CenterOnHall', 'fMoveSpeed', default=0.2)
        self.internalState = ROTATE_LEFT
        self.prevIntState = -1

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        newState = (self.prevIntState != self.internalState)
        self.prevIntState = self.internalState

        if self.internalState == ROTATE_LEFT:
            if newState:
                print('CenterOnHall ROTATE_LEFT')
                alignHeading(30, self.iHeading - 45)
            else:
                if not isMoving():
                    self.internalState = GET_LEFT_DIST

        if self.internalState == GET_LEFT_DIST:
            if newState:
                print('CenterOnHall GET_LEFT_DIST')
                can.send('CenterOnHall', CAN_STATUS_WALL_POS, True)
            else:
                # Look for reply
                CANrx = can.get('CenterOnHall')
                if CANrx is not None:
                    if CANrx['id'] == CAN_STATUS_WALL_POS and not CANrx['rr']:
                        self.mmMidLZ = int.from_bytes(CANrx['data'][2:4], 'big') # mm
                        self.mmCornerLX = int.from_bytes(CANrx['data'][4:6], 'big', signed=True) # mm
                        self.mmCornerLZ = int.from_bytes(CANrx['data'][6:8], 'big') # mm
                        self.internalState = ROTATE_RIGHT

        if self.internalState == ROTATE_RIGHT:
            if newState:
                print('CenterOnHall ROTATE_RIGHT')
                alignHeading(30, self.iHeading + 45)
            else:
                if not isMoving():
                    self.internalState = GET_RIGHT_DIST

        if self.internalState == GET_RIGHT_DIST:
            if newState:
                print('CenterOnHall GET_RIGHT_DIST')
                can.send('CenterOnHall', CAN_STATUS_WALL_POS, True)
            else:
                # Look for reply
                CANrx = can.get('CenterOnHall')
                if CANrx is not None:
                    if CANrx['id'] == CAN_STATUS_WALL_POS and not CANrx['rr']:
                        self.mmMidRZ = int.from_bytes(CANrx['data'][2:4], 'big') # mm
                        self.mmCornerRX = int.from_bytes(CANrx['data'][4:6], 'big', signed=True) # mm
                        self.mmCornerRZ = int.from_bytes(CANrx['data'][6:8], 'big') # mm
                        self.internalState = REPOSITION

        elif self.internalState == REPOSITION:
            if newState:
                print('CenterOnHall REPOSITION')
                mmLX = None
                mmLZ = None
                if self.mmCornerLZ > 0:
                    mmLX = -610 + (self.mmCornerLZ - self.mmCornerLX) * 0.707
                    mmLZ = 0 - (self.mmCornerLZ + self.mmCornerLX) * 0.707
                elif self.mmMidLZ > 0:
                    mmLX = -610 + self.mmCornerLZ * 0.707

                mmRX = None
                mmRZ = None
                if self.mmCornerRZ > 0:
                    mmRX = 610 - (self.mmCornerRZ + self.mmCornerRX) * 0.707
                    mmRZ = 0 - (self.mmCornerRZ - self.mmCornerRX) * 0.707
                elif self.mmMidRZ > 0:
                    mmRX = 610 - self.mmCornerRZ * 0.707
                
                if mmLX is not None and mmRX is not None:
                    mmX = (mmLX + mmRX) / 2
                elif mmLX is not None:
                    mmX = mmLX
                elif mmRX is not None:
                    mmX = mmRX
                else: # unable to see the walls
                    return (Node.FAILURE, None)
                
                if abs(mmX) / 10 < self.iNullZone: # Close enough
                    return (Node.SUCCESS, None)
                
                cmMoveX = -round(mmX / 10)

                if cmMoveX > 0:
                    moveAngle(self.speed, 135, turn='l')
                    moveDist(self.speed, cmMoveX, when='b')
                    moveAngle(self.speed, 90, turn='r', when='b')
                else:
                    moveAngle(self.speed, 45, turn='r')
                    moveDist(self.speed, cmMoveX, when='b')
                    moveAngle(self.speed, 90, turn='l', when='b')
            else:
                if not isMoving():
                    return (Node.SUCCESS, None)

        # Has timeout occurred?
        if self.iTimeout > 0 and (time.monotonic() - self.timeoutStart) > self.iTimeout: # Timeout after iTimeout seconds
            return (Node.FAILURE, None)

        return (Node.RUNNING, None)

