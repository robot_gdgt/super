"""
LocateCan - Locate the can and save it in the blackboard

data.js: {
    "name": "Locate Can",
    "icon": "fas fa-eye",
    "cat": "action",
    "help": "Locate the can and save it in the blackboard.",
    "params": [
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
}
"""

# import logging
# import time

# import can_proxy_thread as can
from can_ids import CAN_STATUS_CAN_POS

from ..Blackboard import Blackboard as bb
from ..Node import Node
from .GetStatus import GetStatus

class LocateCan(GetStatus):
    def __init__(self, data):
        self.bbKey = 'can_pos'
        self.hStatusCANid = CAN_STATUS_CAN_POS
        super().__init__(data)

    def interpretData(self, data):
        wX = int.from_bytes(data[0:2], 'big', signed=True)
        wY = int.from_bytes(data[2:4], 'big')
        wZ = int.from_bytes(data[4:6], 'big')

        if wZ == 0: # invalid data so try again
            return Node.RUNNING

        bb.set(self.label, self.bbKey, (wX, wY, wZ))
        return Node.SUCCESS
