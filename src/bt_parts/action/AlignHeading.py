"""
AlignHeading - Compare heading to target and turn to match.

data.js: {
    "name": "Align to Heading",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "help": "Compare heading to target and turn to match (±1°).",
    "params": [
        {
            "key": "iTargetHeading",
            "label": "Target Heading (°)",
            "kind": "integer",
            "default": 0,
        },
        {
            "key": "iSpeed",
            "label": "Speed (QPPS)",
            "kind": "integer",
            "default": 20,
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 10,
        },
    ]
}
"""

# import logging
import time
# import random

# import can_proxy_thread as can
# from can_ids import CAN_STATUS_IMU

# from ..Blackboard import Blackboard as bb
from ..Node import Node
from ..movement import alignHeading, isMoving

class AlignHeading(Node):

    timeoutStart = None

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'iTargetHeading'):
            raise RuntimeError(f'Missing param - {self.label}.iTargetHeading')
        if not hasattr(self, 'iSpeed'):
            raise RuntimeError(f'Missing param - {self.label}.iSpeed')
        if not hasattr(self, 'iTimeout'):
            raise RuntimeError(f'Missing param - {self.label}.iTimeout')

    def init(self):
        # Not previously RUNNING
        self.timeoutStart = time.monotonic()
        alignHeading(self.iSpeed, self.iTargetHeading)

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member

        if not isMoving():
            return (Node.SUCCESS, None)

        # Has timeout occurred?
        if self.iTimeout > 0 and (time.monotonic() - self.timeoutStart) > self.iTimeout: # Timeout after iTimeout seconds
            return (Node.FAILURE, None)

        return (Node.RUNNING, None)
