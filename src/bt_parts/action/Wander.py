"""
Wander - move

data.js: {
    "name": "Wander",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "params": [
        {
            "key": "iturnrate",
            "label": "Turn Rate",
            "kind": "integer",
            "default": 1,
        },
    ]
}
"""

# import logging

import can_proxy_thread as can
from can_ids import *
from RollingMedian import RollingMedian

from ..Blackboard import Blackboard as bb
from ..Node import Node

class Wander(Node):

    def reset(self):
        super().reset()
        self.run_avg_dist = RollingMedian(7)
        self.run_avg_dir = RollingMedian(7)
        self.prev_dist = 0
        self.prev_dir = 0

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        self.run_avg_dist.add(int(bb.get(self.label, 'obst_dist')))
        self.run_avg_dir.add(int(bb.get(self.label, 'obst_dir')))
        dist = self.run_avg_dist.get()
        if dist is None:
            return (Node.RUNNING, None)
        dir = self.run_avg_dir.get()

        if dist != self.prev_dist or dir != self.prev_dir:
            dir2 = -(dir - 128) * int(self.iturnrate)
            if dir2 < -127:
                dir2 = -127
            elif dir2 > 127:
                dir2 = 127
            print(f'dist: {dist} dir: {dir} dir2: {dir2}')
            can.send('Wander', CAN_CMD_RC_MOVE, data=[dist + 128, dir2 + 128])
            self.prev_dist = dist
            self.prev_dir = dir

        return (Node.RUNNING, None)
