"""
ApproachFrig - Send a CAN command and wait for completion

data.js: {
    "name": "Approach Frig",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "help": "Move to initial frig position. Target X and Z are distance from the front, left corner",
    "params": [
        {
            "key": "iMoveToX",
            "label": "Move to X (cm)",
            "kind": "integer",
            "default": 0,
        },
        {
            "key": "iMoveToZ",
            "label": "Move to Z (cm)",
            "kind": "integer",
            "default": -36,
        },
        {
            "key": "iTargetHeading",
            "label": "Target Heading (°)",
            "kind": "integer",
            "default": 0,
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
}
"""

# import logging
import time

# import can_proxy_thread as can
# from can_ids import CAN_CMD_RC_MOVE_TO, CAN_STATUS_RC, RC_MOVE_IMMED, RC_MOVE_END_STOP

from ..Blackboard import Blackboard as bb
from ..Node import Node
from ..movement import moveTo, isMoving

INTERNAL_IDLE = 0
IS_MOVING = 1

class ApproachFrig(Node):

    internalState = INTERNAL_IDLE
    timeoutStart = None

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'iMoveToX'):
            raise RuntimeError(f'Missing param - {self.label}.iMoveToX')
        if not hasattr(self, 'iMoveToZ'):
            raise RuntimeError(f'Missing param - {self.label}.iMoveToZ')
        if not hasattr(self, 'iTargetHeading'):
            raise RuntimeError(f'Missing param - {self.label}.iTargetHeading')
        if not hasattr(self, 'iTimeout'):
            raise RuntimeError(f'Missing param - {self.label}.iTimeout')

    def init(self):
        frig_pos = bb.get(self.label, 'frig_pos')
        currentHeading = 0 # bb.get(self.label, 'fHeading')
        speed = bb.get(self.label, 'fMoveSpeed', 0.2)

        tX = round((frig_pos['left']['x'] / 10) + self.iMoveToX)
        tZ = round((frig_pos['left']['z'] / 10) + self.iMoveToZ)

        if abs(tX) < 5 and abs(tZ) < 5: # Close enought to frig
            bb.set(self.label, 'iInFrontOfFrig', 1)
            return (Node.SUCCESS, None)
        
        relHeading = round(self.iTargetHeading - currentHeading)
        if relHeading > 180: relHeading -= 360
        elif relHeading< -180: relHeading += 360

        moveTo(speed, tX, tZ, relHeading)

        self.timeoutStart = time.monotonic()
        self.internalState = IS_MOVING

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        # print(f'self.internalState={self.internalState}')
        if self.internalState == IS_MOVING:
            # Request action status
            if not isMoving():
                return (Node.SUCCESS, None)

        # Has timeout occurred?
        if self.iTimeout > 0 and (time.monotonic() - self.timeoutStart) > self.iTimeout: # Timeout after iTimeout seconds
            return (Node.FAILURE, None)

        return (Node.RUNNING, None)
