"""
Backup - move back away from a collision

data.js: {
    "name": "Backup",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "params": [
        {
            "key": "ims",
            "label": "Backup for ms",
            "kind": "integer",
        },
    ]
}
"""

import time
import logging

import can_proxy_thread as can
from can_ids import CAN_CMD_RC_MOVE
# from RollingMedian import RollingMedian

from ..Blackboard import Blackboard as bb
from ..Node import Node

class Backup(Node):
    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'ims'):
            self.ims = 0

    def reset(self):
        super().reset()
        self.start = None

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        if self.start is None:
            self.start = time.perf_counter()
#             print('start backup')
            can.send('Backup', CAN_CMD_RC_MOVE, data=[64, 128])
            bb.set(self.label, 'iAlerts', 0)
        elif (time.perf_counter() - self.start) * 1000 >= self.ims:
            self.start = None
#             print('end backup')
            can.send('Backup', CAN_CMD_RC_MOVE, data=[0, 0])
            return (Node.SUCCESS, None)
        return (Node.RUNNING, None)
