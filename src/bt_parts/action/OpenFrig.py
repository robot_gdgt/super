"""
OpenFrig - Align with the frig and open the door

data.js: {
    "name": "Open Frig",
    "icon": "fas fa-hand",
    "cat": "action",
    "help": "Align with the frig and open the door. Contact point is on the side, measured from the front, left corner.",
    "params": [
        {
            "key": "iDepth",
            "label": "Contact point (cm)",
            "kind": "integer",
            "default": 5,
        },
        {
            "key": "iBackup",
            "label": "Backup (cm)",
            "kind": "integer",
            "default": 15,
        },
        {
            "key": "iSpin",
            "label": "Spin (°)",
            "kind": "integer",
            "default": 30,
        },
    ]
}
"""

# import logging
import math
from time import sleep, monotonic

import can_proxy_thread as can
from can_ids import *

from ..arm import armInit, armJog, armIsMoving, armDoorExtend, JOINT_ROTATE, JOINT_PIVOT
from ..Blackboard import Blackboard as bb
from ..Node import Node
from ..movement import moveDist, moveAngle, disableMotors, isMoving, sendMotorCmd, QPPM

INTERNAL_IDLE = 0
MOVE1 = 1
INIT_ARM = 2
PIVOT_ARM = 3
EXTEND_ARM = 4
PIVOT_TO_FRIG = 5
BACK_UP = 6
ROTATE_GRIPPER = 7
SWING_DOOR = 8
FACE_FRIG = 9

class OpenFrig(Node):

    internalState = INTERNAL_IDLE
    prevIntState = None
    timeoutStart = None
    speed = None
    statusReqTimeoutStart = None

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'iDepth'):
            raise RuntimeError(f'Missing param - {self.label}.iDepth')
        if not hasattr(self, 'iBackup'):
            raise RuntimeError(f'Missing param - {self.label}.iBackup')
        if not hasattr(self, 'iSpin'):
            raise RuntimeError(f'Missing param - {self.label}.iSpin')

    def init(self):
        can.add_filter('OpenFrig', CAN_STATUS_DOOR_POS, 0x7FF)
        can.add_filter('OpenFrig', CAN_STATUS_ARM, 0x7FF)
        self.timeoutStart = monotonic()
        self.speed = bb.get('OpenFrig', 'fMoveSpeed', default=0.2)
        self.internalState = MOVE1
        self.prevIntState = -1

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        newState = (self.prevIntState != self.internalState)
        self.prevIntState = self.internalState

        if self.internalState == MOVE1:
            if newState:
                print('OpenFrig MOVE1')
                frig_pos = bb.get(self.label, 'frig_pos')
                wX = frig_pos['left']['x'] # mm
                wZ = frig_pos['left']['z'] # mm

                if wZ == 0: # invalid data so try again
                    return (Node.FAILURE, None)
                
                if wX < 20 or wX > 105: # Too far left or right
                    print(f'Too far left or right - wX: {wX}')
                    return (Node.FAILURE, None)

                self.angB = math.degrees(math.asin(wX / 409)) - 7.734
                print(f'angB: {self.angB}')
                if self.angB < -5 or self.angB > 10: # Arm does not pivot that far
                    print(f'Arm does not pivot that far - angB: {self.angB}')
                    return (Node.FAILURE, None)
                
                # Move?
                armZ = math.sqrt(409 * 409 - wX * wX) / 10 # cm
                deltaZ = wZ / 10 + self.iDepth - armZ - 5.8 # cm
                if abs(deltaZ) <= 5: # Close enough
                    self.internalState = INIT_ARM
                else:
                    moveDist(self.speed, abs(deltaZ), 'f' if deltaZ > 0 else 'r')

            else:
                if not isMoving():
                    self.internalState = INIT_ARM

        elif self.internalState == INIT_ARM:
            if newState:
                print('OpenFrig INIT_ARM')
                armInit()
            else:
                if not armIsMoving():
                    self.internalState = PIVOT_ARM

        elif self.internalState == PIVOT_ARM:
            if newState:
                print('OpenFrig PIVOT_ARM')
                # Move arm to left for clearance
                if self.angB < 8:
                    armJog(JOINT_PIVOT, self.angB - 8)
                else:
                    self.internalState = EXTEND_ARM
            else:
                if not armIsMoving():
                    self.internalState = EXTEND_ARM

        elif self.internalState == EXTEND_ARM:
            if newState:
                print('OpenFrig EXTEND_ARM')
                armDoorExtend(self.angB - 8)
            else:
                if not armIsMoving():
                    self.internalState = PIVOT_TO_FRIG

        elif self.internalState == PIVOT_TO_FRIG:
            if newState:
                print('OpenFrig PIVOT_ARM')
                disableMotors()
                sleep(0.5)
                pressIntoFrig = 3 # °
                # pivot = self.angB + pressIntoFrig
                # armJog(JOINT_PIVOT, pivot) # Move arm to right
                armDoorExtend(self.angB + pressIntoFrig)
            else:
                if not armIsMoving():
                    self.internalState = BACK_UP

        elif self.internalState == BACK_UP:
            if newState:
                print('OpenFrig BACK_UP')
                # moveDist(self.speed, self.iBackup, 'r', 'l', 150)
                #  42 SpeedDistanceM2(int32_t speed, uint32_t distance, uint8_t flag=0);
                speed = -int(QPPM / 5)
                dist = int(QPPM * self.iBackup / 100)
                data = []
                data.extend(speed.to_bytes(4, 'big', signed=True))
                data.extend(dist.to_bytes(4, 'big'))
                sendMotorCmd(41, data)
            else:
                if not isMoving():
                    self.internalState = ROTATE_GRIPPER

        elif self.internalState == ROTATE_GRIPPER:
            if newState:
                print('OpenFrig ROTATE_GRIPPER')
                armJog(JOINT_ROTATE, 90)
            else:
                if not armIsMoving():
                    self.internalState = SWING_DOOR

        elif self.internalState == SWING_DOOR:
            if newState:
                print('OpenFrig SWING_DOOR')
                moveAngle(self.speed, self.iSpin, 'f', 'r', 0, end='s')
            else:
                if not isMoving():
                    # self.internalState = FACE_FRIG
                    can.send('OpenFrig', CAN_ARM_PARK, False)
                    return (Node.SUCCESS, None)

        # elif self.internalState == FACE_FRIG:
        #     if newState:
        #         print('OpenFrig FACE_FRIG')
        #         can.send('OpenFrig', CAN_ARM_PARK, False)
        #         moveAngle(self.speed, self.iSpin, 'f', 'l', 0, end='s')
        #     else:
        #         if not isMoving():
        #             # self.internalState = INIT_ARM
        #             return (Node.SUCCESS, None)

        # Has timeoutSeconds occurred?
        # if timeoutSeconds > 0 and (monotonic() - self.timeoutStart) > timeoutSeconds: # Timeout after 10 seconds
        #     return (Node.FAILURE, None)

        return (Node.RUNNING, None)

