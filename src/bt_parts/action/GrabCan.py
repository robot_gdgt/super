"""
GrabCan - Locate and grab the can

data.js: {
    "name": "Grab Can",
    "icon": "fas fa-hand",
    "cat": "action",
    "help": "Locate and grab the can.",
    "params": [
        {
            "key": "iMinZ",
            "label": "Min Z (cm)",
            "kind": "integer",
            "default": 38,
        },
        {
            "key": "iMaxZ",
            "label": "Max Z (cm)",
            "kind": "integer",
            "default": 48,
        },
        {
            "key": "iMaxAng",
            "label": "Max ° from center",
            "kind": "integer",
            "default": 10,
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
}
"""

# import logging
import math
import time

import can_proxy_thread as can
from can_ids import *

from ..arm import armInit, armMoveTo, armIsMoving, armSendCmd
from ..Blackboard import Blackboard as bb
from ..Node import Node
from ..movement import moveDist, moveAngle, isMoving

INTERNAL_IDLE = 0
GET_CAN_POS = 1
REPOSITION = 2
INIT_ARM = 4
MOVE_ARM1 = 5
LOCATE_GRIP = 6
MOVE_ARM2 = 7
CLOSE_GRIP = 8
MOVE_ARM3 = 9

class GrabCan(Node):

    internalState = INTERNAL_IDLE
    prevIntState = None
    timeoutStart = None
    speed = None
    statusReqTimeoutStart = None

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'iMinZ'):
            raise RuntimeError(f'Missing param - {self.label}.iMinZ')
        if not hasattr(self, 'iMaxZ'):
            raise RuntimeError(f'Missing param - {self.label}.iMaxZ')
        if not hasattr(self, 'iMaxAng'):
            raise RuntimeError(f'Missing param - {self.label}.iMaxAng')
        if not hasattr(self, 'iTimeout'):
            raise RuntimeError(f'Missing param - {self.label}.iTimeout')

    def init(self):
        can.add_filter('GrabCan', CAN_STATUS_CAN_POS, 0x7FF)
        can.add_filter('GrabCan', CAN_STATUS_GRIP_POS, 0x7FF)
        can.add_filter('GrabCan', CAN_STATUS_ARM, 0x7FF)
        self.timeoutStart = time.monotonic()
        self.speed = bb.get('GrabCan', 'fMoveSpeed', default=0.2)
        self.internalState = GET_CAN_POS
        self.prevIntState = -1

    def deinit(self):
        can.del_filters('GrabCan')

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        newState = (self.prevIntState != self.internalState)
        self.prevIntState = self.internalState

        if self.internalState == GET_CAN_POS:
            if newState:
                print('GrabCan GET_CAN_POS')
                can.send('GrabCan', CAN_STATUS_CAN_POS, True)
            else:
                # Look for reply
                CANrx = can.get('GrabCan')
                if CANrx is not None:
                    if CANrx['id'] == CAN_STATUS_CAN_POS and not CANrx['rr']:
                        self.mmCanX = int.from_bytes(CANrx['data'][0:2], 'big', signed=True) # mm
                        self.mmCanY = int.from_bytes(CANrx['data'][2:4], 'big') # mm
                        self.mmCanZ = int.from_bytes(CANrx['data'][4:6], 'big') # mm
                        if self.mmCanZ < 1:
                            print('GrabCan Failed to get can pos')
                            return (Node.FAILURE, None)
                        else:
                            self.internalState = REPOSITION

        elif self.internalState == REPOSITION:
            if newState:
                print('GrabCan REPOSITION')
                cmCanDist = math.sqrt(self.mmCanX*self.mmCanX + self.mmCanZ*self.mmCanZ) / 10 # mm -> cm
                ang = math.degrees(math.atan(self.mmCanX / self.mmCanZ))

                # Move?
                if cmCanDist < self.iMinZ or cmCanDist > self.iMaxZ:
                    cmTargetDist = (self.iMinZ + self.iMaxZ) / 2
                    moveDist(self.speed / 2, (self.mmCanZ / 10) - cmTargetDist)
                # Turn?
                elif abs(ang) > self.iMaxAng:
                    moveAngle(self.speed / 2, ang / 3)

                else:
                    bb.set(self.label, 'iInFrontOfCan', 2)
                    self.internalState = INIT_ARM
            else:
                if not isMoving():
                    self.internalState = GET_CAN_POS

        elif self.internalState == INIT_ARM:
            if newState:
                print('GrabCan INIT_ARM')
                armInit()
            else:
                if not armIsMoving():
                    self.internalState = MOVE_ARM1

        elif self.internalState == MOVE_ARM1:
            if newState:
                print('GrabCan MOVE_ARM1')
                # Intermediate location
                y = self.mmCanY + 0
                z = self.mmCanZ - 80
                x = round(z / self.mmCanZ * self.mmCanX)
                armMoveTo(x, y, z, 0, 90 - 7.5)
            else:
                if not armIsMoving():
                    self.internalState = LOCATE_GRIP

        elif self.internalState == LOCATE_GRIP:
            if newState:
                print('GrabCan LOCATE_GRIP')
                can.send('GrabCan', CAN_STATUS_GRIP_POS, True)
            else:
                # Look for reply
                CANrx = can.get('GrabCan')
                if CANrx is not None:
                    if CANrx['id'] == CAN_STATUS_GRIP_POS and not CANrx['rr']:
                        self.cmGripX = int.from_bytes(CANrx['data'][0:2], 'big', signed=True) # mm
                        z = int.from_bytes(CANrx['data'][4:6], 'big') # mm
                        if z < 1:
                            self.internalState = LOCATE_GRIP
                            self.prevIntState = None
                        else:
                            self.internalState = MOVE_ARM2

        elif self.internalState == MOVE_ARM2:
            if newState:
                print('GrabCan MOVE_ARM2')
                adjX = self.cmGripX - self.mmCanX
                print(f'adjX: {adjX}')
                x = self.mmCanX - adjX
                y = self.mmCanY - 50
                z = self.mmCanZ - 0
                armMoveTo(x, y, z, 0, 90, 2)
            else:
                if not armIsMoving():
                    self.internalState = CLOSE_GRIP

        elif self.internalState == CLOSE_GRIP:
            if newState:
                print('GrabCan CLOSE_GRIP')
                armSendCmd(CAN_GRIP_CLOSE)
            else:
                if not armIsMoving():
                    self.internalState = MOVE_ARM3

        elif self.internalState == MOVE_ARM3:
            if newState:
                print('GrabCan MOVE_ARM3')
                armMoveTo(0, 302, 214, 0, 90)
            else:
                if not armIsMoving():
                    return (Node.SUCCESS, None)

        # Has timeout occurred?
        if self.iTimeout > 0 and (time.monotonic() - self.timeoutStart) > self.iTimeout: # Timeout after iTimeout seconds
            return (Node.FAILURE, None)

        return (Node.RUNNING, None)

