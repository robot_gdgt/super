"""
GetHeading - Get the current heading and save it in the blackboard

data.js: {
    "name": "Get Heading",
    "icon": "fas fa-compass",
    "cat": "action",
    "help": "Get the current heading and save it in the blackboard.",
    "params": [
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
}
"""

# import logging
# import time

# import can_proxy_thread as can
from can_ids import CAN_STATUS_IMU

from ..Blackboard import Blackboard as bb
from ..Node import Node
from .GetStatus import GetStatus

class GetHeading(GetStatus):
    def __init__(self, data):
        self.hStatusCANid = CAN_STATUS_IMU
        self.bbKey = 'fHeading'
        super().__init__(data)
        if not hasattr(self, 'iTimeout'):
            raise RuntimeError(f'Missing param - {self.label}.iTimeout')

    def interpretData(self, data):
        heading = int.from_bytes(data[0:2], 'big') / 10

        bb.set(self.label, self.bbKey, heading)
        return Node.SUCCESS
