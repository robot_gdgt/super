"""
SendCAN - Send a CAN command

data.js: {
    "name": "Send CAN",
    "icon": "fas fa-paper-plane",
    "cat": "action",
    "help": "Send a CAN command.",
    "params": [
        {
            "key": "hCANid",
            "label": "Send CAN ID 0x",
        },
        {
            "key": "CANdata",
            "label": "Send Data (space-delimited)",
        },
    ]
}
"""

# import logging

import can_proxy_thread as can
# from can_ids import *

# from ..Blackboard import Blackboard as bb
from ..Node import Node

class SendCAN(Node):

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'hCANid'):
            raise RuntimeError(f'Missing param - {self.label}.hCANid')

        data = []
        if hasattr(self, 'CANdata'):
            for d in self.CANdata.split():
                if d[0:2] == '0x' or d[0:2] == '0X': data.append(int(d, 0))
                else: data.append(int(d))
        self.CANdata = data

    def init(self):
        # Not previously RUNNING
        can.send(self.id, self.hCANid, False, self.CANdata)

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        return (Node.SUCCESS, None)
