"""
ElapseTime - Time an event

data.js: {
    "name": "Elapsed Time",
    "icon": "fas fa-hourglass-half",
    "cat": "action",
    "help": "Time an event.",
    "params": [
        {
            "key": "iEvent",
            "label": "Event",
            "kind": "select",
            "options": [
                {"value": 1, "text": "Start"},
                {"value": 2, "text": "End"}
            ]
        }
    ]
}
"""

import logging
import time

import can_proxy_thread as can
# from can_ids import *

from ..Blackboard import Blackboard as bb
from ..Node import Node

class ElapseTime(Node):

    timeoutStart = None

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'iEvent'):
            raise RuntimeError(f'Missing param - {self.label}.iEvent')

    def init(self):
        # print(f'{self.__class__.__name__} - GetStatus.init()')
        # Not previously RUNNING
        can.add_filter(self.id, self.hStatusCANid, 0x7FF)
        can.send(self.id, self.hStatusCANid, True)
        print(f'Send: {self.hStatusCANid}')
        self.timeoutStart = time.monotonic()

    # Do something useful
    def tock(self):
        # print(f'{self.__class__.__name__} - GetStatus.tock()')
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member

        # Look for reply
        CANrx = can.get(self.id)
        if CANrx is not None:
            if CANrx['id'] == self.hStatusCANid and not CANrx['rr']:
                reply = self.interpretData(CANrx['data'])

                if reply == Node.RUNNING:
                    # Send new status request
                    can.send(self.id, self.hStatusCANid, True)
                else:
                    # Set state so next call to action causes it to reassess can position
                    return (reply, None)

        # Has timeout occurred?
        if self.iTimeout > 0 and (time.monotonic() - self.timeoutStart) > self.iTimeout: # Timeout after iTimeout seconds
            return (Node.FAILURE, None)

        return (Node.RUNNING, None)
    
    def interpretData(self, data):
        bb.set(self.label, self.bbKey, data)                 # pylint: disable=no-member
        return Node.SUCCESS

    def deinit(self):
        can.del_filters(self.id)
