"""
CanOnTable - Locate table and place can on top

data.js: {
    "name": "Can on Table",
    "icon": "fas fa-hand",
    "cat": "action",
    "help": "Locate table and place can on top.",
    "params": [
        {
            "key": "iMinZ",
            "label": "Min Z (cm)",
            "kind": "integer",
            "default": 48,
        },
        {
            "key": "iMaxZ",
            "label": "Max Z (cm)",
            "kind": "integer",
            "default": 58,
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
}
"""

# import logging
import math
import time

import can_proxy_thread as can
from can_ids import *

from ..arm import armMoveTo, armIsMoving, armSendCmd
from ..Blackboard import Blackboard as bb
from ..Node import Node
from ..movement import moveDist, isMoving

INTERNAL_IDLE = 0
GET_TABLE_POS = 1
REPOSITION = 2
MOVE_ARM1 = 3
MOVE_TO_TABLE = 4
MOVE_ARM2 = 5
OPEN_GRIP = 6
BACKUP = 7

class CanOnTable(Node):

    internalState = INTERNAL_IDLE
    prevIntState = None
    timeoutStart = None
    speed = None
    statusReqTimeoutStart = None

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'iMinZ'):
            raise RuntimeError(f'Missing param - {self.label}.iMinZ')
        if not hasattr(self, 'iMaxZ'):
            raise RuntimeError(f'Missing param - {self.label}.iMaxZ')
        if not hasattr(self, 'iTimeout'):
            raise RuntimeError(f'Missing param - {self.label}.iTimeout')

    def init(self):
        can.add_filter('CanOnTable', CAN_STATUS_TABLE_POS, 0x7FF)
        can.add_filter('CanOnTable', CAN_STATUS_ARM, 0x7FF)
        self.timeoutStart = time.monotonic()
        self.speed = bb.get('CanOnTable', 'fMoveSpeed', default=0.2)
        self.internalState = GET_TABLE_POS
        self.prevIntState = -1

    def deinit(self):
        can.del_filters('CanOnTable')

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        newState = (self.prevIntState != self.internalState)
        self.prevIntState = self.internalState

        if self.internalState == GET_TABLE_POS:
            if newState:
                print('CanOnTable GET_TABLE_POS')
                can.send('CanOnTable', CAN_STATUS_TABLE_POS, True)
            else:
                # Look for reply
                CANrx = can.get('CanOnTable')
                if CANrx is not None:
                    if CANrx['id'] == CAN_STATUS_TABLE_POS and not CANrx['rr']:
                        self.mmTableZ = int.from_bytes(CANrx['data'][4:6], 'big') # mm
                        if self.mmTableZ < 1:
                            print('CanOnTable Failed to get can pos')
                            return (Node.FAILURE, None)
                        else:
                            self.internalState = REPOSITION

        elif self.internalState == REPOSITION:
            if newState:
                print('CanOnTable REPOSITION')
                # Move?
                if self.mmTableZ < self.iMinZ or self.mmTableZ > self.iMaxZ:
                    cmTargetDist = (self.iMinZ + self.iMaxZ) / 2
                    cmMove = (self.mmTableZ / 10) - cmTargetDist
                    moveDist(self.speed, cmMove)
                    self.mmTableZ -= cmMove * 10
                else:
                    bb.set(self.label, 'iInFrontOfTable', 1)
                    self.internalState = MOVE_ARM1
            else:
                if not isMoving():
                    self.internalState = MOVE_ARM1

        elif self.internalState == MOVE_ARM1:
            if newState:
                print('CanOnTable MOVE_ARM1')
                # Can above table
                armMoveTo(0, 570, 355, 0, 100)
            else:
                if not armIsMoving():
                    self.internalState = MOVE_TO_TABLE

        elif self.internalState == MOVE_TO_TABLE:
            if newState:
                print('CanOnTable MOVE_TO_TABLE')
                cmTargetDist = (self.iMinZ + self.iMaxZ) / 2
                moveDist(self.speed / 2, self.mmTableZ / 10 - 34)
            else:
                if not isMoving():
                    self.internalState = MOVE_ARM2

        elif self.internalState == MOVE_ARM2:
            if newState:
                print('CanOnTable MOVE_ARM2')
                # Can on table
                armMoveTo(0, 550, 355, 0, 95, 2)
            else:
                if not armIsMoving():
                    self.internalState = OPEN_GRIP

        elif self.internalState == OPEN_GRIP:
            if newState:
                print('CanOnTable OPEN_GRIP')
                armSendCmd(CAN_GRIP_OPEN)
            else:
                if not armIsMoving():
                    return (Node.SUCCESS, None)
                    self.internalState = BACKUP

        # elif self.internalState == BACKUP:
        #     if newState:
        #         print('CanOnTable BACKUP')
        #         moveDist(self.speed, -50)
        #     else:
        #         if not isMoving():
        #             armSendCmd(CAN_ARM_PARK)
        #             return (Node.SUCCESS, None)

        # Has timeout occurred?
        if self.iTimeout > 0 and (time.monotonic() - self.timeoutStart) > self.iTimeout: # Timeout after iTimeout seconds
            return (Node.FAILURE, None)

        return (Node.RUNNING, None)

