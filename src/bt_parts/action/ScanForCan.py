"""
ScanForCan - Look for the can, rotating if needed

data.js: {
    "name": "Scan for Can",
    "icon": "fas fa-eye",
    "cat": "action",
    "help": "Look for the can, rotating if needed.",
    "params": [
        {
            "key": "iMaxAng",
            "label": "Max ° from center",
            "kind": "integer",
            "default": 10,
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
}
"""

# import logging
import math
import time

import can_proxy_thread as can
from can_ids import *

from ..Blackboard import Blackboard as bb
from ..Node import Node
from ..movement import moveAngle, isMoving

INTERNAL_IDLE = 0
GET_CAN_POS = 1
REPOSITION = 2

class ScanForCan(Node):

    internalState = INTERNAL_IDLE
    prevIntState = None
    timeoutStart = None
    speed = None
    statusReqTimeoutStart = None

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'iMaxAng'):
            raise RuntimeError(f'Missing param - {self.label}.iMaxAng')
        if not hasattr(self, 'iTimeout'):
            raise RuntimeError(f'Missing param - {self.label}.iTimeout')

    def init(self):
        can.add_filter('ScanForCan', CAN_STATUS_CAN_POS, 0x7FF)
        self.timeoutStart = time.monotonic()
        self.internalState = GET_CAN_POS
        self.prevIntState = -1

    def deinit(self):
        can.del_filters('ScanForCan')

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        newState = (self.prevIntState != self.internalState)
        self.prevIntState = self.internalState

        if self.internalState == GET_CAN_POS:
            if newState:
                print('ScanForCan GET_CAN_POS')
                can.send('ScanForCan', CAN_STATUS_CAN_POS, True)
            else:
                # Look for reply
                CANrx = can.get('ScanForCan')
                if CANrx is not None:
                    if CANrx['id'] == CAN_STATUS_CAN_POS and not CANrx['rr']:
                        self.mmCanX = int.from_bytes(CANrx['data'][0:2], 'big', signed=True) # mm
                        self.mmCanZ = int.from_bytes(CANrx['data'][4:6], 'big') # mm
                        self.internalState = REPOSITION

        elif self.internalState == REPOSITION:
            if newState:
                print('ScanForCan REPOSITION')
                if self.mmCanZ:
                    ang = math.degrees(math.atan(self.mmCanX / self.mmCanZ))
                    # Turn?
                    if abs(ang) > self.iMaxAng:
                        moveAngle(0.1, ang / 3)
                    else:
                        return (Node.SUCCESS, None)
                else:
                    moveAngle(0.1, -self.iMaxAng / 2)
            else:
                if not isMoving():
                    self.internalState = GET_CAN_POS

        # Has timeout occurred?
        if self.iTimeout > 0 and (time.monotonic() - self.timeoutStart) > self.iTimeout: # Timeout after iTimeout seconds
            return (Node.FAILURE, None)

        return (Node.RUNNING, None)

