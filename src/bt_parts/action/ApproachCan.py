"""
ApproachCan - locate can and move to a position a specified distance from it

data.js: {
    "name": "Approach Can",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "help": "Locate the can and move to a specific distance from it.",
    "params": [
        {
            "key": "iMinZ",
            "label": "Min Z (cm)",
            "kind": "integer",
            "default": 38,
        },
        {
            "key": "iMaxZ",
            "label": "Max Z (cm)",
            "kind": "integer",
            "default": 48,
        },
        {
            "key": "iMaxAng",
            "label": "Max ° from center",
            "kind": "integer",
            "default": 10,
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
}
"""

# import logging
import math
import time

import can_proxy_thread as can
from can_ids import CAN_STATUS_INSIDE_POS, CAN_STATUS_CAN_POS

from ..Blackboard import Blackboard as bb
from ..Node import Node
from ..movement import moveTo, moveAngle, isMoving

INTERNAL_IDLE = 0
GET_CAN_POS = 1
MOVE1 = 2

class ApproachCan(Node):

    internalState = INTERNAL_IDLE
    prevIntState = None
    timeoutStart = None
    speed = None
    statusReqTimeoutStart = None

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'iMinZ'):
            raise RuntimeError(f'Missing param - {self.label}.iMinZ')
        if not hasattr(self, 'iMaxZ'):
            raise RuntimeError(f'Missing param - {self.label}.iMaxZ')
        if not hasattr(self, 'iMaxAng'):
            raise RuntimeError(f'Missing param - {self.label}.iMaxAng')
        if not hasattr(self, 'iTimeout'):
            raise RuntimeError(f'Missing param - {self.label}.iTimeout')
        self.maxAng = math.tan(math.radians(self.iMaxAng))

    def init(self):
        can.add_filter('ApproachCan', CAN_STATUS_CAN_POS, 0x7FF)
        self.timeoutStart = time.monotonic()
        self.speed = bb.get('OpenFrig', 'fMoveSpeed', default=0.2)
        self.internalState = GET_CAN_POS
        self.prevIntState = -1

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        newState = (self.prevIntState != self.internalState)
        self.prevIntState = self.internalState

        if self.internalState == GET_CAN_POS:
            if newState:
                print('ApproachCan GET_CAN_POS')
                can.send('ApproachCan', CAN_STATUS_CAN_POS, True)
            else:
                # Look for reply
                CANrx = can.get('ApproachCan')
                if CANrx is not None:
                    # if CANrx['id'] == CAN_STATUS_INSIDE_POS and not CANrx['rr']:
                    #     self.mmCanX = int.from_bytes(CANrx['data'][4:6], 'big', signed=True) / 10 # cm
                    #     self.mmCanZ = int.from_bytes(CANrx['data'][6:], 'big') / 10 # cm
                    #     if self.mmCanZ < 1:
                    #         self.internalState = GET_CAN_POS
                    #         self.prevIntState = None
                    #     else:
                    #         self.internalState = MOVE1
                        
                    #     # Correction for unknown reason
                    #     self.mmCanX = self.mmCanX * 0.8
                    #     self.mmCanZ = (self.mmCanZ - 11) * 0.8 + 11
                    if CANrx['id'] == CAN_STATUS_CAN_POS and not CANrx['rr']:
                        self.mmCanX = int.from_bytes(CANrx['data'][0:2], 'big', signed=True) # mm
                        self.mmCanY = int.from_bytes(CANrx['data'][2:4], 'big') # mm
                        self.mmCanZ = int.from_bytes(CANrx['data'][4:6], 'big') # mm
                        if self.mmCanZ < 1:
                            self.internalState = GET_CAN_POS
                            self.prevIntState = None
                        else:
                            self.internalState = MOVE1

        if self.internalState == MOVE1:
            if newState:
                cmCanDist = math.sqrt(self.mmCanX*self.mmCanX + self.mmCanZ*self.mmCanZ) / 10 # mm -> cm
                ang = math.degrees(math.atan(self.mmCanX / self.mmCanZ))

                # Move?
                if cmCanDist < self.iMinZ or cmCanDist > self.iMaxZ:
                    targetDist = (self.iMinZ + self.iMaxZ) / 2
                    tX = self.mmCanX * (cmCanDist - targetDist) / cmCanDist
                    tZ = self.mmCanZ * (cmCanDist - targetDist) / cmCanDist
                    moveTo(self.speed, tX, tZ, ang)
                # Turn?
                elif abs(ang) > self.iMaxAng:
                    moveAngle(self.speed, abs(ang), turn=('r' if ang > 0 else 'l'))

                else:
                    bb.set(self.label, 'iInFrontOfCan', 1)
                    return (Node.SUCCESS, None)
            else:
                if not isMoving():
                    return (Node.SUCCESS, None)

        # Has timeout occurred?
        if self.iTimeout > 0 and (time.monotonic() - self.timeoutStart) > self.iTimeout: # Timeout after iTimeout seconds
            return (Node.FAILURE, None)

        return (Node.RUNNING, None)
