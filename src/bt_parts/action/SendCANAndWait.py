"""
SendCANAndWait - Send a CAN command and wait for status

data.js: {
    "name": "Send CAN & Wait",
    "icon": "fas fa-paper-plane",
    "cat": "action",
    "help": "Send a CAN command and wait for status.",
    "params": [
        {
            "key": "hActCANid",
            "label": "Send CAN ID 0x",
        },
        {
            "key": "actCANdata",
            "label": "Send Data (space-delimited)",
        },
        {
            "key": "hStatusCANid",
            "label": "Status CAN ID 0x",
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
}
"""

import logging
import time

import can_proxy_thread as can
from can_ids import *

# from ..Blackboard import Blackboard as bb
from ..Node import Node

INTERNAL_IDLE = 0
COMMAND_SENT = 1
WAIT_FOR_STATUS = 2

class SendCANAndWait(Node):

    internalState = INTERNAL_IDLE
    prevInternalState = -1
    timeoutStart = None

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'hActCANid'):
            raise RuntimeError(f'Missing param - {self.label}.hActCANid')
        if not hasattr(self, 'hStatusCANid'):
            raise RuntimeError(f'Missing param - {self.label}.hStatusCANid')
        if not hasattr(self, 'iTimeout'):
            raise RuntimeError(f'Missing param - {self.label}.iTimeout')

        data = []
        if hasattr(self, 'actCANdata'):
            for d in self.actCANdata.split():
                if d[0:2] == '0x' or d[0:2] == '0X': data.append(int(d, 0))
                else: data.append(int)
        self.actCANdata = data

    def init(self):
        # Not previously RUNNING
        can.add_filter(self.id, self.hStatusCANid, 0x7FF)
        can.send(self.id, self.hActCANid, False, self.actCANdata)
        self.timeoutStart = time.monotonic()
        self.internalState = COMMAND_SENT
        return (Node.RUNNING, None)

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member

        if self.internalState == COMMAND_SENT:
            # Request action status
            can.send(self.id, self.hStatusCANid, True)
            self.internalState = WAIT_FOR_STATUS
            return (Node.RUNNING, None)

        if self.internalState == WAIT_FOR_STATUS:
            # Look for reply
            CANrx = can.get(self.id)
            if CANrx is not None:
                if CANrx['id'] == self.hStatusCANid and not CANrx['rr']:
                    reply = self.interpretData(CANrx['data'])

                    if reply == Node.RUNNING:
                        # Send new status request
                        can.send(self.id, self.hStatusCANid, True)
                    else:
                        # Set state so next call to action causes it to reassess can position
                        self.internalState == INTERNAL_IDLE
                        return (reply, None)

        # Has timeout occurred?
        if self.iTimeout > 0 and (time.monotonic() - self.timeoutStart) > self.iTimeout: # Timeout after iTimeout seconds
            return (Node.FAILURE, None)

        return (Node.RUNNING, None)
    
    def interpretData(self, data):
        if len(data) > 0 and data[0] == 0: # Action complete
            return Node.SUCCESS

    def deinit(self):
        can.del_filters(self.id)
