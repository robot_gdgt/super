"""
Pause

data.js: {
    "name": "Pause",
    "label": "Pause {fSecs} seconds",
    "icon": "fas fa-hourglass-half",
    "cat": "action",
    "params": [
        {
            "key": "fSecs",
            "label": "Seconds",
            "kind": "float",
        },
        {
            "key": "iReply",
            "label": "Reply",
            "kind": "select",
            "options": [
                {"value": SUCCESS, "text": "Success"},
                {"value": FAILURE, "text": "Failure"}
            ]
        }
    ]
}
"""

import logging
import time

from ..Node import Node

class Pause(Node):
    startTime = None

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'fSecs'):
            raise RuntimeError(f'Missing param - {self.label}.fSecs')
        if not hasattr(self, 'iReply'):
            raise RuntimeError(f'Missing param - {self.label}.iReply')

    def init(self):
        self.startTime = time.monotonic()

    # Do nothing useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        if (time.monotonic() - self.startTime) >= self.fSecs:
            return (self.iReply, None)
        return (Node.RUNNING, None)
