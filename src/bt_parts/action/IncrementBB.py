"""
Increment BB value

data.js: {
    "name": "Inc BB Value",
    "label": "[{bbKey}] += {iInc}",
    "icon": "fas fa-cog",
    "cat": "action",
    "params": [
        {
            "key": "bbKey",
            "label": "Key to set",
            "kind": "key"
        },
        {
            "key": "iInc",
            "label": "± Value",
            "default": 1
        }
    ]
}
"""

# import logging

from ..Blackboard import Blackboard as bb
from ..Node import Node

class IncrementBB(Node):
    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'bbKey'):
            raise RuntimeError(f'Missing param - {self.label}.bbKey')
        if not hasattr(self, 'iInc'):
            raise RuntimeError(f'Missing param - {self.label}.iInc')
        self.iInc = int(self.iInc)

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        v = bb.get(self.label, self.bbKey, 0)
        bb.set(self.label, self.bbKey, v + self.iInc)                # pylint: disable=no-member
        return (Node.SUCCESS, None)
