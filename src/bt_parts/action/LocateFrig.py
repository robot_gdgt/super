"""
LocateFrig - Locate the frig and save it in the blackboard

data.js: {
    "name": "Locate Frig",
    "icon": "fas fa-eye",
    "cat": "action",
    "help": "Locate the frig and save it in the blackboard.",
    "params": [
        {
            "key": "hStatusCANid",
            "label": "Range",
            "kind": "select",
            "options": [
                {"value": "CAN_STATUS_DOOR_POS", "text": "Close"},
                {"value": "CAN_STATUS_FRIG_POS", "text": "Far"},
            ]
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 0,
        },
    ]
}
"""

# import logging
# import time

# import can_proxy_thread as can
from can_ids import CAN_STATUS_FRIG_POS, CAN_STATUS_DOOR_POS

from ..Blackboard import Blackboard as bb
from ..Node import Node
from .GetStatus import GetStatus

class LocateFrig(GetStatus):
    def __init__(self, data):
        self.bbKey = 'frig_pos'
        super().__init__(data)

    def interpretData(self, data):
        lx = int.from_bytes(data[0:2], 'big', signed=True)
        lz = int.from_bytes(data[2:4], 'big')
        rx = int.from_bytes(data[4:6], 'big', signed=True)
        rz = int.from_bytes(data[6:8], 'big')

        if lz == 0: # invalid data so try again
            return Node.RUNNING

        bb.set(self.label, self.bbKey, {"left":{"x":lx, "z":lz}, "right":{"x":rx, "z":rz}})
        return Node.SUCCESS
