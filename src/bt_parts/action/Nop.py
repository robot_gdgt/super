"""
Nop

data.js: {
    "name": "Do Nothing",
    "icon": "far fa-circle",
    "cat": "action",
    "params": [
        {
            "key": "iReply",
            "label": "Reply",
            "kind": "select",
            "options": [
                {"value": SUCCESS, "text": "Success"},
                {"value": FAILURE, "text": "Failure"},
                {"value": RUNNING, "text": "Running"}
            ]
        }
    ]
}
"""

import logging

from ..Node import Node

class Nop(Node):
    # Do nothing useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        if hasattr(self, 'iReply'):
            return (self.iReply, None)                   # pylint: disable=no-member
        return (Node.FAILURE, None)