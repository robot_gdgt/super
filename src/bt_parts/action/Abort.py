"""
Abort - TTS and sounds

data.js: {
    "name": "Abort",
    "icon": "fas fa-stop",
    "cat": "action",
    "help": "Say something, then abort.",
    "params": [
        {
            "key": "text",
            "label": "Speak Text",
            "kind": "textarea"
        },
    ]
}
"""

import logging

# import can_bus_thread
# from can_ids import *
from speak_direct_can_thread import speak_direct

from ..Node import Node

class Abort(Node):
    # Return Abort but say something first
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        if hasattr(self, 'text'):
            print(f'speak {self.text}')
            speak_direct(self.text)

        return (Node.ABORT, None)
