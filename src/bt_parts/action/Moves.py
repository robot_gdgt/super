"""
Moves

data.js: {
    "name": "Moves",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "help": "JSON data = array of objects with these values:<br>\
    dist = distance (cm) or<br>\
    ang = angle (°) and<br>\
    rad = radius (cm)<br>\
    sp = speed† (m/s)<br>\
    sf = speed factor† (* sys speed)<br>\
    turn = turn† (r/l)<br>\
    dir = direction† (f/r)<br>\
    end = end† (c/s - last always s)<br>\
    † = optional - 1st value is default\
    ",
    "params": [
        {
            "key": "moves",
            "label": "Moves JSON data",
            "kind": "textarea",
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 10
        },
    ]
}
"""

# import logging
import json
from time import sleep, monotonic

from ..Blackboard import Blackboard as bb
from ..Node import Node
from ..movement import moveDist, moveAngle, isMoving

INTERNAL_IDLE = 0
SLEEP1 = 1
MOVING = 2
SLEEP2 = 3
CONFIRM = 4

class Moves(Node):

    internalState = INTERNAL_IDLE
    timeoutStart = None

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'moves'):
            raise RuntimeError(f'Missing param - {self.label}.moves')
        try:
            self.moves = json.loads(self.moves)
        except:
            raise RuntimeError(f'Invalid moves data - {self.label}')
        if not hasattr(self, 'iTimeout'):
            raise RuntimeError(f'Missing param - {self.label}.iTimeout')

    def init(self):
        # Not previously RUNNING
        sysSpeed = bb.get('Move', 'fMoveSpeed', default=0.2)
        lastM = len(self.moves) - 1
        for m, move in enumerate(self.moves):
            speed = sysSpeed
            if 'sp' in move: speed = move['sp']
            elif 'sf' in move: speed = sysSpeed * move['sf']

            dir = move['dir'] if 'dir' in move else 'f'
            when = 'i' if m == 0 else 'b'
            end = move['end'] if 'end' in move else ('s' if m == lastM else 'c')

            if 'dist' in move:
                moveDist(speed, move['dist'], dir, when, end)
            elif 'ang' in move and 'rad' in move:
                turn = move['turn'] if 'turn' in move else 'r'
                moveAngle(speed, move['ang'], dir, turn, move['rad'], when, end)
            else:
                print(f'Failed to queue: {move}')
        self.timeoutStart = monotonic()
        self.internalState = MOVING

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member

        # if self.internalState == SLEEP1:
        #     if monotonic() - self.timeoutStart > 5:
        #         self.internalState = MOVING

        # elif self.internalState == MOVING:
        #     if not isMoving():
        #         return (Node.SUCCESS, None)

        # elif self.internalState == SLEEP2:
        #     if not isMoving():
        #         self.timeoutStart = monotonic()
        #         self.internalState = CONFIRM

        # elif self.internalState == CONFIRM:
        #     if monotonic() - self.timeoutStart > 1:
        #     if not isMoving():
        #         return (Node.SUCCESS, None)

        if not isMoving():
            if monotonic() - self.timeoutStart > 1:
                return (Node.SUCCESS, None)
            self.timeoutStart = monotonic()

        # Has timeout occurred?
        if self.iTimeout > 0 and (monotonic() - self.timeoutStart) > self.iTimeout: # Timeout after iTimeout seconds
            print(f'{self.label} timeout')
            return (Node.FAILURE, None)

        return (Node.RUNNING, None)
