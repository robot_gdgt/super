"""
Move

data.js: {
    "name": "Move",
    "icon": "fas fa-person-walking",
    "cat": "action",
    "params": [
        {
            "key": "iType",
            "label": "Type",
            "kind": "select",
            "options": [
                {"value": "CAN_CMD_RC_MOVE_DIST", "text": "Distance"},
                {"value": "CAN_CMD_RC_MOVE_ANGLE", "text": "Angle"}
            ]
        },
        {
            "key": "iDistAng",
            "label": "Distance (cm) or Angle (°)",
            "kind": "integer"
        },
        {
            "key": "dir",
            "label": "Direction",
            "kind": "select",
            "options": [
                {"value": "f", "text": "Forward"},
                {"value": "r", "text": "Reverse"}
            ],
            "default": "f"
        },
        {
            "key": "turn",
            "label": "Turn",
            "kind": "select",
            "options": [
                {"value": "s", "text": "Straight"},
                {"value": "l", "text": "Left"},
                {"value": "r", "text": "Right"}
            ],
            "default": "s"
        },
        {
            "key": "iRadius",
            "label": "Radius (cm)",
            "kind": "integer"
        },
        {
            "key": "when",
            "label": "When",
            "kind": "select",
            "options": [
                {"value": "i", "text": "Immediate"},
                {"value": "b", "text": "Buffered"}
            ],
            "default": "i"
        },
        {
            "key": "end",
            "label": "End",
            "kind": "select",
            "options": [
                {"value": "s", "text": "Stop"},
                {"value": "c", "text": "Coast"}
            ],
            "default": "s"
        },
        {
            "key": "iTimeout",
            "label": "Timeout (seconds)",
            "kind": "integer",
            "default": 10
        },
    ]
}
"""

# import logging
import time

from ..Blackboard import Blackboard as bb
from ..Node import Node
from ..movement import move, isMoving

class Move(Node):

    timeoutStart = None

    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'iType'):
            raise RuntimeError(f'Missing param - {self.label}.iType')
        if not hasattr(self, 'iDistAng'):
            raise RuntimeError(f'Missing param - {self.label}.iDistAng')
        if not hasattr(self, 'dir'):
            raise RuntimeError(f'Missing param - {self.label}.dir')
        if not hasattr(self, 'turn'):
            raise RuntimeError(f'Missing param - {self.label}.turn')
        if self.turn == 's':
            self.iRadius = 0
        if not hasattr(self, 'iRadius'):
            raise RuntimeError(f'Missing param - {self.label}.iRadius')
        if not hasattr(self, 'when'):
            raise RuntimeError(f'Missing param - {self.label}.when')
        if not hasattr(self, 'end'):
            raise RuntimeError(f'Missing param - {self.label}.end')
        if not hasattr(self, 'iTimeout'):
            raise RuntimeError(f'Missing param - {self.label}.iTimeout')

    def init(self):
        # Not previously RUNNING
        self.timeoutStart = time.monotonic()
        speed = bb.get('Move', 'fMoveSpeed', default=0.2)
        move(self.iType, speed, self.iDistAng, self.dir, self.turn, self.iRadius, self.when, self.end)

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member

        if not isMoving():
            return (Node.SUCCESS, None)

        # Has timeout occurred?
        if self.iTimeout > 0 and (time.monotonic() - self.timeoutStart) > self.iTimeout: # Timeout after iTimeout seconds
            print(f'{self.label} timeout')
            return (Node.FAILURE, None)

        return (Node.RUNNING, None)
