"""
Deco is the basis for all decorators
"""

from .Base import Base

class Deco(Base):
    def __init__(self, data, node):
        self.label = '?'
        self.node = node

        # Add deco properties
        for k, v in data.items():
            if k[0] == 'i':
                v = int(v)
            elif k[0] == 'f':
                v = float(v)
            setattr(self, k, v)
        super().__init__()

    def tick(self, state=None):
        return state
