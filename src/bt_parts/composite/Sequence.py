"""
Sequence ticks each child until one returns FAILURE or RUNNING

data.js: {
    "name": "Sequence",
    "icon": "far fa-arrow-alt-circle-right",
    "cat": "composite",
    "help": "Execute each child in order until one returns Failure.",
}
"""

from ..Node import Node
from ..Composite import Composite

class Sequence(Composite):
    # Call the children one after the other
    def tock(self):
        for i in range(0, len(self.children)):
            if self.children[i].isDisabled: continue
            reply = self.children[i].tick()
            if reply != Node.SUCCESS:
                if self.childIndex > i:
                    self.children[self.childIndex].abort()
                self.childIndex = i
                return (reply, self.children[i].id)
        return (Node.SUCCESS, None)
