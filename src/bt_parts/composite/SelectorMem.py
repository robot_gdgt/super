"""
Selector ticks one child until one returns SUCCESS or RUNNING

data.js: {
    "name": "Selector*",
    "icon": "fas fa-question-circle",
    "cat": "composite",
    "help": "Execute each child in order until one returns Success. Returns to Running child.",
}
"""

from ..Node import Node
from ..Composite import Composite

class SelectorMem(Composite):
    # Call the children one after the other
    def tock(self):
        for i in range(self.childIndex, len(self.children)):
            if self.children[i].isDisabled: continue
            reply = self.children[i].tick()
            if reply == Node.RUNNING:
                if self.childIndex > i:
                    self.children[self.childIndex].abort()
                self.childIndex = i
                return (reply, self.children[i].id)
            if reply != Node.FAILURE:
                if self.childIndex > i:
                    self.children[self.childIndex].abort()
                return (reply, self.children[i].id)
        return (Node.FAILURE, None)
