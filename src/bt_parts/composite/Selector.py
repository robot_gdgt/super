"""
Selector ticks one child until one returns SUCCESS or RUNNING

data.js: {
    "name": "Selector",
    "icon": "far fa-question-circle",
    "cat": "composite",
    "help": "Execute each child in order until one returns Success.",
}
"""

from ..Node import Node
from ..Composite import Composite

class Selector(Composite):
    # Call the children one after the other
    def tock(self):
        for i in range(0, len(self.children)):
            if self.children[i].isDisabled: continue
            reply = self.children[i].tick()
            if reply != Node.FAILURE:
                if self.childIndex > i:
                    self.children[self.childIndex].abort()
                self.childIndex = i
                return (reply, self.children[i].id)
        return (Node.FAILURE, None)
