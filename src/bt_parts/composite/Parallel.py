"""
Parallel ticks all children until one returns FAILURE or RUNNING or all return SUCCESS

data.js: {
    "name": "Parallel",
    "icon": "fas fa-bars",
    "cat": "composite"
}
"""

# import logging

from ..Node import Node
from ..Composite import Composite

class Parallel(Composite):
    def reset(self):
        self.childReplies = [0] * len(self.children)

    # Call all of the children
    def tock(self):
        assert self.children, f'{self.label} has no children'
        firstTime = self.childReplies[0] == 0
        counts = {
            Node.SUCCESS: 0,
            Node.FAILURE: 0,
            Node.RUNNING: 0,
        }
        for i in range(0, len(self.children)):
            if self.children[i].isDisabled: continue
            if firstTime or self.childReplies[i] == Node.RUNNING:
                self.childReplies[i] = self.children[i].tick()
                if self.childReplies[i] == Node.ABORT:
                    return (Node.ABORT, self.children[i].id)
            # logging.info(f'{i} = {self.childReplies[i]}')
            counts[self.childReplies[i]] += 1

        if counts[Node.FAILURE] > 0:
            self.reset()
            return (Node.FAILURE, None)
        if counts[Node.RUNNING] > 0:
            return (Node.RUNNING, None)
        self.reset()
        return (Node.SUCCESS, None)
