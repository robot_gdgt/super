"""
Sequence ticks each child until one returns FAILURE or RUNNING

data.js: {
    "name": "Sequence*",
    "icon": "fas fa-arrow-alt-circle-right",
    "cat": "composite",
    "help": "Execute each child in order until one returns Failure. Returns to Running child.",
}
"""

from ..Node import Node
from ..Composite import Composite

class SequenceMem(Composite):
    # Call the children one after the other
    def tock(self):
        for i in range(self.childIndex, len(self.children)):
            if self.children[i].isDisabled: continue
            reply = self.children[i].tick()
            if reply == Node.RUNNING:
                if self.childIndex > i:
                    self.children[self.childIndex].abort()
                self.childIndex = i
                return (reply, self.children[i].id)
            if reply != Node.SUCCESS:
                if self.childIndex > i:
                    self.children[self.childIndex].abort()
                return (reply, self.children[i].id)
        return (Node.SUCCESS, None)
