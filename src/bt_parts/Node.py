"""
Node is the basis for all action, decision and composite nodes
"""

from importlib import import_module
import logging
# import json
# import importlib.util
# import os
from time import sleep

from .Base import Base
from .utils import replyStr
import web_socket_server

# logging.info(__file__)
# logging.info(globals())

class Node(Base):
    # Class constants
    NOT_READY = 0
    SUCCESS = 1
    FAILURE = 2
    RUNNING = 3
    ABORT = -1

    importedModules = {}

    def __init__(self, data):
        self.sourceId = None
        self.replyDeco = []
        self.shieldDeco = []
        self.execDeco = []

        # logging.info(f"--> {__file__} - data: ", end='')
        # logging.info(data)

        # Add node properties
        # for k, v in data.items():
        #     if k != 'children' and k != 'replyDeco' and k != 'shieldDeco' and k != 'execDeco':
        #         setattr(self, k, v)
        self.id = data['id'] if 'id' in data else '?'
        self.type = data['type'] if 'type' in data else '?'
        self.label = data['nodeLabel'] if 'nodeLabel' in data else '?'
        self.isDisabled = data['isDisabled'] if 'isDisabled' in data else False
        if 'params' in data:
            for k, v in data['params'].items():
                if str(v)[0:2] == '0x' or str(v)[0:2] == '0X':
                    v = int(v, 0)
                elif k[0] == 'i':
                    v = int(v)
                elif k[0] == 'h':
                    v = int(v, 16)
                elif k[0] == 'f':
                    v = float(v)
                setattr(self, k, v)

        # Add decorators
        for cat in ['replyDeco', 'shieldDeco', 'execDeco']:
            if cat in data:
                for dtype, deco in data[cat].items():
                    className = dtype[0:1].upper() + dtype[1:]
                    deco['label'] = dtype
                    fullname = f'.{cat}.{className}'
                    if fullname not in Node.importedModules:
                        # print(f'from bt_parts.{cat} import {className} as {className}')
                        try:
                            temp = import_module(f'.{className}', package=f'bt_parts.{cat}')
                            Node.importedModules[fullname] = getattr(temp, className)
                        except ImportError as err:
                            logging.info(f"Failed to import {fullname}: {err}")
                            continue
                    klass = Node.importedModules[fullname]
                    logging.info(f"{__name__} loading: {klass}")
                    getattr(self, cat).append(klass(deco, self))
        super().__init__()

    def reset(self):
        # print(f'{self.__class__.__name__} - Node.reset()')
        logging.info(f'{self.label} is reset')
        self.state = Node.NOT_READY
        for cat in ['replyDeco', 'shieldDeco', 'execDeco']:
            for deco in getattr(self, cat):
                deco.reset()

    # Need some methods for processing the return signal through the decorators
    def tick(self):
        # print(f'{self.__class__.__name__} - Node.tick()')
        # Consider the guard decorators first
        reply = None
        for d in self.shieldDeco:
            reply = d.tick()
            if reply:
                break

        sourceId = None

        # If the guard decorators all return None then perform node's task
        if reply == None:
            if self.state != Node.RUNNING:
                self.init()
            (reply, sourceId) = self.tock()

            if reply != Node.ABORT:
                # Consider exec decorators
                for d in self.execDeco:
                    reply = d.tick(reply)

                # Lastly, pass the outcome through the reply decorators
                for d in self.replyDeco:
                    reply = d.tick(reply)

        logging.info(f'{self.label} is returning {reply} = {replyStr(reply)}')

        if reply != self.state or sourceId != self.sourceId:
            web_socket_server.broadcast('bt', {'id': self.id, 'state': reply, 'sourceId': sourceId})
            self.state = reply
            self.sourceId = sourceId
            # if(hasattr(self, 'children') and sourceId):
            #     foundChild = False
            #     for i in range(0, len(self.children)):
            #         if foundChild == False and self.children[i].id == sourceId:
            #             foundChild = True
            #         elif foundChild == True:
            #             self.children[i].abort()

        if reply != Node.RUNNING:
            self.deinit()

        self.state = reply

        return reply

    def tock(self):
        # print(f'{self.__class__.__name__} - Node.tick()')
        return (Node.SUCCESS, None)

    def abort(self):
        # print(f'{self.__class__.__name__} - Node.abort()')
        if self.state != Node.ABORT:
            web_socket_server.broadcast('bt', {'id': self.id, 'state': Node.ABORT, 'sourceId': None})
        self.state = Node.ABORT

    def teardown(self):
        # print(f'{self.label}.Node.teardown()')
        self.replyDeco = None
        self.shieldDeco = None
        self.execDeco = None
        super().teardown()
