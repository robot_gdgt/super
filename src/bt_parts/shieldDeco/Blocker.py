"""
Shield if condition met

data.js: {
    "name": "Blocker",
    "label": "Always reply {iReply}",
    "icon": "fas fa-shield-alt",
    "cat": "shieldDeco",
    "help": "Do not execute node and always return specified state.",
    "params": [
        {
            "key": "iReply",
            "label": "Reply",
            "kind": "select",
            "options": [
                {"value": SUCCESS, "text": "Success"},
                {"value": FAILURE, "text": "Failure"},
                {"value": RUNNING, "text": "Running"},
                {"value": ABORT, "text": "Abort"}
            ]
        },
    ]
}
"""

from ..Deco import Deco
# from ..Node import Node

class Blocker(Deco):
    def __init__(self, data, node):
        super().__init__(data, node)
        if not hasattr(self, 'iReply'):
            raise RuntimeError(f'Missing param - {node.label}.{self.label}.iReply')

    def tick(self):
        return self.iReply                              # pylint: disable=no-member
