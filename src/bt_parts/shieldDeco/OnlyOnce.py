"""
Run once then return previous reply

data.js: {
    "name": "Only Once",
    "icon": "fas fa-shield-alt",
    "cat": "shieldDeco",
    "help": "Return previously returned state after first Success or Failure",
}
"""

from ..Deco import Deco
from ..Node import Node

class OnlyOnce(Deco):

#     def __init__(self, data, node):
#         self.didThis = False
#         super().__init__(data, node)

    def tick(self):
        if self.node.state == Node.SUCCESS or self.node.state == Node.FAILURE:
            return self.node.state
        return None

    # def reset(self):
    #     self.didThis = False
