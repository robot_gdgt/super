"""
Shield if condition met

data.js: {
    "name": "Test BB",
    "label": "If [{bbKey}] is {ibool} reply {iReply}",
    "icon": "fas fa-shield-alt",
    "cat": "shieldDeco",
    "help": "Return specified state if blackboard value is true/false without triggering node.",
    "params": [
        {
            "key": "bbKey",
            "label": "Return reply if<br>blackboard key",
            "kind": "key"
        },
        {
            "key": "compare",
            "label": "Is",
            "kind": "select",
            "options": [
                {"value": "=", "text": "="},
                {"value": "!=", "text": "!="},
                {"value": "<", "text": "<"},
                {"value": "<=", "text": "<="},
                {"value": ">", "text": ">"},
                {"value": ">=", "text": ">="},
                {"value": "null", "text": "is null"},
            ]
        },
        {
            "key": "value",
            "label": "Value"
        },
        {
            "key": "iReply",
            "label": "Reply",
            "kind": "select",
            "options": [
                {"value": SUCCESS, "text": "Success"},
                {"value": FAILURE, "text": "Failure"},
                {"value": RUNNING, "text": "Running"}
            ]
        },
    ]
}
"""

from ..Blackboard import Blackboard as bb
from ..Deco import Deco
# from ..Node import Node

class TestBBshield(Deco):
    def __init__(self, data, node):
        super().__init__(data, node)
        if not hasattr(self, 'bbKey'):
            raise RuntimeError(f'Missing param - {node.label}.{self.label}.bbKey')
        if not hasattr(self, 'compare'):
            raise RuntimeError(f'Missing param - {node.label}.{self.label}.compare')
        if not hasattr(self, 'value'):
            raise RuntimeError(f'Missing param - {node.label}.{self.label}.value')
        if not hasattr(self, 'iReply'):
            raise RuntimeError(f'Missing param - {node.label}.{self.label}.iReply')
        if self.bbKey[0] == 'i':
            self.value = int(self.value)
        elif self.bbKey[0] == 'f':
            self.value = float(self.value)

    def tick(self):
        b = bb.get(self.label, self.bbKey)
        c = self.compare
        v = self.value
        # print(f'TestBB: {b} ({type(b)}) {c} {v}')
        if c == '=':
                if b == v:
                    return self.iReply                              # pylint: disable=no-member
        elif c == '!=':
                if b != v:
                    return self.iReply                              # pylint: disable=no-member
        elif c == '<':
                if b < v:
                    return self.iReply                              # pylint: disable=no-member
        elif c == '<=':
                if b <= v:
                    return self.iReply                              # pylint: disable=no-member
        elif c == '>':
                if b > v:
                    return self.iReply                              # pylint: disable=no-member
        elif c == '>=':
                if b >= v:
                    return self.iReply                              # pylint: disable=no-member
        else: # if c == 'null':
                if b == None:
                    return self.iReply                              # pylint: disable=no-member
        return None
