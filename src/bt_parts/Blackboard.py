"""
Methods for accessing the blackboard
"""

# import json
import logging

class Blackboard:

    dict = {}
    log = []

    @classmethod
    def get(klass, caller, key, default=None):
        if key in klass.dict:
            value = klass.dict[key]
            logging.debug(f'{caller} get bb key {key} = {value}')
        else:
            value = default
            logging.debug(f'{caller} bb key {key} not found - default used: {value}')
        return value

    @classmethod
    def set(klass, caller, key, value):
        # if key[0] == 'i':
        #     value = int(value)
        # elif key[0] == 'f':
        #     value = float(value)
        # print(f'{caller} set bb key {key} = {value}')
        logging.debug(f'{caller} set bb key {key} = {value}')
        klass.dict[key] = value
        klass.log.append({'caller':caller, 'key':key, 'value':value})

    @classmethod
    def delete(klass, caller, key):
        logging.debug(f'{caller} del bb key {key}')
        if key in klass.dict:
            del klass.dict[key]

    @classmethod
    def getLog(klass):
        l = klass.log.copy()
        klass.log = []
        return l

    @classmethod
    def dump(klass):
        return klass.dict

    @classmethod
    def clear(klass):
        klass.dict = {}
