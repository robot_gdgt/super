"""
Base for Nodes and Decorators
"""

# from .Blackboard import Blackboard

class Base(object):
    def __init__(self):
        pass
        # print(f'{self.label}.base.__init__()')

    def reset(self):
        pass
        # print(f'{self.label}.base.reset()')

    def init(self):
        pass
        # print(f'{self.label}.base.init()')

    def tick(self):
        pass
        # print(f'{self.label}.base.tick()')

    def tock(self):
        pass
        # print(f'{self.label}.base.tock()')

    def deinit(self):
        pass
        # print(f'{self.label}.base.deinit()')

    def abort(self):
        pass
        # print(f'{self.label}.base.abort()')

    def teardown(self):
        pass
        # print(f'{self.label}.base.teardown()')

"""
__init__:


reset:
    same as __init__
    called once at start of execution
    propagates to children and decorators

init:
    called before tock when previous reply was not RUNNING

tick:
    tick shield decos
    init if not running
    tock
    tick exec decos
    tick reply decos
    deinit if not running

tock:


deinit:
    called when return state is not RUNNING

abort:
    called on any node that was previously running but is now superceded
    propagates as needed

teardown:

"""
