const SUCCESS = 1
const FAILURE = 2
const RUNNING = 3
const ABORT = -1

function replyText(reply) {
	switch(parseInt(reply)) {
	case SUCCESS:
		return 'Success';
	case FAILURE:
		return 'Failure';
	case RUNNING:
		return 'Running';
	case ABORT:
		return 'Abort';
	}
	return '?';
}

const nodeCats = {
	"composite": {
		"name": "Composites",
		"applyTo": [
			"root",
			"composite"
		],
	},
	"shieldDeco": {
		"name": "Shield Decorators",
		"applyTo": [
			"composite",
			"action"
		],
	},
	"replyDeco": {
		"name": "Reply Decorators",
		"applyTo": [
			"composite",
			"action"
		],
	},
	"execDeco": {
		"name": "Exec Decorators",
		"applyTo": [
			"composite",
			"action"
		],
	},
	"condition": {
		"name": "Conditions",
		"applyTo": [
			"root",
			"composite"
		],
	},
	"action": {
		"name": "Actions",
		"applyTo": [
			"root",
			"composite"
		],
	},
	"importFile": {
		"name": "Import",
		"applyTo": [
			"root",
			"composite"
		],
	},
};

const nodeTypes = {
	"root": {
		"name": "Start",
		"icon": "fas fa-map-marker",
		"cat": "root",
        "params": [
            {
                "key": "bbInit",
                "label": "Initalize BB<br>key=value (1 per line)",
                "kind": "textarea"
            }
        ]
	},
PUT_IT_HERE
};
