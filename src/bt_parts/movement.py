"""
Movement helpers
"""

import time

import can_proxy_thread as can
from can_ids import CAN_CMD_RC_MOVE_DIST, CAN_CMD_RC_MOVE_TO, CAN_STATUS_RC_STAT, RC_MOVE_IMMED, RC_MOVE_END_STOP
from can_ids import RC_MOVE_FORWARD, RC_MOVE_LEFT, RC_MOVE_STRAIGHT, CAN_CMD_RC_ALIGN_HEADING, CAN_CMD_RC_MOVE_ANGLE
from can_ids import CAN_CMD_RC_MOTORS_OFF, CAN_RC_DIRECT

statusReqTimeoutStart = None
MOVE_IT = True
QPPM = 1590

##### SETUP #####

can.del_filters('movement')
can.add_filter('movement', CAN_STATUS_RC_STAT, 0x7FF)

##### HELPER FUNCTIONS #####

def moveDist(speed, dist, dir='f', when='i', end='s'):
    if dist < 0:
        dist = abs(dist)
        dir = 'r' if dir == 'f' else 'f'
    move(CAN_CMD_RC_MOVE_DIST, speed, dist, dir, 's', 0, when, end)

def moveAngle(speed, angle, dir='f', turn='r', radius = 0, when='i', end='s'):
    if angle < 0:
        angle = abs(angle)
        turn = 'l' if turn == 'r' else 'r'
    move(CAN_CMD_RC_MOVE_ANGLE, speed, angle, dir, turn, radius, when, end)

def move(id, speed, dist_ang, dir='f', turn='s', radius = 0, when='i', end='s'):
    dist_ang = round(dist_ang)
    radius = round(radius)

    print(f'\nmove(speed={speed}, dist_ang={dist_ang}, dir={dir}, turn={turn}, radius={radius}, when={when}, end={end})')

    bytes = []

    bytes.append(
        (RC_MOVE_IMMED if when == 'i' else 0) |
        (RC_MOVE_END_STOP if end == 's' else 0) |
        (RC_MOVE_FORWARD if dir == 'f' else 0) |
        round(speed * 10)
    )

    if turn != 's':
        r = (RC_MOVE_LEFT if turn == 'l' else 0) | radius
    else:
        r = RC_MOVE_STRAIGHT
    bytes.extend(r.to_bytes(2, 'big'))
    bytes.extend(dist_ang.to_bytes(2, 'big'))

    if MOVE_IT: can.send('movement', id, data=bytes)

def moveTo(speed, x, z, heading, when='i', end='s'):
    x = round(x)
    z = round(z)
    heading = round(heading)

    print(f'\nmoveTo(speed={speed}, x={x}, z={z}, heading={heading}, when={when}, end={end})')

    bytes = []

    bytes.extend(x.to_bytes(2, 'big', signed=True))
    bytes.extend(z.to_bytes(2, 'big', signed=True))
    bytes.append(
        (RC_MOVE_IMMED if when == 'i' else 0) |
        (RC_MOVE_END_STOP if end == 's' else 0) |
        round(speed * 10)
    )
    bytes.extend(heading.to_bytes(2, 'big', signed=True))

    if MOVE_IT: can.send('movement', CAN_CMD_RC_MOVE_TO, data=bytes)

def alignHeading(speed, heading):
    speed = round(speed) # QPPS
    heading = round(360 + heading) % 360

    print(f'\nalignHeading(speed={speed}, heading={heading})')

    bytes = []

    bytes.extend(heading.to_bytes(2, 'big'))
    bytes.append(speed)

    if MOVE_IT: can.send('movement', CAN_CMD_RC_ALIGN_HEADING, data=bytes)

def sendMotorCmd(cmd, data):
    can.send('movement', CAN_RC_DIRECT | cmd, data=data)

def disableMotors():
    can.send('movement', CAN_CMD_RC_MOTORS_OFF)

def isMoving():
    global statusReqTimeoutStart

    if MOVE_IT == False:
        return False

    if statusReqTimeoutStart is None:
        statusReqTimeoutStart = time.monotonic()

    CANrx = can.get('movement')
    if CANrx is not None:
        print(f'isMoving rx - id: {format(CANrx["id"], "#X")}, rr: {CANrx["rr"]}, dl: {CANrx["dl"]}, data: {CANrx["data"]}')
        if CANrx['id'] == CAN_STATUS_RC_STAT and not CANrx['rr']:
            if len(CANrx['data']) > 0 and CANrx['data'][0] == 0: # Stopped moving
                statusReqTimeoutStart = None
                print('stopped moving')
                return False
            print('moving')
                
            statusReqTimeoutStart = time.monotonic()

    if (time.monotonic() - statusReqTimeoutStart) > 1.0:
        can.send('movement', CAN_STATUS_RC_STAT, True)
        statusReqTimeoutStart = time.monotonic()

    return True
