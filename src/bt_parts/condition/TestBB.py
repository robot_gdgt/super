"""
Test BB value

data.js: {
    "name": "Test BB Value",
    "label": "Is [{bbKey}] {compare} {value}",
    "icon": "fas fa-question",
    "cat": "condition",
    "params": [
        {
            "key": "bbKey",
            "label": "Return <strong>Success</strong> if<br>blackboard key",
            "kind": "key"
        },
        {
            "key": "compare",
            "label": "Is",
            "kind": "select",
            "options": [
                {"value": "=", "text": "="},
                {"value": "!=", "text": "!="},
                {"value": "<", "text": "<"},
                {"value": "<=", "text": "<="},
                {"value": ">", "text": ">"},
                {"value": ">=", "text": ">="},
                {"value": "null", "text": "is null"},
            ]
        },
        {
            "key": "value",
            "label": "Value"
        }
    ]
}
"""

import logging

# from var_dump import var_dump as vd

from ..Blackboard import Blackboard as bb
from ..Node import Node

class TestBB(Node):
    def __init__(self, data):
        super().__init__(data)
        if not hasattr(self, 'bbKey'):
            raise RuntimeError(f'Missing param - {self.label}.bbKey')
        if not hasattr(self, 'compare'):
            raise RuntimeError(f'Missing param - {self.label}.compare')
        if not hasattr(self, 'value'):
            raise RuntimeError(f'Missing param - {self.label}.value')
        if self.bbKey[0] == 'i':
            self.value = int(self.value)
        elif self.bbKey[0] == 'h':
            self.value = int(self.value, 16)
        elif self.bbKey[0] == 'f':
            self.value = float(self.value)

    # Do something useful
    def tock(self):
        # logging.info(__name__+' '+self.label)                      # pylint: disable=no-member
        b = bb.get(self.label, self.bbKey)
        c = self.compare
        v = self.value

        # print('TestBB')
        # print(f'bbKey: {self.bbKey}')
        # print('bb value: ', end='')
        # vd(b)
        # print('compare: ', end='')
        # vd(c)
        # print('compare value: ', end='')
        # vd(v)

        # print(f'TestBB: {b} ({type(b)}) {c} {v}')
        if c == '=':
                return (Node.SUCCESS if b == v else Node.FAILURE, None)    # pylint: disable=no-member
        elif c == '!=':
                return (Node.SUCCESS if b != v else Node.FAILURE, None)    # pylint: disable=no-member
        elif c == '<':
                return (Node.SUCCESS if b < v else Node.FAILURE, None)    # pylint: disable=no-member
        elif c == '<=':
                return (Node.SUCCESS if b <= v else Node.FAILURE, None)    # pylint: disable=no-member
        elif c == '>':
                return (Node.SUCCESS if b > v else Node.FAILURE, None)    # pylint: disable=no-member
        elif c == '>=':
                return (Node.SUCCESS if b >= v else Node.FAILURE, None)    # pylint: disable=no-member
        else: # if c == 'null':
                return (Node.SUCCESS if b == None else Node.FAILURE, None)    # pylint: disable=no-member
