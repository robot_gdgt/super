"""
Composite is the basis for nodes with children
"""

from importlib import import_module
import logging
# import json
# import importlib.util
import sys

from .Node import Node

# logging.info(__file__)
# logging.info(globals())

class Composite(Node):

    importedModules = {}

    def __init__(self, data):
        super().__init__(data)

        self.childIndex = None
        self.children = []

        # Add children
        if 'children' in data:
            for c in data['children']:
                # logging.info(c)
                if c['type'] == 'exec':
                    className = c['params']['name'][0:1].upper() + c['params']['name'][1:].lower()
                else:
                    className = c['type'][0:1].upper() + c['type'][1:]
                cat = c['cat']
                fullname = f'{cat}.{className}'
                # if className not in globals():
                if fullname not in Composite.importedModules:
                    logging.info(f'from bt_parts.{cat} import {className} as {className}')
                    try:
                        # Composite.importedModules[fullname] = import_module(className, package=cat)
                        # exec(f'from bt_parts.{cat} import {className} as {className}')
                        # Composite.importedModules[className] = getattr(__import__(f'bt_parts.{cat}.{className}', globals(), locals(), [className], 0), className)
                        temp = import_module(f'.{className}', package=f'bt_parts.{cat}')
                        Composite.importedModules[fullname] = getattr(temp, className)
                    except ImportError as err:
                        logging.info(f"Failed to import {fullname}: {err}")
                        continue
                #     print(globals())
                # klass = globals()[className]
                klass = Composite.importedModules[fullname]
                # logging.info(f"{__name__} loading: {klass}")
                self.children.append(klass(c))
        assert self.children, f'{self.label} has no children'

    def reset(self):
        for child in self.children:
            child.reset()
        super().reset()

    def init(self):
        # print(f'{self.label}.init()')
        self.childIndex = 0

    # Call the lone child
    def tock(self):
        # assert self.children, f'{self.label} has no children'
        if self.children and not self.children[0].isDisabled:
            return (self.children[0].tick(), self.children[0].id)
        return (Node.FAILURE, None)

    def abort(self):
        if self.childIndex is not None:
            self.children[self.childIndex].abort()

    def teardown(self):
        # print(f'{self.label}.teardown()')
        # assert self.children, f'{self.label} has no children'
        for child in self.children:
            child.teardown()
        self.children = None
        super().teardown()
