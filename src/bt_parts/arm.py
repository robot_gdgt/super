"""
Arm helpers
"""

import math
import time

import can_proxy_thread as can
from can_ids import CAN_STATUS_ARM, CAN_ARM_INIT, CAN_ARM_JOG, CAN_ARM_DOOR_EXT, CAN_ARM_PARK 
from can_ids import CAN_ARM_LIMP, CAN_ARM_UNPARK, CAN_ARM_MOVETO_A, CAN_ARM_MOVETO_B

JOINT_SHOULDER = 0
JOINT_ELBOW = 1
JOINT_WRIST = 2
JOINT_ROTATE = 3
JOINT_PIVOT = 4

WORLD_OFFSET_X = 0 # mm
WORLD_OFFSET_Y = 271 # mm
WORLD_OFFSET_Z = 58 # mm

motionCmdSent = None
lastStatusRecd = None

##### SETUP #####

can.del_filters('arm')
can.add_filter('arm', CAN_STATUS_ARM, 0x7FF)

##### HELPER FUNCTIONS #####

def armInit():
    armSendCmd(CAN_ARM_INIT)

def armLimp():
    armSendCmd(CAN_ARM_LIMP)

def armPark():
    armSendCmd(CAN_ARM_PARK)

def armUnpark():
    armSendCmd(CAN_ARM_UNPARK)

def armDoorExtend(angP, vf=1):
    # armSendCmd(CAN_ARM_DOOR_EXT)
    x = int(math.sin(math.radians(angP)) * 448) + WORLD_OFFSET_X
    z = int(math.cos(math.radians(angP)) * 448) + WORLD_OFFSET_Z
    armMoveTo(x, WORLD_OFFSET_Y, z, -90, 90, vf)
          
def armJog(joint, deg, vf=1):
    data = [joint]
    data.extend(int.to_bytes(round(deg * 10), 2, 'big', signed=True)) # deg x.x°
    data.append(vf)
    print(f'armJog: {data}')
    armSendCmd(CAN_ARM_JOG, data)

def armMoveTo(x, y, z, angR, angG, vf=1):
    data = []
    data.extend(int.to_bytes(round(x), 2, 'big', signed=True)) # mm
    data.extend(int.to_bytes(round(y), 2, 'big')) # mm
    data.extend(int.to_bytes(round(z), 2, 'big')) # mm
    data.append(0)
    print(f'armMoveTo A: {data}')
    armSendCmd(CAN_ARM_MOVETO_A, data)
    data = []
    data.extend(int.to_bytes(round(angR), 2, 'big', signed=True))
    data.extend(int.to_bytes(round(angG), 1, 'big'))
    data.append(vf)
    data.append(1) # Do it
    print(f'armMoveTo B: {data}')
    armSendCmd(CAN_ARM_MOVETO_B, data)

def armSendCmd(cmd, data=[]):
    global motionCmdSent
    global lastStatusRecd

    can.send('arm', cmd, False, data)
    motionCmdSent = lastStatusRecd = time.monotonic()

def armIsMoving():
    global motionCmdSent
    global lastStatusRecd

    if time.monotonic() - motionCmdSent < 2.0:
        return True

    # Look for reply
    CANrx = can.get('arm')
    if CANrx is not None:
        if CANrx['id'] == CAN_STATUS_ARM and not CANrx['rr']:
            print(f'armIsMoving: {CANrx}')
            if len(CANrx['data']) > 2 and not CANrx['data'][1]: # Not moving
                motionCmdSent = None
                return False
            lastStatusRecd = time.monotonic()

    if time.monotonic() - lastStatusRecd > 1.0:
        can.send('arm', CAN_STATUS_ARM, True) # Get arm isMoving
        lastStatusRecd = time.monotonic()

    return True