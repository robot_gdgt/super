"""
Return FAILURE, or RUNNING until N SUCCESSes

data.js: {
    "name": "Repeat N Times",
    "nodeLabel": "Repeat {i} Times",
    "icon": "fas fa-sync-alt",
    "cat": "execDeco",
    "params": [
        {
            "key": "i",
            "label": "Number of times",
            "default": "1",
            "kind": "integer"
        }
    ]
}
"""

from ..Deco import Deco
from ..Node import Node

class Repeat(Deco):

    def __init__(self, data, node):
        super().__init__(data, node)
        self.i = int(self.i) if hasattr(self, 'i') else 1
        self.counter = 0

    def tick(self, state):
        if state == Node.RUNNING:
            return Node.RUNNING
        if state == Node.FAILURE:
            self.counter = 0
            return Node.FAILURE
        if self.i == 1:
            return Node.SUCCESS
        self.counter += 1
        if self.counter == self.i:
            self.counter = 0
            return Node.SUCCESS
        return Node.RUNNING
