"""
Repeat until success, or after a specified number of failures (optional)

data.js: {
    "name": "Repeat Until Success",
    "icon": "fas fa-sync-alt",
    "cat": "execDeco",
    "params": [
        {
            "key": "i",
            "label": "Maximum number of Failures",
            "kind": "integer"
        }
    ]
}
"""

from ..Deco import Deco
from ..Node import Node

class RepeatUntilSuccess(Deco):

    def __init__(self, data, node):
        super().__init__(data, node)
        self.i = int(self.i) if hasattr(self, 'i') else 0
        self.counter = 0

    def tick(self, state=None):
        if state == Node.RUNNING:
            return Node.RUNNING
        if state == Node.SUCCESS:
            self.counter = 0
            return Node.SUCCESS
        self.counter += 1
        if self.i and self.counter == self.i:
            self.counter = 0
            return Node.FAILURE
        return Node.RUNNING
