"""
Always returns RUNNING or FAILURE

data.js: {
    "name": "Running or Failure",
    "icon": "far fa-times-circle",
    "cat": "replyDeco",
    "help": "Reply is Running or Failure regardless of reply generated by node."
}
"""

from ..Deco import Deco
from ..Node import Node

class RunningOrFailure(Deco):

    def tick(self, state=None):
        if state == Node.RUNNING:
            return state
        return Node.FAILURE
