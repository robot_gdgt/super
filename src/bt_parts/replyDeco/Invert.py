"""
Always invert SUCCESS or FAILURE

data.js: {
    "name": "Invert",
    "icon": "fas fa-random",
    "cat": "replyDeco",
    "help": "Node's Success or Failure replies become Failure or Success, respectively."
}
"""

from ..Deco import Deco
from ..Node import Node

class Invert(Deco):

    def tick(self, state=None):
        if state == Node.RUNNING:
            return state
        if state == Node.SUCCESS:
            return Node.FAILURE
        return Node.SUCCESS
