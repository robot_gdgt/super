"""
Supervisor (BT version) v0.0
"""

import logging
import math
import os
import subprocess
from time import sleep, monotonic

import json

from bt_parts.Node import Node
from bt_parts.Composite import Composite
from bt_parts.Blackboard import Blackboard as bb
from bt_parts.utils import replyStr

from can_ids import *
import can_proxy_thread as can
from gdgt_platform import *
from speak_direct_can_thread import speak_direct
from status_led import StatusLED
from tts_groups import *
from var_dump import var_dump as vd

import web_socket_server
# import redis_client

hbLED = StatusLED(LED_PIN, True)

# FILE = path.splitext(path.basename(__file__))[0]
# logger.init(None, logging.WARNING)

# App state object class
class AppState:
    def __init__(self):
        self.state = CAN_CMD_PGM_SLEEP
        self.file = None
        self.loaded = None
        self.root = None
        self.delay = 0

state = AppState()

##### WEB SOCKET #####

class WebSocketCallbacks:
    def __init__(self):
        pass

    def open(self, client):
        # try:
        if True:
            name = client.data['data']['name']
            print(f'open: files/{name}')
            if not os.path.isfile(f'../files/{name}.json'):
                client.reply(error = "file not found")
                return
            with open(f'../files/{name}.json') as fp:
                btdata = json.load(fp)
            client.reply(btdata)
            state.file = name
            state.loaded = None
        # except Exception as e :
        #     print(e)

    def files(self, client):
        try:
            files = os.listdir('../files')
            files = [f for f in files if os.path.isfile(f'../files/{f}') and f.endswith('.json')] #Filtering only the json files.
            for i in range(len(files)):
                files[i] = os.path.splitext(files[i])[0]
            files.sort(key=str.lower)
            print(files)
            client.reply(json.dumps(files))
        except Exception as e :
            print(e)

    def save(self, client):
        data = client.data
        try:
            name = data['data']['name']
            print(f'save: {name}')
            # print(data['data']['nodes'])
            # vd(data['data']['nodes'])
            with open(f'../files/{name}.json', 'w') as fp:
                json.dump(data['data']['nodes'], fp, indent=2)
            client.reply({'status':'saved'})
            state.file = name
            state.loaded = None
        except Exception as e :
            print(e)

    def delete(self, client):
        try:
            name = client.data['data']['name']
            print(f'delete: {name}')
            os.unlink(f'../files/{name}.json')
            client.reply({'status':'deleted'})
        except Exception as e :
            print(e)

    def run(self, client):
        try:
            if state.file is None:
                client.reply(error = "no file loaded")
                return
            if state.loaded != state.file:
                if state.root is not None:
                    print('teardown')
                    state.root.teardown()
                    print('set to None')
                    state.root = None
                    Node.importedModules = {}
                    Composite.importedModules = {}
                print(f'load: files/{state.file}.json')
                with open(f'../files/{state.file}.json') as fp:
                    btdata = json.load(fp)
                # walk the tree adding subtrees along the way

                try:
                    state.root = Composite(btdata)
                except RuntimeError as e:
                    print(e.args[0])
                    client.reply({'status':'failed', 'msg':e.args[0]})
                    return
                state.loaded = state.file
            state.state = CAN_CMD_PGM_RUNNING
            state.delay = float(bb.get('bt', 'delay', 0))
            client.reply({'status':'ok'})
            web_socket_server.broadcast('start')
        except Exception as e:
            print(e)

    def pause(self, client):
        try:
            state.state = CAN_CMD_PGM_SLEEP
            client.reply({'status':'ok'})
        except Exception as e :
            print(e)

    def stop(self, client):
        try:
            state.state = CAN_CMD_PGM_STOPPING
            client.reply({'status':'ok'})
        except Exception as e :
            print(e)

    def bbset(self, client):
        try:
            k = client.data['data']['k']
            v = client.data['data']['v']
            if v.lower() == 'null' or v.lower() == 'none':
                v = None
            elif k[0] == 'i':
                v = int(v)
            elif k[0] == 'f':
                v = float(v)
            elif v[0] == '(' and v[-1] == ')':
                a = []
                for n in v[1:-1].split(','):
                    a.append(float(n) if '.' in n else int(n))
                v = tuple(a)
            bb.set('bt', k, v)
            if isinstance(v, tuple):
                v = f'{v}'
            client.reply({'status':'ok', 'value':v})
        except Exception as e :
            print(e)

    def bbget(self, client):
        try:
            k = client.data['data']['k']
            v = bb.get('bt', k)
            if isinstance(v, tuple):
                v = f'{v}'
            client.reply({'status':'ok', 'value':v})
        except Exception as e :
            print(e)

    def bbdump(self, client):
        try:
            data = bb.dump()
            for k in data:
                if isinstance(data[k], tuple):
                    data[k] = f'{data[k]}'
            client.reply({'status':'ok', 'values':data})
        except Exception as e :
            print(e)

    def connect(self, client):
        try:
            client.reply({'status':'ok'})
        except Exception as e :
            print(e)

##### SETUP #####

can.del_filter('main', 'all', 'all')
can.add_filter('main', CAN_ALERT, 0x7F8) # Want alert 000 - 007 packets
can.add_filter('main', CAN_STATUS_BATT, 0x7FF) # Want all timeslot packets
can.add_filter('main', CAN_CMD_HALT, 0x7FF)
can.add_filter('main', CAN_CMD_QUIT, 0x7FF)
can.add_filter('main', CAN_CMD_SUPER, 0x7F0)

# red = redis_client.init()

web_socket_server.webSocketCallbacks = WebSocketCallbacks()

# speak_direct('supervisor is running')

# red.set('obst_dist', 0)
# red.set('obst_dir', 0)
# red.set('iAlerts', 0)
# bb.set('bt', 'obst_dist', 0)
# bb.set('bt', 'obst_dir', 0)
bb.set('bt', 'iAlerts', 0)
bb.set('bt', 'fAvgV', 0.0)
bb.set('bt', 'fAvgI', 0.0)
bb.set('bt', 'fMoveSpeed', 0.2)
bb.getLog()

hbLED.heartbeat()

##### LOOP #####

elapsed = None
passes = 0
reply = None
prev_state = None
prevTick = 0

spinner = 0
spinners = ['-', '\\', '|', '/']
print('READY\n-', end='')
spinnerTrigger = monotonic()

while True:
    if (monotonic() - spinnerTrigger) >= 0.1:
        spinner = (spinner + 1) % 4
        print(f'\b{spinners[spinner]}', end='', flush=True)
        spinnerTrigger = monotonic()

    CANrx = can.get('main')
    if CANrx is not None:
        if CANrx['id'] != CAN_STATUS_BATT:
            print(f'\nCAN Rx: {CANrx}')

        if CANrx['id'] & 0x7F8 == CAN_ALERT: # any alert
            bitmask = 1 << (CANrx['id'] & 0x007)
            alerts = bb.get('CANrx CAN_ALERT', 'iAlerts') | bitmask
            bb.set('CANrx CAN_ALERT', 'iAlerts', alerts)
            alerts = format(alerts, '08b')
            print(f'alerts 1: {alerts}')
            # print('CANrx CAN_ALERT')

        elif CANrx['id'] & 0x7F8 == CAN_STATUS: # any status
            bitmask = 1 << (CANrx['id'] & 0x007)
            alerts = bb.get('CANrx CAN_STATUS', 'iAlerts') & ~bitmask
            bb.set('CANrx CAN_STATUS', 'iAlerts', alerts)
            alerts = format(alerts, '08b')
            print(f'alerts 2: {alerts}')

        elif CANrx['id'] == CAN_STATUS_BATT:
            avgV = int.from_bytes(CANrx['data'][0:2], 'big') / 1000
            avgI = (int.from_bytes(CANrx['data'][4:6], 'big') - 32768) / 1000

            bb.set('CANrx CAN_STATUS_BATT', 'fAvgV', avgV)
            bb.set('CANrx CAN_STATUS_BATT', 'fAvgI', avgI)

        elif CANrx['id'] == CAN_STATUS_VIS_OBST:
            bb.set('CANrx CAN_STATUS_VIS_OBST', 'obst_dist', CANrx['data'][0])
            bb.set('CANrx CAN_STATUS_VIS_OBST', 'obst_dir', CANrx['data'][1])

        elif CANrx['id'] == CAN_CMD_SUPER:
            if CANrx['data'][0] == SUPER_STATE:
                state.state = CANrx['data'][1]
            elif CANrx['data'][0] == SUPER_SPEED:
                bb.set('CANrx CAN_CMD_SUPER', 'fMoveSpeed', CANrx['data'][1] / 10)

        elif CANrx['id'] == CAN_CMD_QUIT:
            logging.info('quit')
            state.state = CAN_CMD_QUIT

        elif CANrx['id'] == CAN_CMD_HALT:
            logging.info('shutdown')
            state.state = CAN_CMD_HALT

    if state.state == CAN_CMD_PGM_RUNNING:
        if elapsed == None:
            state.root.reset()

            # Set inital BB values
            if hasattr(state.root, 'bbInit'):
                for row in state.root.bbInit.split('\n'):
                    (bbKey, bbValue) = row.split('=')
                    bbKey = bbKey.strip()
                    if bbKey == '': continue
                    bbValue = bbValue.strip()
                    if bbKey[0] == 'i':
                        if bbValue[0:2] == '0x' or bbValue[0:2] == '0X':
                            bbValue = int(bbValue, 0)
                        else:
                            bbValue = int(bbValue)
                    elif bbKey[0] == 'h':
                        bbValue = int(bbValue, 16)
                    elif bbKey[0] == 'f':
                        bbValue = float(bbValue)
                    bb.set('root', bbKey, bbValue)

            elapsed = 0
            passes = 0
            start = monotonic()
        elif prev_state == CAN_CMD_PGM_SLEEP:
            start = monotonic()

        logging.info('==============================')
        if state.delay > 0:
            if monotonic() - prevTick >= state.delay:
                reply = state.root.tick()
                prevTick = monotonic()
        else:
            reply = state.root.tick()
        # logging.info(f'tick: {reply} = {replyStr(reply)}')
        passes += 1

        bblog = bb.getLog()
        if bblog != []:
            for e in bblog:
                if isinstance(e['value'], tuple):
                    e['value'] = f'{e["value"]}'
            web_socket_server.broadcast('bbchanges', bblog)
        
        if passes % 100 == 0:
            web_socket_server.broadcast('tick')

        if reply != Node.RUNNING:
            state.state = CAN_CMD_PGM_STOPPING

    if prev_state == CAN_CMD_PGM_RUNNING and state.state > CAN_CMD_PGM_RUNNING:
        elapsed += monotonic() - start

    if state.state == CAN_CMD_PGM_STOPPING:
        web_socket_server.broadcast('finished', {
            "passes": passes,
            "elapsed": elapsed,
            "result": reply
        })
        if elapsed is not None and elapsed > 0:
            print(f"{int(passes / elapsed + 0.5)} FPS")
        elapsed = None
        passes = 0
        state.state = CAN_CMD_PGM_SLEEP

    prev_state = state.state

    # Blink the LED
    hbLED.tickle()

    if state.state == CAN_CMD_PGM_QUIT or state.state == CAN_CMD_QUIT or state.state == CAN_CMD_HALT:
        break
    
if state.state == CAN_CMD_HALT:
    sleep(5)
    subprocess.run("sudo halt", shell=True)

print('\nGood-bye')
