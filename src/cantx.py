import can_proxy_thread as can

while (True):
	userInput = str(input('Enter data: ')).lower()
	tok = userInput.split()
	if len(tok) == 0:
		continue
	if tok[0] == 'q':
		break
	
	if tok[0][0:2] == '0x' or tok[0][0:2] == '0X':
		id = int(tok[0], 0)
	else:
		id = int(tok[0])

	bytes = []

	for v in tok[1:]:
		if v == '':
			continue
		bytes.append(int(v))

	print(f'ID: {id} data: {bytes}')
	can.send('direct', id, data=bytes)
